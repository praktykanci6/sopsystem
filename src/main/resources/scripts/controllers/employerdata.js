/**
 * Created by Konrad on 14.11.2016.
 */

angular.module('mainApp')
    .controller('EmployerDataCtrl', function ($scope, $routeParams, $resource) {
        $scope.employer = {

        };
        if($routeParams.employerId != null){
            var employerId = $routeParams.employerId;
            $resource('get/employer/data',null, {
                'getEmployer': {method:'POST', isArray: false}
            }).getEmployer(employerId, function (response) {
                $scope.employer.id = response.id;
                $scope.employer.name = response.name;
            })
        }
    });
