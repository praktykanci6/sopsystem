/**
 * Created by Konrad on 29.10.2016.
 */

angular.module("mainApp")
    .controller("PracticelistCtrl" ,function ($scope, $http, $location) {

        $scope.employerList = [];
        $http.get('/list_of_practices')
            .success(function (response) {
            $scope.employerList = response;
        })
            .error(function(response){

            });


        $scope.showItem = function showItem(row) {
            var index = $scope.employerList.indexOf(row);
            $location.path('/employer/show/' + index);
        }
    });


