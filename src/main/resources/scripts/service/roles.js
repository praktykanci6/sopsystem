/**
 * Created by Konrad on 10.11.2016.
 */

angular.module('mainApp')
    .constant('roles',{
        'admin':['ADMIN'],
        'student': ['STUDENT'],
        'employer': ['EMPLOYER']
    });
