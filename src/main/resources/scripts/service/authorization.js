/**
 * Created by Konrad on 10.11.2016.
 */

angular.
    module('mainApp').
    factory('authorizationService', function ($resource, $q, $rootScope, $location) {
    return{

        permissionModel: {
            permission: {},
            isPermissionLoaded: false
        },

        permissionCheck: function (roleCollection) {

            var deferred = $q.defer();

            var parentPointer = this;

            if(this.permissionModel.isPermissionLoaded){
                this.getPermission(this.permissionModel,roleCollection,deferred);
            }else{
                $resource('/user/authentication').get().$promise.then(function(response){
                   parentPointer.permissionModel.permission = response;

                    parentPointer.permissionModel.isPermissionLoaded = true;

                    parentPointer.getPermission(parentPointer.permissionModel,roleCollection,deferred);
                });

            }
            return deferred.promise;
        },
        
        getPermission: function (permissionModel, roleCollection, deferred) {
            var ifPermissionPassed = false;

            angular.forEach(roleCollection,function (role) {
                switch(role){
                    case 'STUDENT':
                        if(permissionModel.permission.role == 'STUDENT'){
                            ifPermissionPassed = true;
                        }
                        break;
                    case 'ADMIN':
                        if(permissionModel.permission.role == 'ADMIN'){
                            ifPermissionPassed = true;
                        }
                        break;
                    case 'EMPLOYER':
                        if(permissionModel.permission.role == 'EMPLOYER'){
                            ifPermissionPassed = true;
                        }
                        break;
                    default:
                        ifPermissionPassed = false;
                }
            });
            if(!ifPermissionPassed){
               $location.path('/authorized_request_error');
            }else{
                deferred.resolve();
            }
        }
    }
});