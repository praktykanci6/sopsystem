﻿
--TRYBY STUDIÓW
INSERT INTO tryby_studiow (id_trybu_studiow,tryb_studiow) VALUES 
(1,'stacjonarne'),
(2,'nie stacjonarne');

--TYTUŁY ZAWODOWE
INSERT INTO tytuly_zawodowe (id_tytulu_zawodowego,tytul_zawodowy,archiwum) VALUES 
(1,'Licencjat',false),
(2,'Inżynier',false),
(3,'Magister',false);

--STOPNIE STUDIÓW
INSERT INTO stopnie_studiow (id_stopnia_studiow, stopien_studiow) VALUES
(1,'licencjackie,inżynierskie'),
(2,'magisterskie'),
(3,'podyplomowe');

--LICZBA SEMESTRÓW
INSERT INTO liczba_semestrow (id_liczby_semestrow,liczba_semestrow,archiwum) VALUES 
(1,4,false),
(2,6,false),
(3,7,false);

--PROFILE KSZTALCENIA
INSERT INTO profile_ksztalcenia (id_profilu_ksztalcenia,profil_ksztalcenia) VALUES
(1,'akademicki'),
(2,'praktyczny');

--ROLE
INSERT INTO role (id_roli,rola) VALUES
(1,'ADMINISTRATOR'),
(2,'STUDENT'),
(3,'PRAKTYKODAWCA'),
(4,'OPIEKUN');
