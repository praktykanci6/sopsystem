
/*
Created: 14.12.2013
Modified: 18.11.2016
Project: ORM (Alternate Keys)
Model: Praktyki (Hibernate)
Version: 1.20
Database: PostgreSQL 9.2
*/




-- Create schemas section -------------------------------------------------

CREATE SCHEMA "praktyki"
;

-- Create tables section -------------------------------------------------

-- Table studenci

CREATE TABLE "studenci"(
 "nr_albumu" Character varying(6) NOT NULL,
 "id_adresu" BIGINT,
 "id_osoby" BIGINT NOT NULL
)
;

-- Add keys for table studenci

ALTER TABLE "studenci" ADD CONSTRAINT "K001" PRIMARY KEY ("nr_albumu")
;

ALTER TABLE "studenci" ADD CONSTRAINT "K002" UNIQUE ("id_osoby")
;

-- Table adresy

CREATE TABLE "adresy"(
 "id_adresu" BIGSERIAL NOT NULL,
 "ulica" Character varying(255) NOT NULL,
 "nr_budynku" Character varying(10) NOT NULL,
 "miejscowosc" Character varying(35) NOT NULL,
 "kod_pocztowy" Character varying(6) NOT NULL,
 "kraj" Character varying(35) NOT NULL,
 "telefon1" Character varying(15),
 "telefon2" Character varying(15),
 "fax" Character varying(40)
)
;

-- Add keys for table adresy

ALTER TABLE "adresy" ADD CONSTRAINT "Key3" PRIMARY KEY ("id_adresu")
;

-- Table kierunki_studiow

CREATE TABLE "kierunki_studiow"(
 "id_kierunku" BIGSERIAL NOT NULL,
 "nazwa_kierunku" Character varying(50) NOT NULL,
 "id_stopnia_studiow" Smallint NOT NULL,
 "id_trybu_studiow" Smallint NOT NULL,
 "id_tytulu_zawodowego" Smallint NOT NULL,
 "id_liczby_semestrow" Integer,
 "id_profilu_ksztalcenia" Bigint,
 "id_specjalnosci" Bigint,
 "polon" Character(20)
)
;

-- Create indexes for table kierunki_studiow

CREATE INDEX "IX_Relationship3" ON "kierunki_studiow" ("id_liczby_semestrow")
;

CREATE INDEX "IX_Relationship18" ON "kierunki_studiow" ("id_profilu_ksztalcenia")
;

CREATE INDEX "IX_Relationship19" ON "kierunki_studiow" ("id_specjalnosci")
;

-- Add keys for table kierunki_studiow

ALTER TABLE "kierunki_studiow" ADD CONSTRAINT "K008" PRIMARY KEY ("id_kierunku")
;

ALTER TABLE "kierunki_studiow" ADD CONSTRAINT "K018" UNIQUE ("nazwa_kierunku","id_stopnia_studiow","id_trybu_studiow")
;

-- Table praktykodawcy

CREATE TABLE "praktykodawcy"(
 "id_praktykodawcy" BigSerial NOT NULL,
 "nazwa" Character varying(30) NOT NULL,
 "pelna_nazwa" Character varying(100) NOT NULL,
 "zaufany" Boolean NOT NULL,
 "opis" Varchar
)
;

-- Add keys for table praktykodawcy

ALTER TABLE "praktykodawcy" ADD CONSTRAINT "Key5" PRIMARY KEY ("id_praktykodawcy")
;

-- Table praktyki

CREATE TABLE "praktyki"(
 "id_praktyki_studenckiej" BigSerial NOT NULL,
 "id_szablonu" Integer,
 "id_praktykodawcy" Integer NOT NULL,
 "data_rozpoczecia" Date NOT NULL,
 "data_zakonczenia" Date NOT NULL,
 "id_porozumienia" Bigint,
 "data_zapisu" Timestamp with time zone,
 "ocena" Smallint,
 "skierowanie" Bytea,
 "karta_oceny" Bytea,
 "oswiadczenie_praktykodawcy" Bytea,
 "wymagana_kwalifikacja" Boolean NOT NULL,
 "data_zgloszenia" Date DEFAULT now() NOT NULL,
 "id_koordynatora_kierunkowego_praktykodawcy" Bigint,
 "id_studenta_kierunku" Bigint,
 "id_adresu_praktykodawcy" Bigint NOT NULL,
 "id_opiekuna_kierunkowego" Bigint,
 "id_praktyki_kierunkowej" Bigint
)
;

-- Add keys for table praktyki

ALTER TABLE "praktyki" ADD CONSTRAINT "K020" PRIMARY KEY ("id_praktyki_studenckiej")
;

-- Table studenci_kierunkow

CREATE TABLE "studenci_kierunkow"(
 "id_studenta_kierunku" BigSerial NOT NULL,
 "nr_albumu" Character varying(6) NOT NULL,
 "id_rocznika" Bigint NOT NULL
)
;

-- Add keys for table studenci_kierunkow

ALTER TABLE "studenci_kierunkow" ADD CONSTRAINT "Key12" PRIMARY KEY ("nr_albumu","id_rocznika","id_studenta_kierunku")
;

ALTER TABLE "studenci_kierunkow" ADD CONSTRAINT "Key13" UNIQUE ("id_studenta_kierunku")
;

-- Table profil

CREATE TABLE "profil"(
 "id_profilu" Serial NOT NULL,
 "branza" Character varying(50) NOT NULL
)
;

-- Add keys for table profil

ALTER TABLE "profil" ADD CONSTRAINT "K006" PRIMARY KEY ("id_profilu")
;

ALTER TABLE "profil" ADD CONSTRAINT "K007" UNIQUE ("branza")
;

-- Table profile_praktykodawcy

CREATE TABLE "profile_praktykodawcy"(
 "id_praktykodawcy" BIGSERIAL NOT NULL,
 "id_profilu" Integer NOT NULL
)
;

-- Add keys for table profile_praktykodawcy

ALTER TABLE "profile_praktykodawcy" ADD CONSTRAINT "Key15" PRIMARY KEY ("id_praktykodawcy","id_profilu")
;

-- Table typy_praktyk

CREATE TABLE "typy_praktyk"(
 "id_typu_praktyki" Smallint NOT NULL,
 "typ_praktyki" Character varying(20) NOT NULL
)
;

-- Add keys for table typy_praktyk

ALTER TABLE "typy_praktyk" ADD CONSTRAINT "Key19" PRIMARY KEY ("id_typu_praktyki")
;

ALTER TABLE "typy_praktyk" ADD CONSTRAINT "Key2001" UNIQUE ("typ_praktyki")
;

-- Table praktyki_kierunkowe

CREATE TABLE "praktyki_kierunkowe"(
 "id_praktyki_kierunkowej" BigSerial NOT NULL,
 "id_typu_praktyki" Smallint NOT NULL,
 "id_roku_akademickiego" Smallint NOT NULL,
 "liczba_godzin" Smallint,
 "czas_trwania" Smallint,
 "id_jednostki" Smallint NOT NULL,
 "id_rocznika" Bigint NOT NULL
)
;

-- Add keys for table praktyki_kierunkowe

ALTER TABLE "praktyki_kierunkowe" ADD CONSTRAINT "K049" PRIMARY KEY ("id_typu_praktyki","id_roku_akademickiego","id_rocznika","id_praktyki_kierunkowej")
;

ALTER TABLE "praktyki_kierunkowe" ADD CONSTRAINT "K050" UNIQUE ("id_praktyki_kierunkowej")
;

-- Table lata_akademickie

CREATE TABLE "lata_akademickie"(
 "id_roku_akademickiego" Serial NOT NULL,
 "rok_akademicki" Character varying(10) NOT NULL,
 "data_rozpoczecia" Date NOT NULL,
 "data_zakonczenia" Date NOT NULL
)
;

-- Add keys for table lata_akademickie

ALTER TABLE "lata_akademickie" ADD CONSTRAINT "Key21" PRIMARY KEY ("id_roku_akademickiego")
;

ALTER TABLE "lata_akademickie" ADD CONSTRAINT "Key22" UNIQUE ("rok_akademicki")
;

-- Table szablony_praktyk

CREATE TABLE "szablony_praktyk"(
 "id_szablonu" Serial NOT NULL,
 "szablon" Character varying(50) NOT NULL,
 "opis_szablonu" Character varying(1000)
)
;

-- Add keys for table szablony_praktyk

ALTER TABLE "szablony_praktyk" ADD CONSTRAINT "Key2002" PRIMARY KEY ("id_szablonu")
;

-- Table jednostki_kalendarzowe

CREATE TABLE "jednostki_kalendarzowe"(
 "id_jednostki_kalendarzowej" Smallint NOT NULL,
 "jednostka_kalendarzowa" Character varying(15) NOT NULL
)
;

-- Add keys for table jednostki_kalendarzowe

ALTER TABLE "jednostki_kalendarzowe" ADD CONSTRAINT "K014" PRIMARY KEY ("id_jednostki_kalendarzowej")
;

ALTER TABLE "jednostki_kalendarzowe" ADD CONSTRAINT "K015" UNIQUE ("jednostka_kalendarzowa")
;

-- Table koordynatorzy_praktyk

CREATE TABLE "koordynatorzy_praktyk"(
 "id_koordynatora_praktyk" BigSerial NOT NULL,
 "id_osoby" BIGINT
)
;

-- Add keys for table koordynatorzy_praktyk

ALTER TABLE "koordynatorzy_praktyk" ADD CONSTRAINT "Key2008" PRIMARY KEY ("id_koordynatora_praktyk")
;

-- Table koordynatorzy_kierunkowi

CREATE TABLE "koordynatorzy_kierunkowi"(
 "id_kordynatora_kierunkowego" BigSerial NOT NULL,
 "id_koordynatora_praktyk" Bigint NOT NULL,
 "data_powolania" Date,
 "id_rocznika" Bigint NOT NULL
)
;

-- Add keys for table koordynatorzy_kierunkowi

ALTER TABLE "koordynatorzy_kierunkowi" ADD CONSTRAINT "Key2009" PRIMARY KEY ("id_koordynatora_praktyk","id_rocznika","id_kordynatora_kierunkowego")
;

ALTER TABLE "koordynatorzy_kierunkowi" ADD CONSTRAINT "Key2010" UNIQUE ("id_kordynatora_kierunkowego")
;

-- Table typy_zgloszen

CREATE TABLE "typy_zgloszen"(
 "id_typu_zgloszenia" Smallint NOT NULL,
 "typ_zgloszenia" Character varying(25) NOT NULL
)
;

-- Add keys for table typy_zgloszen

ALTER TABLE "typy_zgloszen" ADD CONSTRAINT "K003" PRIMARY KEY ("id_typu_zgloszenia")
;

ALTER TABLE "typy_zgloszen" ADD CONSTRAINT "K004" UNIQUE ("typ_zgloszenia")
;

-- Table koordynatorzy_kierunkowi_praktykodawcow

CREATE TABLE "koordynatorzy_kierunkowi_praktykodawcow"(
 "id_koordynatora_kierunkowego_praktykodawcy" BigSerial NOT NULL,
 "id_praktykodawcy" BIGINT NOT NULL,
 "id_kordynatora_kierunkowego" Bigint NOT NULL
)
;

-- Add keys for table koordynatorzy_kierunkowi_praktykodawcow

ALTER TABLE "koordynatorzy_kierunkowi_praktykodawcow" ADD CONSTRAINT "Key2011" PRIMARY KEY ("id_praktykodawcy","id_kordynatora_kierunkowego","id_koordynatora_kierunkowego_praktykodawcy")
;

ALTER TABLE "koordynatorzy_kierunkowi_praktykodawcow" ADD CONSTRAINT "Key2013" UNIQUE ("id_praktykodawcy")
;

ALTER TABLE "koordynatorzy_kierunkowi_praktykodawcow" ADD CONSTRAINT "Key2014" UNIQUE ("id_koordynatora_kierunkowego_praktykodawcy")
;

-- Table porozumienia

CREATE TABLE "porozumienia"(
 "id_porozumienia" BigSerial NOT NULL,
 "nr_porozumienia" Character varying(25) NOT NULL,
 "data_zawarcia" Date NOT NULL,
 "porozumienie" Bytea
)
;

-- Add keys for table porozumienia

ALTER TABLE "porozumienia" ADD CONSTRAINT "Key20014" PRIMARY KEY ("id_porozumienia")
;

-- Table opiekunowie_praktyk

CREATE TABLE "opiekunowie_praktyk"(
 "id_opiekuna_praktyk" BIGSERIAL NOT NULL,
 "stanowisko" Character varying(50) NOT NULL,
 "id_osoby" BIGINT
)
;

-- Add keys for table opiekunowie_praktyk

ALTER TABLE "opiekunowie_praktyk" ADD CONSTRAINT "K005" PRIMARY KEY ("id_opiekuna_praktyk")
;

-- Table osoby

CREATE TABLE "osoby"(
 "id_osoby" BigSerial NOT NULL,
 "tytul_zawodowy" Character varying(255),
 "imie" Character varying(255) NOT NULL,
 "nazwisko" Character varying(255) NOT NULL,
 "data_urodzenia" Date,
 "email" Character varying(255) NOT NULL,
 "telefon_komorkowy" Character varying(15),
 "telefon_stacjonarny" Character varying(15)
)
;

-- Add keys for table osoby

ALTER TABLE "osoby" ADD CONSTRAINT "Key2016" PRIMARY KEY ("id_osoby")
;

ALTER TABLE "osoby" ADD CONSTRAINT "email" UNIQUE ("email")
;

-- Table opiekunowie_kierunkowi

CREATE TABLE "opiekunowie_kierunkowi"(
 "id_opiekuna_kierunkowego" BigSerial NOT NULL,
 "id_opiekuna_praktyk" Integer NOT NULL,
 "id_praktykodawcy" BIGSERIAL NOT NULL,
 "id_rocznika" Bigint NOT NULL
)
;

-- Add keys for table opiekunowie_kierunkowi

ALTER TABLE "opiekunowie_kierunkowi" ADD CONSTRAINT "Key2017" PRIMARY KEY ("id_praktykodawcy","id_opiekuna_praktyk","id_rocznika","id_opiekuna_kierunkowego")
;

ALTER TABLE "opiekunowie_kierunkowi" ADD CONSTRAINT "Key2018" UNIQUE ("id_opiekuna_kierunkowego")
;

-- Table uzytkownicy

CREATE TABLE "uzytkownicy"(
 "id_uzytkownika" BigSerial NOT NULL,
 "id_osoby" BIGINT NOT NULL,
 "id_roli" Bigint,
 "login" Character varying(20) NOT NULL,
 "haslo" Character varying(30) NOT NULL,
 "blokada" Boolean
)
;

-- Add keys for table uzytkownicy

ALTER TABLE "uzytkownicy" ADD CONSTRAINT "K0002" PRIMARY KEY ("id_uzytkownika")
;

ALTER TABLE "uzytkownicy" ADD CONSTRAINT "K0001" UNIQUE ("login")
;

-- Table role

CREATE TABLE "role"(
 "id_roli" INTEGER NOT NULL,
 "rola" Character varying(25) NOT NULL
)
;

-- Add keys for table role

ALTER TABLE "role" ADD CONSTRAINT "Key201979" PRIMARY KEY ("id_roli")
;

ALTER TABLE "role" ADD CONSTRAINT "nazwa_roli" UNIQUE ("rola")
;

-- Table adresy_praktykodawcy

CREATE TABLE "adresy_praktykodawcy"(
 "id_adresu_praktykodawcy" BigSerial NOT NULL,
 "id_adresu" BIGINT NOT NULL,
 "id_praktykodawcy" BIGINT NOT NULL,
 "oddzial" Boolean NOT NULL
)
;

-- Add keys for table adresy_praktykodawcy

ALTER TABLE "adresy_praktykodawcy" ADD CONSTRAINT "Key201980" PRIMARY KEY ("id_adresu","id_praktykodawcy","id_adresu_praktykodawcy")
;

ALTER TABLE "adresy_praktykodawcy" ADD CONSTRAINT "Key2019811" UNIQUE ("id_adresu_praktykodawcy")
;

-- Table stopnie_studiow

CREATE TABLE "stopnie_studiow"(
 "id_stopnia_studiow" Smallint NOT NULL,
 "stopien_studiow" Character varying(30) NOT NULL
)
;

-- Add keys for table stopnie_studiow

ALTER TABLE "stopnie_studiow" ADD CONSTRAINT "K009" PRIMARY KEY ("id_stopnia_studiow")
;

ALTER TABLE "stopnie_studiow" ADD CONSTRAINT "K010" UNIQUE ("stopien_studiow")
;

-- Table tryby_studiow

CREATE TABLE "tryby_studiow"(
 "id_trybu_studiow" Smallint NOT NULL,
 "tryb_studiow" Character varying(30) NOT NULL
)
;

-- Add keys for table tryby_studiow

ALTER TABLE "tryby_studiow" ADD CONSTRAINT "K011" PRIMARY KEY ("id_trybu_studiow")
;

ALTER TABLE "tryby_studiow" ADD CONSTRAINT "K052" UNIQUE ("tryb_studiow")
;

-- Table tytuly_zawodowe

CREATE TABLE "tytuly_zawodowe"(
 "id_tytulu_zawodowego" Smallint NOT NULL,
 "tytul_zawodowy" Character varying(30) NOT NULL,
 "archiwum" Boolean NOT NULL
)
;

-- Add keys for table tytuly_zawodowe

ALTER TABLE "tytuly_zawodowe" ADD CONSTRAINT "K012" PRIMARY KEY ("id_tytulu_zawodowego")
;

ALTER TABLE "tytuly_zawodowe" ADD CONSTRAINT "K013" UNIQUE ("tytul_zawodowy")
;

-- Table statusy

CREATE TABLE "statusy"(
 "id_statusu" Smallint NOT NULL,
 "status" Character varying(25) NOT NULL
)
;

-- Add keys for table statusy

ALTER TABLE "statusy" ADD CONSTRAINT "K016" PRIMARY KEY ("id_statusu")
;

ALTER TABLE "statusy" ADD CONSTRAINT "K017" UNIQUE ("status")
;

-- Table roczniki_studiow

CREATE TABLE "roczniki_studiow"(
 "id_rocznika" BigSerial NOT NULL,
 "id_roku_akademickiego" Smallint NOT NULL,
 "id_kierunku" BIGINT NOT NULL,
 "rozpoczecie_zapisow_rozmowy" Date,
 "zakoczenie_zapisow_rozmowy" Date,
 "rozpoczecie_wyborow_praktyk" Date,
 "zakonczenie_wyborow_praktyk" Date,
 "rozpoczecie_praktyk" Date,
 "zakonczenie_praktyk" Date
)
;
COMMENT ON COLUMN "roczniki_studiow"."rozpoczecie_praktyk" IS 'Minimalna (najwczesniejsza) data rozpoczecia praktyki'
;
COMMENT ON COLUMN "roczniki_studiow"."zakonczenie_praktyk" IS 'Maksymalna data zakonczenia praktyki'
;

-- Add keys for table roczniki_studiow

ALTER TABLE "roczniki_studiow" ADD CONSTRAINT "Key201981" PRIMARY KEY ("id_roku_akademickiego","id_kierunku","id_rocznika")
;

ALTER TABLE "roczniki_studiow" ADD CONSTRAINT "Key2019d3" UNIQUE ("id_rocznika")
;

-- Table specjalnosci

CREATE TABLE "specjalnosci"(
 "id_specjalnosci" Bigint NOT NULL,
 "nazwa_specjalnosci" Varchar NOT NULL,
 "archiwum" Boolean NOT NULL
)
;

-- Add keys for table specjalnosci

ALTER TABLE "specjalnosci" ADD CONSTRAINT "Key201982" PRIMARY KEY ("id_specjalnosci")
;

ALTER TABLE "specjalnosci" ADD CONSTRAINT "id_specjalnosci" UNIQUE ("id_specjalnosci")
;

-- Table liczba_semestrow

CREATE TABLE "liczba_semestrow"(
 "id_liczby_semestrow" Integer NOT NULL,
 "liczba_semestrow" Integer NOT NULL,
 "archiwum" Boolean NOT NULL
)
;

-- Add keys for table liczba_semestrow

ALTER TABLE "liczba_semestrow" ADD CONSTRAINT "Key201983" PRIMARY KEY ("id_liczby_semestrow")
;

ALTER TABLE "liczba_semestrow" ADD CONSTRAINT "id_liczby_semestrow" UNIQUE ("id_liczby_semestrow")
;

-- Table rozmowy_kwalifikacyjne

CREATE TABLE "rozmowy_kwalifikacyjne"(
"id_rozmowy_kwalifikacyjnej" BigSerial PRIMARY KEY ,
 "termin_rozmowy" Timestamp with time zone NOT NULL,
 "ocena" Integer,
 "id_praktykodawcy" BIGINT NOT NULL,
 "id_studenta_kierunku" Bigint
)
;

-- Create indexes for table rozmowy_kwalifikacyjne

CREATE INDEX "IX_Relationship16" ON "rozmowy_kwalifikacyjne" ("id_studenta_kierunku")
;

CREATE INDEX "IX_Relationship22" ON "rozmowy_kwalifikacyjne" ("id_praktykodawcy")
;

-- Add keys for table rozmowy_kwalifikacyjne

-- ALTER TABLE "rozmowy_kwalifikacyjne" ADD CONSTRAINT "Key201987" PRIMARY KEY ("termin_rozmowy","id_praktykodawcy")
-- ;

-- Table profile_ksztalcenia

CREATE TABLE "profile_ksztalcenia"(
 "id_profilu_ksztalcenia" Bigint NOT NULL,
  "profil_ksztalcenia" Character varying(30) NOT NULL
)
;

-- Add keys for table profile_ksztalcenia

ALTER TABLE "profile_ksztalcenia" ADD CONSTRAINT "Key201988" PRIMARY KEY ("id_profilu_ksztalcenia")
;

ALTER TABLE "profile_ksztalcenia" ADD CONSTRAINT "profil_ksztalcenia" UNIQUE ("profil_ksztalcenia")
;

-- Table wybory_praktyk

CREATE TABLE "wybory_praktyk"(
 "id_wyboru_praktyki" BigSerial NOT NULL,
 "id_praktyki_studenckiej" Bigint NOT NULL,
 "priotytet" Smallint NOT NULL,
 "id_studenta_kierunku" Bigint NOT NULL
)
;

-- Add keys for table wybory_praktyk

ALTER TABLE "wybory_praktyk" ADD CONSTRAINT "Key2019901" PRIMARY KEY ("id_wyboru_praktyki")
;

ALTER TABLE "wybory_praktyk" ADD CONSTRAINT "Key201989" UNIQUE ("id_praktyki_studenckiej","id_studenta_kierunku")
;

-- Table statusy_praktyk

CREATE TABLE "statusy_praktyk"(
 "id_statusu_praktyki" BigSerial NOT NULL,
 "id_praktyki_studenckiej" Bigint NOT NULL,
 "data_poczatku" Date NOT NULL,
 "data_konca" Date,
 "biezacy" Boolean NOT NULL,
 "id_statusu" Smallint
)
;

-- Create indexes for table statusy_praktyk

CREATE INDEX "IX_Relationship25" ON "statusy_praktyk" ("id_statusu")
;

CREATE INDEX "IX_Relationship26" ON "statusy_praktyk" ("id_praktyki_studenckiej")
;

-- Add keys for table statusy_praktyk

ALTER TABLE "statusy_praktyk" ADD CONSTRAINT "Key201990" PRIMARY KEY ("id_statusu_praktyki")
;

-- Create relationships section -------------------------------------------------

ALTER TABLE "studenci_kierunkow" ADD CONSTRAINT "R005" FOREIGN KEY ("nr_albumu") REFERENCES "studenci" ("nr_albumu") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki" ADD CONSTRAINT "R021" FOREIGN KEY ("id_praktykodawcy") REFERENCES "praktykodawcy" ("id_praktykodawcy") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "profile_praktykodawcy" ADD CONSTRAINT "R037" FOREIGN KEY ("id_praktykodawcy") REFERENCES "praktykodawcy" ("id_praktykodawcy") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "profile_praktykodawcy" ADD CONSTRAINT "R026" FOREIGN KEY ("id_profilu") REFERENCES "profil" ("id_profilu") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "studenci" ADD CONSTRAINT "R032" FOREIGN KEY ("id_adresu") REFERENCES "adresy" ("id_adresu") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki_kierunkowe" ADD CONSTRAINT "R006" FOREIGN KEY ("id_typu_praktyki") REFERENCES "typy_praktyk" ("id_typu_praktyki") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki_kierunkowe" ADD CONSTRAINT "R007" FOREIGN KEY ("id_roku_akademickiego") REFERENCES "lata_akademickie" ("id_roku_akademickiego") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki" ADD CONSTRAINT "R010" FOREIGN KEY ("id_szablonu") REFERENCES "szablony_praktyk" ("id_szablonu") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki" ADD CONSTRAINT "R015" FOREIGN KEY ("id_praktyki_kierunkowej") REFERENCES "praktyki_kierunkowe" ("id_praktyki_kierunkowej") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki_kierunkowe" ADD CONSTRAINT "R004" FOREIGN KEY ("id_jednostki") REFERENCES "jednostki_kalendarzowe" ("id_jednostki_kalendarzowej") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "koordynatorzy_kierunkowi" ADD CONSTRAINT "R029" FOREIGN KEY ("id_koordynatora_praktyk") REFERENCES "koordynatorzy_praktyk" ("id_koordynatora_praktyk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "koordynatorzy_kierunkowi_praktykodawcow" ADD CONSTRAINT "R030" FOREIGN KEY ("id_kordynatora_kierunkowego") REFERENCES "koordynatorzy_kierunkowi" ("id_kordynatora_kierunkowego") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "koordynatorzy_kierunkowi_praktykodawcow" ADD CONSTRAINT "R033" FOREIGN KEY ("id_praktykodawcy") REFERENCES "praktykodawcy" ("id_praktykodawcy") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "koordynatorzy_praktyk" ADD CONSTRAINT "R028" FOREIGN KEY ("id_osoby") REFERENCES "osoby" ("id_osoby") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "opiekunowie_praktyk" ADD CONSTRAINT "R025" FOREIGN KEY ("id_osoby") REFERENCES "osoby" ("id_osoby") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "studenci" ADD CONSTRAINT "R031" FOREIGN KEY ("id_osoby") REFERENCES "osoby" ("id_osoby") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "opiekunowie_kierunkowi" ADD CONSTRAINT "R023" FOREIGN KEY ("id_praktykodawcy") REFERENCES "praktykodawcy" ("id_praktykodawcy") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "opiekunowie_kierunkowi" ADD CONSTRAINT "R022" FOREIGN KEY ("id_opiekuna_praktyk") REFERENCES "opiekunowie_praktyk" ("id_opiekuna_praktyk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki" ADD CONSTRAINT "R019" FOREIGN KEY ("id_opiekuna_kierunkowego") REFERENCES "opiekunowie_kierunkowi" ("id_opiekuna_kierunkowego") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "uzytkownicy" ADD CONSTRAINT "R039" FOREIGN KEY ("id_osoby") REFERENCES "osoby" ("id_osoby") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "uzytkownicy" ADD CONSTRAINT "R040" FOREIGN KEY ("id_roli") REFERENCES "role" ("id_roli") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "adresy_praktykodawcy" ADD CONSTRAINT "R027" FOREIGN KEY ("id_adresu") REFERENCES "adresy" ("id_adresu") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "adresy_praktykodawcy" ADD CONSTRAINT "R041" FOREIGN KEY ("id_praktykodawcy") REFERENCES "praktykodawcy" ("id_praktykodawcy") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "kierunki_studiow" ADD CONSTRAINT "R043" FOREIGN KEY ("id_stopnia_studiow") REFERENCES "stopnie_studiow" ("id_stopnia_studiow") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "kierunki_studiow" ADD CONSTRAINT "R044" FOREIGN KEY ("id_trybu_studiow") REFERENCES "tryby_studiow" ("id_trybu_studiow") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "kierunki_studiow" ADD CONSTRAINT "R045" FOREIGN KEY ("id_tytulu_zawodowego") REFERENCES "tytuly_zawodowe" ("id_tytulu_zawodowego") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki" ADD CONSTRAINT "R047" FOREIGN KEY ("id_studenta_kierunku") REFERENCES "studenci_kierunkow" ("id_studenta_kierunku") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki" ADD CONSTRAINT "R042" FOREIGN KEY ("id_adresu_praktykodawcy") REFERENCES "adresy_praktykodawcy" ("id_adresu_praktykodawcy") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki" ADD CONSTRAINT "R049" FOREIGN KEY ("id_porozumienia") REFERENCES "porozumienia" ("id_porozumienia") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki" ADD CONSTRAINT "R048" FOREIGN KEY ("id_koordynatora_kierunkowego_praktykodawcy") REFERENCES "koordynatorzy_kierunkowi_praktykodawcow" ("id_koordynatora_kierunkowego_praktykodawcy") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "roczniki_studiow" ADD CONSTRAINT "R034" FOREIGN KEY ("id_roku_akademickiego") REFERENCES "lata_akademickie" ("id_roku_akademickiego") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "studenci_kierunkow" ADD CONSTRAINT "R003" FOREIGN KEY ("id_rocznika") REFERENCES "roczniki_studiow" ("id_rocznika") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "kierunki_studiow" ADD CONSTRAINT "R251" FOREIGN KEY ("id_liczby_semestrow") REFERENCES "liczba_semestrow" ("id_liczby_semestrow") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "koordynatorzy_kierunkowi" ADD CONSTRAINT "R260" FOREIGN KEY ("id_rocznika") REFERENCES "roczniki_studiow" ("id_rocznika") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "praktyki_kierunkowe" ADD CONSTRAINT "R267" FOREIGN KEY ("id_rocznika") REFERENCES "roczniki_studiow" ("id_rocznika") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "rozmowy_kwalifikacyjne" ADD CONSTRAINT "R333" FOREIGN KEY ("id_studenta_kierunku") REFERENCES "studenci_kierunkow" ("id_studenta_kierunku") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "kierunki_studiow" ADD CONSTRAINT "R250" FOREIGN KEY ("id_profilu_ksztalcenia") REFERENCES "profile_ksztalcenia" ("id_profilu_ksztalcenia") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "kierunki_studiow" ADD CONSTRAINT "R259" FOREIGN KEY ("id_specjalnosci") REFERENCES "specjalnosci" ("id_specjalnosci") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "roczniki_studiow" ADD CONSTRAINT "R255" FOREIGN KEY ("id_kierunku") REFERENCES "kierunki_studiow" ("id_kierunku") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "opiekunowie_kierunkowi" ADD CONSTRAINT "R457" FOREIGN KEY ("id_rocznika") REFERENCES "roczniki_studiow" ("id_rocznika") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "rozmowy_kwalifikacyjne" ADD CONSTRAINT "R564" FOREIGN KEY ("id_praktykodawcy") REFERENCES "praktykodawcy" ("id_praktykodawcy") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "wybory_praktyk" ADD CONSTRAINT "R334" FOREIGN KEY ("id_praktyki_studenckiej") REFERENCES "praktyki" ("id_praktyki_studenckiej") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "wybory_praktyk" ADD CONSTRAINT "R445" FOREIGN KEY ("id_studenta_kierunku") REFERENCES "studenci_kierunkow" ("id_studenta_kierunku") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "statusy_praktyk" ADD CONSTRAINT "R673" FOREIGN KEY ("id_statusu") REFERENCES "statusy" ("id_statusu") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "statusy_praktyk" ADD CONSTRAINT "R763" FOREIGN KEY ("id_praktyki_studenckiej") REFERENCES "praktyki" ("id_praktyki_studenckiej") ON DELETE NO ACTION ON UPDATE NO ACTION
;



-- Create roles section -------------------------------------------------

--CREATE ROLE "studenci" LOGIN
----;
--CREATE ROLE "administrator" LOGIN
--;
--CREATE ROLE "zaklad_praktyk" LOGIN
--;
--CREATE ROLE "niezweryfikowany_uzytkownik" LOGIN
--;

-- Grant permissions section -------------------------------------------------


GRANT SELECT ON "praktyki" TO "administrator"
;
GRANT INSERT ON "praktyki" TO "administrator"
;
GRANT UPDATE ON "praktyki" TO "administrator"
;
GRANT DELETE ON "praktyki" TO "administrator"
;
GRANT REFERENCES ON "praktyki" TO "administrator"
;
GRANT TRIGGER ON "praktyki" TO "administrator"
;
GRANT TRUNCATE ON "praktyki" TO "administrator"
;
GRANT SELECT ON "praktyki" TO "zaklad_praktyk"
;
GRANT UPDATE ON "praktyki" TO "zaklad_praktyk"
;
GRANT SELECT ON "praktyki" TO "studenci"
;
GRANT SELECT ON "praktykodawcy" TO "administrator"
;
GRANT UPDATE ON "praktykodawcy" TO "administrator"
;
GRANT DELETE ON "praktykodawcy" TO "administrator"
;
GRANT REFERENCES ON "praktykodawcy" TO "administrator"
;
GRANT TRIGGER ON "praktykodawcy" TO "administrator"
;
GRANT TRUNCATE ON "praktykodawcy" TO "administrator"
;
GRANT INSERT ON "praktykodawcy" TO "administrator"
;
GRANT SELECT ON "praktykodawcy" TO "studenci"
;
GRANT SELECT ON "praktykodawcy" TO "zaklad_praktyk"
;
GRANT UPDATE ON "praktykodawcy" TO "zaklad_praktyk"
;
GRANT SELECT ON "kierunki_studiow" TO "administrator"
;
GRANT INSERT ON "kierunki_studiow" TO "administrator"
;
GRANT UPDATE ON "kierunki_studiow" TO "administrator"
;
GRANT DELETE ON "kierunki_studiow" TO "administrator"
;
GRANT REFERENCES ON "kierunki_studiow" TO "administrator"
;
GRANT TRIGGER ON "kierunki_studiow" TO "administrator"
;
GRANT TRUNCATE ON "kierunki_studiow" TO "administrator"
;
GRANT SELECT ON "kierunki_studiow" TO "studenci"
;
GRANT SELECT ON "kierunki_studiow" TO "zaklad_praktyk"
;
GRANT SELECT ON "studenci" TO "zaklad_praktyk"
;
GRANT SELECT ON "studenci" TO "studenci"
;
GRANT UPDATE ON "studenci" TO "studenci"
;
GRANT TRUNCATE ON "studenci" TO "administrator"
;
GRANT TRIGGER ON "studenci" TO "administrator"
;
GRANT REFERENCES ON "studenci" TO "administrator"
;
GRANT DELETE ON "studenci" TO "administrator"
;
GRANT UPDATE ON "studenci" TO "administrator"
;
GRANT INSERT ON "studenci" TO "administrator"
;
GRANT SELECT ON "studenci" TO "administrator"
;
GRANT SELECT ON "adresy" TO "zaklad_praktyk"
;
GRANT UPDATE ON "adresy" TO "administrator"
;
GRANT INSERT ON "adresy" TO "administrator"
;
GRANT SELECT ON "adresy" TO "administrator"
;
GRANT DELETE ON "adresy" TO "administrator"
;
GRANT REFERENCES ON "adresy" TO "administrator"
;
GRANT TRUNCATE ON "adresy" TO "administrator"
;
GRANT TRIGGER ON "adresy" TO "administrator"
;
GRANT SELECT ON "adresy" TO "studenci"
;
GRANT UPDATE ON "adresy" TO "studenci"
;


