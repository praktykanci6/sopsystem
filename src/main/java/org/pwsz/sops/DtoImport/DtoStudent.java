package org.pwsz.sops.DtoImport;

import com.ebay.xcelite.annotations.Column;
import com.ebay.xcelite.annotations.Row;

/**
 * Created by tifet on 2017-04-23.
 */
@Row(colsOrder = {"nrAlbumu","imie","nazwisko"})
public class DtoStudent {
    @Column(name = "nrAlbumu")
    Long id;
    @Column(name = "imie")
    String firstName;
    @Column(name = "nazwisko")
    String lastName;

    public String getId() {

        return String.valueOf(this.id);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
