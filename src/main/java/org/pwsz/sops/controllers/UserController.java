package org.pwsz.sops.controllers;

import org.pwsz.sops.dto.UserDto;
import org.pwsz.sops.entities.Roles;
import org.pwsz.sops.entities.Users;
import org.pwsz.sops.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * Created by tifet on 2017-04-25.
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/currentUser", method = RequestMethod.GET)
    @ResponseBody
    public UserDto getCurentUser(){
        return new UserDto((Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    @RequestMapping(value = "/userRole", method = RequestMethod.GET,produces = "application/json")
    public Roles getUserRole(){
        Users users = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return users.getRoles();
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity saveUser(@RequestBody UserDto user){
        Users u = userService.updateOrSaveUser(user);
        user = new UserDto(u);
        return new ResponseEntity(u,HttpStatus.OK);
    }
}
