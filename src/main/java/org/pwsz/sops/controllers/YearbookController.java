package org.pwsz.sops.controllers;

import org.pwsz.sops.dto.YearbookDto;
import org.pwsz.sops.entities.Practice;
import org.pwsz.sops.entities.PracticesCourses;
import org.pwsz.sops.entities.PracticesType;
import org.pwsz.sops.entities.Yearbook;
import org.pwsz.sops.repositories.PracticesCoursesRepository;
import org.pwsz.sops.repositories.YearbookRepository;
import org.pwsz.sops.services.YearbookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by tifet on 2017-05-02.
 */
@RestController
public class YearbookController {

    @Autowired
    YearbookRepository yearbookRepository;
    @Autowired
    YearbookService yearbookService;
    @Autowired
    PracticesCoursesRepository practicesCoursesRepository;


    @RequestMapping(value = "/yearbookUpdate", method = RequestMethod.POST)
    public ResponseEntity<?> saveOrUpdateYearbook(@RequestBody YearbookDto yearbookDto){
        Yearbook yearbook = yearbookRepository.findOne(yearbookDto.getId());
        Yearbook yearbook1 = yearbookDto.getYearbook();
        yearbook1.setAcademicYear(yearbook.getAcademicYear());
        yearbook1.setCourseOfStudy(yearbook.getCourseOfStudy());
        yearbookDto.setAcademicYearId(yearbook.getAcademicYear().getId());
        yearbookDto.setCourseOfStudyId(yearbook.getCourseOfStudy().getId());
        yearbook = yearbookRepository.save(yearbook1);
        PracticesCourses practicesCourses ;
        practicesCourses = practicesCoursesRepository.findByAcademicYearIdAnAndYearbookId(yearbookDto.getAcademicYearId(),yearbook.getId());
        if(practicesCourses==null){
            practicesCourses = new PracticesCourses();
        }
        practicesCourses.setNumberHoures(yearbookDto.getHour());
        practicesCourses.setDurtion(yearbookDto.getDay());
        practicesCourses.setAcademicYear(yearbook.getAcademicYear());
        PracticesType practicesType = new PracticesType();
        short s = 1;
        practicesType.setId(s);
        practicesCourses.setPracticesType(practicesType);
        practicesCourses.setYearbook(yearbook);
        practicesCourses = practicesCoursesRepository.save(practicesCourses);
        return new ResponseEntity(yearbookDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/yearbook/coursesList",method = RequestMethod.GET)
    public ResponseEntity<?> getYearbookCoursesList(){
      List<YearbookDto> yearbookDtoList = yearbookService.getYerbookCoursesList();
      return new ResponseEntity(yearbookDtoList,HttpStatus.OK);
    }

    @RequestMapping(value = "/yearbookDeatils", method = RequestMethod.GET)
    public ResponseEntity<?>getCoursesDeatils(@RequestParam("id") Long id){
        YearbookDto yearbookDto = new YearbookDto();
        Yearbook yearbook = yearbookRepository.findOne(id);
        PracticesCourses practicesCourses = practicesCoursesRepository.findByAcademicYearIdAnAndYearbookId(yearbook.getAcademicYear().getId(),id);
        yearbookDto.setId(id);
        yearbookDto.setAcademicYearId(yearbook.getAcademicYear().getId());
        yearbookDto.setCoursesOfStudeyName(yearbook.getCourseOfStudy().getNameCourses());
        yearbookDto.setCoursesOfStudtSepcilaization(yearbook.getCourseOfStudy().getSpecilaization());
        if(practicesCourses!=null){
            yearbookDto.setDay(practicesCourses.getDurtion());
            yearbookDto.setHour(practicesCourses.getNumberHoures());
        }
        yearbookDto.setStartChoice(yearbook.getStartChoice());
        yearbookDto.setEndChoice(yearbook.getEndChoice());
        yearbookDto.setStartInterviews(yearbook.getStartInterviews());
        yearbookDto.setEndInterviews(yearbook.getEndInterviews());
        yearbookDto.setStartPractice(yearbook.getStartPractice());
        yearbookDto.setEndPractice(yearbook.getEndPractice());
        return new ResponseEntity(yearbookDto,HttpStatus.OK);
    }

}
