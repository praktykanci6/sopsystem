package org.pwsz.sops.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.pwsz.sops.dto.AddressDto;
import org.pwsz.sops.dto.EmployerDTO;

import org.pwsz.sops.dto.SimpleEmployerDto;
import org.pwsz.sops.entities.AssistentPractices;
import org.pwsz.sops.entities.Employer;
import org.pwsz.sops.repositories.EmployerRepository;
import org.pwsz.sops.services.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konrad on 14.11.2016.
 */

@RestController
public class EmployerDataController {

    @Autowired
    private EmployerRepository employerRepository;


    @Autowired
    EmployerService employerService;

    @RequestMapping(value = "/get/employer/data", method = RequestMethod.POST)
    public EmployerDTO getEmployerData(@RequestBody Long employerId){
        Employer newEmployer = new Employer();
        newEmployer = employerRepository.findOne(employerId+1);
        EmployerDTO employerDTO = new EmployerDTO();
        if(newEmployer != null){
            employerDTO.setId(String.valueOf(newEmployer.getId()));
            employerDTO.setName(newEmployer.getName());
            return employerDTO;
        }
        return null;
    }

    @RequestMapping(value = "/addAddress", method = RequestMethod.POST,consumes = "application/json")
    public void saveAddress(@RequestBody String data){
            String data1 = data;
//
        EmployerDTO employer = new EmployerDTO();
        JSONObject object= new JSONObject(data);
        String object1 =(String) object.get("data");
        JSONObject object2 = new JSONObject(object1);
        employer.setName(object2.getString("name"));
        employer.setFullName(object2.getString("fullName"));
        employer.setDescription(object2.getString("description"));
        String addresesJson =object2.getJSONArray("addresses").toString();
        ObjectMapper maper = new ObjectMapper();
        List<AddressDto>adress=new ArrayList<>();
        try {
            TypeReference<List<AddressDto>> mapType = new TypeReference<List<AddressDto>>() {};
            adress = maper.readValue(addresesJson,mapType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        employer.setEmployerAddresses(adress);
        try {
            employerService.saveEmployer(employer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public @ResponseBody void handleFileUpload(
            @RequestParam(value="file", required=true) MultipartFile file ,
            @RequestParam(value="person", required=true) String person) {
            System.out.println("nazwa pliku = "+file.getOriginalFilename());
            String name = person;
            System.out.println(person);
    }

    @RequestMapping(value = "/employers",method = RequestMethod.GET)
    public ResponseEntity<?> getEmployers(@RequestParam("id") Long id ){
        List<SimpleEmployerDto>list = new ArrayList<>();
        list = employerService.getSimpleEmployersData(id);
        if(list.size()==0) {
            return new ResponseEntity("Brak praktykodawców",HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity(list, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/addEmployer", method = RequestMethod.POST, consumes = "application/json")
    public HttpStatus getNewEmployerWithCoordinator(@RequestBody String data){
        employerService.addEmployerWithCoordinator(data);
        return HttpStatus.OK;
    }

    @RequestMapping(value = "/employerFullName",method = RequestMethod.GET)
    public ResponseEntity<?> getEmployerFullName(){
        Employer employer = employerService.getEmployerByCurrentUser();
        return new ResponseEntity<Object>(employer.getFullName(),HttpStatus.OK);
    }

    @RequestMapping(value = "/employersForStudent", method = RequestMethod.GET)
    public ResponseEntity getEmployerListToStudent(){
        List<EmployerDTO>list = employerService.getEmployerForStudents();
        if(list.isEmpty()){
            return new ResponseEntity("Nie znależiono praktyk dla twojego kierunku",HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity(list,HttpStatus.OK);
    }
}
