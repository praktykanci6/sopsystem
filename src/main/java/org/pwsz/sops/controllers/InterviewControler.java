package org.pwsz.sops.controllers;

import org.pwsz.sops.dto.Notification;
import org.pwsz.sops.entities.Interview;
import org.pwsz.sops.repositories.InterviewRepository;
import org.pwsz.sops.services.StudenstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tifet on 2017-06-04.
 */
@RestController
public class InterviewControler {
    @Autowired
    StudenstService studenstService;
    @Autowired
    InterviewRepository interviewRepository;

    @RequestMapping(value = "/studentInterview", method = RequestMethod.GET)
    public ResponseEntity<?> getStudentInterviews(){
        List<Interview> list = new ArrayList<>();
        list = studenstService.gestStudentInterviews();
        return new ResponseEntity(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/interviewSave", method = RequestMethod.POST)
    ResponseEntity<?>saveInterview(@RequestBody Interview interview){
        Interview i = interviewRepository.save(interview);
        Notification notification = new Notification("Zapisano zmiany");
        return new ResponseEntity<Object>(notification ,HttpStatus.OK);
    }
}
