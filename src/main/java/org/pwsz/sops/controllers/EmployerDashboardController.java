package org.pwsz.sops.controllers;

import org.pwsz.sops.dto.EmployerDashboardDto;
import org.pwsz.sops.dto.InterviewDto;
import org.pwsz.sops.dto.PracticeDto;
import org.pwsz.sops.dto.YearbookDto;
import org.pwsz.sops.entities.*;
import org.pwsz.sops.repositories.*;
import org.pwsz.sops.services.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by tifet on 2017-05-04.
 */
@RestController
public class EmployerDashboardController {
    @Autowired
    AssistentsPracticeRepository assistentsPracticeRepository;
    @Autowired
    AsistentCoursesRepository asistentCoursesRepository;
    @Autowired
    PracticesCoursesRepository practicesCoursesRepository;
    @Autowired
    PracticeRepository practiceRepository;
    @Autowired
    AcademicYearRepository academicYearRepository;
    @Autowired
    EmployerService employerService;


    @RequestMapping(value = "/getDashboardData", method = RequestMethod.GET)
    public List<EmployerDashboardDto> getDashboardData(){
        return prepareDashboardData();
    }

    /**
     * Przygotowuje dane na strone Głowna Opiekuna Praktykodawcy
     * @return
     */
    private List<EmployerDashboardDto> prepareDashboardData(){
        List<EmployerDashboardDto>list = new ArrayList<>();
        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AssistentPractices assistentPractice = assistentsPracticeRepository.findByPersonId(user.getPerson().getId());
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        List<AssistenCourses>assistenCourses =asistentCoursesRepository.findByPersoneIdAndAcademicYear(assistentPractice.getPerson().getId(),academicYear.getId());
        for(AssistenCourses assistenCourse : assistenCourses){
            Set<Practice>listPractice = new HashSet<>();
            PracticesCourses practicesCourses = practicesCoursesRepository.
                    findByAcademicYearIdAnAndYearbookId(assistenCourse.getYearbook().getAcademicYear().getId(),
                            assistenCourse.getYearbook().getId());
            listPractice = practiceRepository.findByAssistenCoursesId(assistenCourse.getId());
            List<PracticeDto> practiceDtoList = new ArrayList<>();
            for(Practice p : listPractice){
                PracticeDto practiceDto = new PracticeDto();
                Integer count = practiceRepository.countPracticeByStartDateAndEndDateAndEmployerId((Date)p.getStartDate(),
                        (Date) p.getEndDate(),assistenCourse.getEmployer().getId());
                practiceDto.setCounter(count);
                practiceDto.setStar(p.getStartDate());
                practiceDto.setEnd(p.getEndDate());
                practiceDto.setId(p.getId());
                practiceDtoList.add(practiceDto);
                practiceDto.setYearbookId(p.getPracticesCourses().getYearbook().getId());
                practiceDto.setEmployerId(p.getEmployer().getId());
            }
            EmployerDashboardDto dto = new EmployerDashboardDto();
            YearbookDto yearbookDto = new YearbookDto();
            yearbookDto.setStartPractice(assistenCourse.getYearbook().getStartPractice());
            yearbookDto.setEndPractice(assistenCourse.getYearbook().getEndPractice());
            if(practicesCourses != null){
            yearbookDto.setHour(practicesCourses.getNumberHoures());
            yearbookDto.setDay(practicesCourses.getDurtion());
            }
            yearbookDto.setCoursesOfStudeyName(assistenCourse.getYearbook().getCourseOfStudy().getNameCourses());
            yearbookDto.setCoursesOfStudtSepcilaization(assistenCourse.getYearbook().getCourseOfStudy().getSpecilaization());
            yearbookDto.setId(assistenCourse.getYearbook().getId());
            dto.setAssistentId(assistentPractice.getId());
            dto.setEmployerId(assistenCourse.getEmployer().getId());
            dto.setYearbook(yearbookDto);
            dto.setPractices(practiceDtoList);
            list.add(dto);
        }
        return list;
    }

    @RequestMapping(value = "/practiceInterview", method = RequestMethod.GET)
    public ResponseEntity<?> getPracticeInterviewApplication(@RequestParam("id")Long id,@RequestParam("yearbookId")Long yearbooId){
        List<Interview> list = employerService.getPracticeInterview(id,yearbooId);
        return new ResponseEntity<Object>(list, HttpStatus.OK);
    }
}


