package org.pwsz.sops.controllers;

import org.pwsz.sops.dto.Notification;
import org.pwsz.sops.entities.Students;
import org.pwsz.sops.entities.StudentsCourses;
import org.pwsz.sops.repositories.CourseOfStudyRepository;
import org.pwsz.sops.repositories.StudentsCoursesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tifet on 2017-05-02.
 */

@RestController
public class StudentsCoursesCtrl {

    @Autowired
    StudentsCoursesRepository studentsCoursesRepository;

    @RequestMapping(value = "/studentsCourses",method = RequestMethod.GET)
    public ResponseEntity<Students> getStudentsList(@RequestParam(name = "id")Long id){
        List<Students> students = new ArrayList<>();
        students = studentsCoursesRepository.findByYearbookId(id);
        return new ResponseEntity(students, HttpStatus.OK);
    }

    @RequestMapping(value="/studentCoursesDetail", method = RequestMethod.GET)
    public ResponseEntity<?> getStudentCoursesDteail(@RequestParam("id")String studenCourseId, @RequestParam("yearbookId")Long yearbookId){
        StudentsCourses studentsCourses = studentsCoursesRepository.findByYearbookIdAndStudentsNumberAlbum(yearbookId,studenCourseId);
        if(studentsCourses != null){
            return new ResponseEntity<Object>(studentsCourses,HttpStatus.OK);
        }

        return new ResponseEntity<Object>(new Notification("Wystąpil bład serwera"),HttpStatus.BAD_REQUEST);
    }
}
