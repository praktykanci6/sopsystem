package org.pwsz.sops.controllers;

import org.pwsz.sops.entities.Address;
import org.pwsz.sops.services.EmployerAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tifet on 2017-03-25.
 */
@RestController
public class AddressCtr {

    @Autowired
    EmployerAddressService employerAddressService;

    @RequestMapping(value = "/employerAddress", method = RequestMethod.GET)
    public ResponseEntity<Address>getEmployerAddres(@Param("id") Long id){
        List<Address> employerAddresses = new ArrayList<>();
        employerAddresses =  employerAddressService.getEmployerAddres(id);
        return new ResponseEntity(employerAddresses, HttpStatus.OK);
    }
}
