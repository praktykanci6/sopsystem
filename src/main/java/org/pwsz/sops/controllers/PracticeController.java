package org.pwsz.sops.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.pwsz.sops.dto.Notification;
import org.pwsz.sops.dto.PracticeDto;
import org.pwsz.sops.dto.ReportDto;
import org.pwsz.sops.entities.Practice;
import org.pwsz.sops.services.PracticeService;
import org.pwsz.sops.services.StudenstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by tifet on 2017-05-04.
 */
@RestController
public class PracticeController {
    @Autowired
    PracticeService practiceService;
    @Autowired
    StudenstService studenstService;

    @RequestMapping(value = "/practicesEmployer", method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<?>getEmployerPractice(@RequestParam("id")Long id){
        Set<PracticeDto> practices = new HashSet<>();
        practices = practiceService.getEmployerPracitce(id);
        return new ResponseEntity(practices, HttpStatus.OK);
    }

    @RequestMapping(value = "/addPractice", method = RequestMethod.POST)
    public ResponseEntity<?> addPractice(@RequestBody PracticeDto practice){
        List<Practice> practice1= practiceService.addPractice(practice);
        return new ResponseEntity<Object>(practice1,HttpStatus.OK);
    }

    @RequestMapping(value = "/addOwenPractice",method = RequestMethod.POST)
    public ResponseEntity<?> addOwenPractice(@RequestParam(value="file") MultipartFile multipartFile, @RequestParam String data,
                                             @RequestParam(value = "id")Long yearbookId){
        try {
            ReportDto object  = convertJsonTOObject(data);
            Practice practice = practiceService.addOwnPractice(object,yearbookId,multipartFile);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @RequestMapping(value = "/getTemplate",method = RequestMethod.GET)
    public ResponseEntity<?>getTemplatePractice(@Param(value = "id")Long id){
        Practice practice = practiceService.getPracticeById(id);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/octet-stream"));
        headers.add("content-disposition", "inline;filename=" + "KartaZgloszeniaZakladuPracy.pdf");
        ByteArrayResource resource = null;
        resource = new ByteArrayResource(practice.getReferrals());
        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }


    @RequestMapping(value = "/addAgreement", method = RequestMethod.POST)
    public ResponseEntity<?> addStudentAgreement(@RequestParam(value="file") MultipartFile multipartFile, @RequestParam String data,
                                                 @RequestParam(value = "id")Long yearbookId){
        Practice practice = null;
        try {
            ReportDto reportDto = convertJsonTOObject(data);
            try {
                practice = practiceService.addStudentAgreement(reportDto, yearbookId,multipartFile);
            } catch (Exception e) {
                e.printStackTrace();
                new ResponseEntity<>(HttpStatus.OK);
            }

        } catch (IOException e) {
            new ResponseEntity<>(HttpStatus.OK);
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/addStudentApplication", method = RequestMethod.POST)
    public ResponseEntity<?> addStudentApplication(@RequestBody PracticeDto practiceDto){
        Notification notification = studenstService.addStudentApplication(practiceDto);
        return new ResponseEntity(notification,HttpStatus.OK);
    }
    ReportDto convertJsonTOObject(String s) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ReportDto dto = mapper.readValue(s,ReportDto.class);
        return dto;
    }
}
