package org.pwsz.sops.controllers;

import fr.opensagres.xdocreport.core.XDocReportException;
import org.pwsz.sops.dto.ReportDto;
import org.pwsz.sops.entities.AcademicYear;
import org.pwsz.sops.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author $k.kabat
 */
@RestController
public class ReportController {
    
    private static final String REPORT1 = "Karta oświadczenia zakładu pracy";
    private static final String REPORT2 = "Karta zgłoszenia umowy";
    private static final String REPORT3 = "Karta zgłoszenia zakładu pracy";

    List<String> reports = Arrays.asList(REPORT1, REPORT2, REPORT3);

    @Autowired
    private ReportService reportService;

    @RequestMapping(value = "/availableReports", method = RequestMethod.GET)
    private List<String> getAvailableReports() {
        return reports;
    }

    @RequestMapping(value = "/generateReport", method = RequestMethod.GET)
    public ResponseEntity<?> downloadPDFFile(@RequestParam(name = "id") String id) {
        try {
            ByteArrayOutputStream out = null;
            System.out.println("\n********** Download PDF File : ************\n");
            AcademicYear academicYear = new AcademicYear();
            academicYear.setName("rok akademicki");
            academicYear.setStart(new Date());
            academicYear.setEnd(new Date());
            /**
             * TODO mozna zamiast id nazwy raportu przekazywac z widoku i tu sprawdzac nazw w case
             */
            String fileName = "";
            switch (id) {
                case REPORT1:
                    out = reportService.getWorkPlaceDeclarationReport(academicYear, new ArrayList<>());
                    fileName = "OświadczenieZakładuPracy.pdf";
                    break;
                case REPORT2:
                    out = reportService.getAgreementApplicationReport();
                    fileName = "ZgłoszenieUmowy.pdf";
                    break;
                case REPORT3:
                    out = reportService.getWorkPlaceApplication();
                    fileName = "ZgłoszenieZakładuPracy.pdf";
                    break;
            }

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/octet-stream"));
            headers.add("content-disposition", "inline;filename=" + fileName);
//        File file = FileUtils.getFile("C:\\Users\\tifet\\Documents\\AngularJS. Profesjonalne techniki - Adam Freeman [HQ].pdf");
//        FileSystemResource fileSystemResource = new FileSystemResource(file);
            ByteArrayResource resource = null;
//        Path path = Paths.get(file.getAbsolutePath());
            resource = new ByteArrayResource(out.toByteArray());
            return new ResponseEntity<>(resource, headers, HttpStatus.OK);
        } catch (IOException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XDocReportException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @RequestMapping(value = "/aPT1", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> getAddPracticeTemplate(@RequestBody ReportDto data){

        try {
            ByteArrayOutputStream out = null;
            System.out.println("\n********** Download PDF File : ************\n");
            out = reportService.getWorkPlaceApplicationReport(data);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/octet-stream"));
            headers.add("content-disposition", "inline;filename=" + "KartaZgloszeniaZakladuPracy.pdf");
            ByteArrayResource resource = null;
            resource = new ByteArrayResource(out.toByteArray());
            return new ResponseEntity<>(resource, headers, HttpStatus.OK);
        } catch (IOException ex) {

            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<Object>("Wystąpił nieoczekiwany bład serwera",HttpStatus.BAD_REQUEST);

        } catch (XDocReportException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<Object>("Wystąpił nieoczekiwany bład serwera",HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/aPT2", method = RequestMethod.POST, consumes = "application/json")
    public HttpEntity<?> getAgreementApplicationTemplate(@RequestBody ReportDto data){

        try {
            ByteArrayOutputStream out = null;
            System.out.println("\n********** Download PDF File : ************\n");
            out = reportService.getAgreementApplicationReport(data);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/octet-stream"));
            headers.add("content-disposition", "inline;filename=" + "ZgloszenieUmowy.pdf");
            ByteArrayResource resource = null;
            resource = new ByteArrayResource(out.toByteArray());
            return new ResponseEntity<>(resource, headers, HttpStatus.OK);
        } catch (IOException | XDocReportException ex) {

            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<Object>("Wystąpił nieoczekiwany bład serwera",HttpStatus.BAD_REQUEST);

        }
    }


}
