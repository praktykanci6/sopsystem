package org.pwsz.sops.controllers;

import org.pwsz.sops.dto.EmployerDTO;
import org.pwsz.sops.entities.Employer;
import org.pwsz.sops.repositories.EmployerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

//import org.pwsz.sops.repositories.PracticeRepository;

/**
 * Created by k.kabat on 01.11.2016.
 */
@RestController
public class PracticeListController {

//    @Autowired
//    protected PracticeRepository practiceRepository;

    @Autowired
    protected EmployerRepository employerRepository;

    @RequestMapping(value = "/list_of_practices", method = RequestMethod.GET)
    @ResponseBody
    public List<EmployerDTO> getEmployerList() {
        List<Employer> employerList = employerRepository.findAll();
        List<EmployerDTO> returnList = new ArrayList<>();
        for (Employer e : employerList) {
            EmployerDTO temp = new EmployerDTO();
            temp.setId(String.valueOf(e.getId()));
            temp.setName(String.valueOf(e.getName()));

            returnList.add(temp);
        }
        return returnList;
    }


}
