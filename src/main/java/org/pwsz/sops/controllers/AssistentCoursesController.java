package org.pwsz.sops.controllers;

import org.pwsz.sops.dto.AssistantCourseDto;
import org.pwsz.sops.dto.AssistantPracticeDto;
import org.pwsz.sops.dto.UserDto;
import org.pwsz.sops.dto.YearbookDto;
import org.pwsz.sops.entities.AssistenCourses;
import org.pwsz.sops.entities.AssistentPractices;
import org.pwsz.sops.repositories.AsistentCoursesRepository;
import org.pwsz.sops.repositories.AssistentsPracticeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by tifet on 2017-05-03.
 */
@RestController
public class AssistentCoursesController {

    @Autowired
    AsistentCoursesRepository asistentCoursesRepository;
    @Autowired
    AssistentsPracticeRepository assistentsPracticeRepository;

    @RequestMapping(value = "/assistantCourseByEmployer", method = RequestMethod.GET)
    public List<AssistantPracticeDto> getAssistantCoursesByEmployer(@RequestParam("id") Long id){
        Set<AssistentPractices> assistentPractices = new HashSet<>();
        List<AssistantPracticeDto> list = new ArrayList<>();
        assistentPractices = assistentsPracticeRepository.findByEmployerId(id);
        for(AssistentPractices assistentPractices1 :assistentPractices){
            UserDto userDto = new UserDto();
            userDto.setEmail(assistentPractices1.getPerson().getEmail());
            userDto.setPhone(assistentPractices1.getPerson().getPhone());
            userDto.setDegree(assistentPractices1.getPerson().getDegree());
            userDto.setPhone2(assistentPractices1.getPerson().getMobilePhone());
            userDto.setFirstName(assistentPractices1.getPerson().getFirstName());
            userDto.setLastName(assistentPractices1.getPerson().getLastName());
            userDto.setPosition(assistentPractices1.getPosition());
            AssistantPracticeDto assistantPracticeDto = new AssistantPracticeDto();
            assistantPracticeDto.setUserDto(userDto);
            assistantPracticeDto.setId(assistentPractices1.getId());
            List<YearbookDto>yearbookDtos = new ArrayList<>();
            for(AssistenCourses assistenCourses : assistentPractices1.getAssistenCourses()){
                YearbookDto yearbookDto = new YearbookDto();
                yearbookDto.setCoursesOfStudeyName(assistenCourses.getYearbook().getCourseOfStudy().getNameCourses());
                yearbookDto.setCoursesOfStudtSepcilaization(assistenCourses.getYearbook().getCourseOfStudy().getSpecilaization());
                yearbookDto.setId(assistenCourses.getYearbook().getId());
                yearbookDtos.add(yearbookDto);
            }
            assistantPracticeDto.setYearbookDtoList(yearbookDtos);
            list.add(assistantPracticeDto);
        }
        return list;
    }

}
