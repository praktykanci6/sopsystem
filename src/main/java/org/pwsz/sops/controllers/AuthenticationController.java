package org.pwsz.sops.controllers;

import org.pwsz.sops.dto.UserAuthenticationDTO;
import org.pwsz.sops.entities.Users;
import org.pwsz.sops.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Konrad on 10.11.2016.
 */
@RestController
public class AuthenticationController {

    private static Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    protected UserRepository userRepository;

    @RequestMapping("/user/authentication")
    public UserAuthenticationDTO getUserAuthorization(){

        Users user = null;

        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            user = (Users) auth.getPrincipal();
        }catch(Exception ex){
            logger.error(ex.getMessage(),ex);
        }

        UserAuthenticationDTO userAuthenticationDTO = new UserAuthenticationDTO(user);
        return userAuthenticationDTO;
    }
}
