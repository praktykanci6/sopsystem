package org.pwsz.sops.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.pwsz.sops.dto.AcademicYearDto;
import org.pwsz.sops.entities.AcademicYear;
import org.pwsz.sops.entities.CoursesOfStudy;
import org.pwsz.sops.entities.Yearbook;
import org.pwsz.sops.repositories.AcademicYearRepository;
import org.pwsz.sops.services.YearbookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by tifet on 2017-04-08.
 */
@RestController
public class AcademicYearController {

    @Autowired
    YearbookService yearbookService;

    @Autowired
    AcademicYearRepository academicYearRepository;

    /**
     *  Tworzy rok akademickie wraz z rocznikami
     * @param data adne roku akademickiego plus kierunki
     * @return id utworzonego roku akademickiego roku akademickiego
     */
    @RequestMapping(value = "/addAcademicYear", method = RequestMethod.POST, consumes = "application/json")
    public Integer saveAcademicYear(@RequestBody String data){
        Integer id = null;
        String data1 = data;

        //pobiera danych z Json object
        AcademicYear academicYear = new AcademicYear();
        JSONObject object = new JSONObject(data);
        String object1 = (String) object.get("data");
        JSONObject object2 = new JSONObject(object1);
        String object3 = (String) object2.get("academicYear").toString();
        JSONObject academicYearModel = new JSONObject(object3);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String start = academicYearModel.getString("startDate");
        String end = academicYearModel.getString("endDate");
        Date endDate = null;
        Date startDate= null;
        try {
            startDate = sdf.parse(start);
            endDate = sdf.parse(end);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        academicYear.setName(academicYearModel.getString("name"));
        academicYear.setStart(startDate);
        academicYear.setEnd(endDate);
//        academicYear = academicYearRepository.save(academicYear);
        String coursesOfStudyObject =(String) object2.getJSONArray("coursesOfStudy").toString();
        List<Long> coursesList = new ArrayList<>();
        List<Yearbook> yearbooks = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();

        TypeReference<List<Long>> mapType = new TypeReference<List<Long>>(){};
        try {
            coursesList = mapper.readValue(coursesOfStudyObject,mapType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(Long coursesId : coursesList){
            Yearbook yearbook = new Yearbook();
            CoursesOfStudy coursesOfStudy = new CoursesOfStudy();
            coursesOfStudy.setId(coursesId);
            yearbook.setCourseOfStudy(coursesOfStudy);
            yearbooks.add(yearbook);
        }
        //zapis roku oraz roczikow z podstawowymi danymi id roku oraz id kierunków
        try {
          id=  yearbookService.addConfigurationAcademicYear(academicYear,yearbooks);
        } catch (Exception e) {
            e.printStackTrace();
        }
        yearbooks.size();
        return id;
    }

    @RequestMapping(value = "/getActualAcademicYear")
    public ResponseEntity<?> getActualAcademicYear(){
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        AcademicYearDto dto = new AcademicYearDto();
        dto.setId(academicYear.getId());
        dto.setName(academicYear.getName());
        dto.setStart(academicYear.getStart());
        dto.setEnd(academicYear.getEnd());
        return new ResponseEntity(dto, HttpStatus.OK);
    }
}
