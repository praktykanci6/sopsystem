package org.pwsz.sops.controllers;

import org.pwsz.sops.dto.CoordinatorCoursesDto;
import org.pwsz.sops.dto.UserDto;
import org.pwsz.sops.services.CoordinatroCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by tifet on 2017-05-02.
 */
@RestController
public class CoordinatorCourseController {

    @Autowired
    CoordinatroCourseService coordinatroCourseService;


    @RequestMapping(value = "/addCoordinatorCourse", method = RequestMethod.POST)
    public ResponseEntity<?> addOrUpdateCoordinator(@RequestBody CoordinatorCoursesDto coursesDto){
        coursesDto = coordinatroCourseService.addCoordinatorCourse(coursesDto);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/coordinatorCourseList", method = RequestMethod.GET)
    public ResponseEntity<?>getCoordinatorCourseList(@RequestParam("id") Long id){
        List<UserDto>userDtoList = coordinatroCourseService.getCoordinatorCoursesList(id);
        return new ResponseEntity(userDtoList,HttpStatus.OK);
    }
}
