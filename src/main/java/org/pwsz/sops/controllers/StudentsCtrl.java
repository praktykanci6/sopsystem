package org.pwsz.sops.controllers;

import org.pwsz.sops.dto.PracticeDto;
import org.pwsz.sops.dto.YearbookDto;
import org.pwsz.sops.entities.Interview;
import org.pwsz.sops.entities.Practice;
import org.pwsz.sops.entities.Students;
import org.pwsz.sops.entities.StudentsCourses;
import org.pwsz.sops.services.StudenstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tifet on 2017-04-23.
 */

@RestController
public class StudentsCtrl {


    @Autowired
    private StudenstService studentService;

    /**
     * Import studentów xslx
     * return studenci kierunku
     */
    @RequestMapping(value = "/importStudentsXls", method = RequestMethod.POST ,produces = "application/json")
    public ResponseEntity<?> getStudentsFromFile(@RequestParam(value = "file" , required = true)MultipartFile multipartFile,
                                              @RequestParam(value = "id") Long yearId) throws IOException {
        File xslFile = new File( "C:\\Users\\tifet\\Documents\\file\\"+multipartFile.getOriginalFilename());
        List<StudentsCourses> studentsList = new ArrayList<>();
        List<Students> studentsList1 = new ArrayList<>();
        try {
            multipartFile.transferTo(xslFile);
          studentsList =  studentService.getStudentsFromXsl(xslFile, yearId);
          for(StudentsCourses sc : studentsList){
              Students students = sc.getStudents();
              studentsList1.add(students);
          }
        } catch (IOException e) {
           e.getLocalizedMessage();
       } catch (Exception e) {
           e.getLocalizedMessage();
        }
        return  new ResponseEntity(studentsList1,HttpStatus.OK);

    }

    @RequestMapping(value = "/studentCourses",method = RequestMethod.GET)
    public ResponseEntity<?>getStudentCourses(){
        List<YearbookDto> list = new ArrayList<>();
        list = studentService.getsStudentCourses();
        return new ResponseEntity<Object>(list,HttpStatus.OK);
    }

    @RequestMapping(value = "/studentPractice", method = RequestMethod.GET)
    public ResponseEntity<?>getStudentPractice(){
        List<Practice> list = studentService.getStudetnPractice();
        return new ResponseEntity<Object>(list,HttpStatus.OK);
    }
//    @RequestMapping(value = "/studentInterview",method = RequestMethod.GET)
//    public ResponseEntity<?>getStudentInterview(){
//        List<Interview> list = new ArrayList<>();
//        list.addAll(studentService.getInterviewsStudent());
//        return new ResponseEntity<Object>(list,HttpStatus.OK);
//    }

    @RequestMapping(value = "/studentApplication",method = RequestMethod.GET)
    public ResponseEntity<?> gestStudentApplicationes(){
        List<PracticeDto>list = studentService.getstudentApplicationes();
        return new ResponseEntity<Object>(list,HttpStatus.OK);
    }
}
