package org.pwsz.sops.egeriaEntities;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name = "osoby")
public class EegeriaData implements Serializable{
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String firstName;
    @Column(name="surname")
    private String lastName;
    @Column(name="pesel")
    private String pesel;
    @Email
    @Column(name = "email")
    private String email;
    @Column(name="city")
    private String city;
    @Column(name = "poczta")
    private String  poczta;
    @Column(name="zipcode")
    private String zipcode;
    @Column(name="street")
    private String street;
    @Column(name="home_number")
    private Integer homeNumber;
    @Column(name="flat_number")
    private Integer flatNumber;
    @Column(name = "phone1")
    private String phone1;
    @Column(name="phone2")
    private String phone2;
    @Column(name="index_number")
    private String indexNumber;
    @Column(name = "institute")
    private String institute;
    @Column(name = "course")
    private String course;
    @Column(name="specialization")
    private String specialization;
    @Column(name="start_year")
    private Integer startYear;
    @Column(name = "end_year")
    private Integer endYear;
    @Column(name = "stude_mode")
    private String studyMode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPoczta() {
        return poczta;
    }

    public void setPoczta(String poczta) {
        this.poczta = poczta;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(Integer homeNumber) {
        this.homeNumber = homeNumber;
    }

    public Integer getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(Integer flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(String indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Integer getStartYear() {
        return startYear;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public Integer getEndYear() {
        return endYear;
    }

    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    public String getStudyMode() {
        return studyMode;
    }

    public void setStudyMode(String studyMode) {
        this.studyMode = studyMode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EegeriaData)) return false;

        EegeriaData that = (EegeriaData) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (pesel != null ? !pesel.equals(that.pesel) : that.pesel != null) return false;
        if (!email.equals(that.email)) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (poczta != null ? !poczta.equals(that.poczta) : that.poczta != null) return false;
        if (zipcode != null ? !zipcode.equals(that.zipcode) : that.zipcode != null) return false;
        if (street != null ? !street.equals(that.street) : that.street != null) return false;
        if (homeNumber != null ? !homeNumber.equals(that.homeNumber) : that.homeNumber != null) return false;
        if (flatNumber != null ? !flatNumber.equals(that.flatNumber) : that.flatNumber != null) return false;
        if (phone1 != null ? !phone1.equals(that.phone1) : that.phone1 != null) return false;
        if (phone2 != null ? !phone2.equals(that.phone2) : that.phone2 != null) return false;
        if (!indexNumber.equals(that.indexNumber)) return false;
        if (!institute.equals(that.institute)) return false;
        if (!course.equals(that.course)) return false;
        if (specialization != null ? !specialization.equals(that.specialization) : that.specialization != null)
            return false;
        if (startYear != null ? !startYear.equals(that.startYear) : that.startYear != null) return false;
        if (endYear != null ? !endYear.equals(that.endYear) : that.endYear != null) return false;
        return studyMode.equals(that.studyMode);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (pesel != null ? pesel.hashCode() : 0);
        result = 31 * result + email.hashCode();
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (poczta != null ? poczta.hashCode() : 0);
        result = 31 * result + (zipcode != null ? zipcode.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (homeNumber != null ? homeNumber.hashCode() : 0);
        result = 31 * result + (flatNumber != null ? flatNumber.hashCode() : 0);
        result = 31 * result + (phone1 != null ? phone1.hashCode() : 0);
        result = 31 * result + (phone2 != null ? phone2.hashCode() : 0);
        result = 31 * result + indexNumber.hashCode();
        result = 31 * result + institute.hashCode();
        result = 31 * result + course.hashCode();
        result = 31 * result + (specialization != null ? specialization.hashCode() : 0);
        result = 31 * result + (startYear != null ? startYear.hashCode() : 0);
        result = 31 * result + (endYear != null ? endYear.hashCode() : 0);
        result = 31 * result + studyMode.hashCode();
        return result;
    }
}
