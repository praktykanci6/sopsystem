package org.pwsz.sops.services;

import org.pwsz.sops.dto.PracticeDto;
import org.pwsz.sops.dto.ReportDto;
import org.pwsz.sops.entities.Practice;
import org.pwsz.sops.entities.Users;
import org.springframework.security.access.method.P;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

/**
 * Created by tifet on 2017-05-04.
 */
public interface PracticeService {
    Set<PracticeDto> getEmployerPracitce(Long employerId);

    List<Practice> addPractice(PracticeDto practice);

    Practice addOwnPractice(ReportDto data, Long yearbookId, MultipartFile file) throws Exception;

    List<Practice> getStudentPractice(Users user);

    List<PracticeDto> getStudentAvaliblePractice();

    public Practice getPracticeById(Long practiceId);

    List<Practice> getEmployerPractice(Long practiceId);

    public Practice addStudentAgreement(ReportDto data, Long yearbookId, MultipartFile file) throws Exception;



}
