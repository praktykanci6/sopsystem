package org.pwsz.sops.services;

import org.pwsz.sops.dto.UserDto;
import org.pwsz.sops.entities.Roles;
import org.pwsz.sops.entities.Users;

/**
 * Created by tifet on 2017-04-25.
 */
public interface UserService {
    public UserDto getCurrentUsers();

    public Users updateOrSaveUser(UserDto user);

    public Users adduserWithPersone(UserDto user, Roles roles);
}
