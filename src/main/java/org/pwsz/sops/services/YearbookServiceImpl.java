package org.pwsz.sops.services;

import org.pwsz.sops.dto.YearbookDto;
import org.pwsz.sops.entities.AcademicYear;
import org.pwsz.sops.entities.Yearbook;
import org.pwsz.sops.repositories.AcademicYearRepository;
import org.pwsz.sops.repositories.YearbookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tifet on 2017-04-09.
 */
@Service
public class YearbookServiceImpl implements YearbookService{

    @Autowired
    private AcademicYearRepository academicYearRepository;
    @Autowired
    private YearbookRepository yearbookRepository;

    @Override
    @Transactional
    public Integer addConfigurationAcademicYear(AcademicYear academicYear, List<Yearbook> yearbooks) throws Exception {

        academicYear = academicYearRepository.save(academicYear);
        for(Yearbook yearbook : yearbooks){
            yearbook.setAcademicYear(academicYear);
        }
        yearbookRepository.save(yearbooks);
        return academicYear.getId();
    }

    @Override
    public List<YearbookDto> getYerbookCoursesList() {
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        List<Yearbook>yearbooks = new ArrayList<>();
        List<YearbookDto> yearbookDtos = new ArrayList<>();
        yearbooks = yearbookRepository.findByAcademicYearId(academicYear.getId());
        for (Yearbook yearbook : yearbooks){
            YearbookDto dto = new YearbookDto();
            dto.setId(yearbook.getId());
            dto.setAcademicYearId(academicYear.getId());
            dto.setCourseOfStudyId(yearbook.getCourseOfStudy().getId());
            dto.setCoursesOfStudeyName(yearbook.getCourseOfStudy().getNameCourses());
            dto.setCoursesOfStudtSepcilaization(yearbook.getCourseOfStudy().getSpecilaization());
            yearbookDtos.add(dto);
        }
        return yearbookDtos;
    }

    @Override
    public Yearbook getYearbookById(Long id) {
        return yearbookRepository.findOne(id);
    }
}
