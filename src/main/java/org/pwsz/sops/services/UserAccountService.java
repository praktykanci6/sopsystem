package org.pwsz.sops.services;

import com.unboundid.ldap.sdk.migrate.ldapjdk.LDAPConnection;
import org.pwsz.sops.entities.Students;
import org.pwsz.sops.entities.Users;

import org.pwsz.sops.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.jws.WebService;
import java.util.List;

/**
 * Created by Konrad on 14.10.2016.
 */
@Service
public class UserAccountService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        SecurityContextHolder.getContext().getAuthentication();
        LDAPConnection connection = new LDAPConnection();
        final Users user = userRepository.findOneByLogin(login);
//        List<Students>studentses = studentsRepository.findAll();
        if(user == null)
            throw new UsernameNotFoundException("No user with login " + login);
        return user;
    }
}
