package org.pwsz.sops.services;

import org.pwsz.sops.dto.EmployerDTO;
import org.pwsz.sops.dto.InterviewDto;
import org.pwsz.sops.dto.SimpleEmployerDto;
import org.pwsz.sops.entities.Employer;
import org.pwsz.sops.entities.Interview;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * Created by tifet on 2017-03-25.
 */
public interface EmployerService {

    Employer saveEmployer(EmployerDTO employerDTO)throws Exception;
    List<SimpleEmployerDto> getSimpleEmployersData(Long id);
    boolean addEmployerWithCoordinator(String data);
    Employer getEmployerByCurrentUser();
    List<EmployerDTO> getEmployerForStudents();
    List<Interview>getPracticeInterview(Long employerId, Long yearbookId);
}
