package org.pwsz.sops.services;

import org.pwsz.sops.dto.CoordinatorCoursesDto;
import org.pwsz.sops.dto.UserDto;

import java.util.List;

/**
 * Created by tifet on 2017-05-02.
 */
public interface CoordinatroCourseService {

    CoordinatorCoursesDto addCoordinatorCourse(CoordinatorCoursesDto coursesDto);
    List<UserDto> getCoordinatorCoursesList(Long yearbookId);

}
