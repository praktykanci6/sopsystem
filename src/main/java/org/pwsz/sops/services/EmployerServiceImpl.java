package org.pwsz.sops.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.catalina.User;
import org.pwsz.sops.dto.*;
import org.pwsz.sops.entities.*;
import org.pwsz.sops.entities.Dictionary.InterviewStatus;
import org.pwsz.sops.entities.Dictionary.PracticeType;
import org.pwsz.sops.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

/**
 * Created by tifet on 2017-03-25.
 */
@ComponentScan
@Service("employerService")
public class EmployerServiceImpl implements EmployerService {
    @Autowired
    EmployerRepository employerRepository;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    EmployAddressRepository employAddressRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    AssistentsPracticeRepository assistentsPracticeRepository;
    @Autowired
    AsistentCoursesRepository asistentCoursesRepository;
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AcademicYearRepository academicYearRepository;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    StudentsCoursesRepository studentsCoursesRepository;
    @Autowired
    PracticeRepository practiceRepository;
    @Autowired
    InterviewRepository interviewRepository;

    @Override
    @Transactional("defaultTransactionManager")
    public Employer saveEmployer(EmployerDTO employerDTO) throws Exception {
        Address mainAddress = null;
        List<Address> departamentAdresses = new ArrayList<>();
        List<EmployAddress> employAddressList = new ArrayList<>();
        Employer employer = new Employer();
        employer.setName(employerDTO.getName());
        employer.setFullName(employerDTO.getFullName());
        employer.setDescription(employerDTO.getDescription());
        employer.setConfidental(true);
        employer = employerRepository.save(employer);
        for (AddressDto address : employerDTO.getEmployerAddresses()) {
            Address address1 = new Address();
            address1.setCity(address.getCity());
            address1.setCountry(address.getCountry());
            address1.setFax(address.getFax());
            address1.setNumberBuilding(address.getNumberBuilding());
            address1.setPhone1(address.getPhone1());
            address1.setPhone2(address.getPhone2());
            address1.setStreet(address.getStreet());
            address1.setPostCode(address.getPostCode());
            Address address2 = addressRepository.findByStreetAndNumberBuildingAndCityAndCountryAndPostCodeAndPhone1AndPhone2AndFax(address1.getStreet(),
                    address1.getNumberBuilding(), address1.getCity(), address1.getCountry(), address1.getPostCode(),
                    address1.getPhone1(), address1.getPhone2(), address1.getFax());
            if (!address.isDepartament()) {
                if (address2 != null) {
                    mainAddress = address2;
                } else {
                    mainAddress = addressRepository.save(address1);
                }
            }
            if (address.isDepartament()) {
                if (address2 != null) {
                    departamentAdresses.add(address2);
                } else {
                    departamentAdresses.add(addressRepository.save(address1));
                }
            }
        }
        EmployAddress employAddressMain = new EmployAddress();
        employAddressMain.setAddress(mainAddress);
        employAddressMain.setEmployer(employer);
        employAddressMain.setBranch(false);
        employAddressMain = employAddressRepository.save(employAddressMain);
        employAddressList.add(employAddressMain);
        for (Address address : departamentAdresses) {
            EmployAddress address12 = new EmployAddress();
            address12.setEmployer(employer);
            address12.setAddress(address);
            address12.setBranch(true);
            employAddressList.add(employAddressRepository.save(address12));

        }
        employer.setAddresses(employAddressList);
        return employer;
    }

    @Override
    public List<SimpleEmployerDto> getSimpleEmployersData(Long id) {
        Set<Employer> employerList = new HashSet<>();
        List<SimpleEmployerDto> simpleEmployerDtoList = new ArrayList<>();
        employerList = employerRepository.findByYearbooId(id);
        for (Employer employer : employerList) {
            SimpleEmployerDto dto = new SimpleEmployerDto();
            dto.setId(employer.getId());
            dto.setFullName(employer.getFullName());
            dto.setName(employer.getName());
            simpleEmployerDtoList.add(dto);
        }
        return simpleEmployerDtoList;
    }

    @Override
    public boolean addEmployerWithCoordinator(String data) {
        EmployerDTO employer = new EmployerDTO();
        List<AddressDto> addressDtos = new ArrayList<>();
        UserDto userDto = new UserDto();
        JSONObject object = new JSONObject(data);
        String object1 = (String) object.get("data");
        JSONObject object2 = new JSONObject(object1);
        String jsonC = object2.get("coordinator").toString();
        JSONObject objectC = new JSONObject(jsonC);
        String jsonCourses = object2.get("courses").toString();
        String objec3 = object2.get("employerData").toString();
        JSONObject objectE = new JSONObject(objec3);
        employer.setName(objectE.getString("name"));
        employer.setFullName(objectE.getString("fullName"));
        employer.setDescription(objectE.getString("description"));
        String addresesJson = objectE.getJSONArray("addresses").toString();
        ObjectMapper maper = new ObjectMapper();
        List<AddressDto> adress = new ArrayList<>();
        List<Long> coursesId = new ArrayList<>();
        try {
            TypeReference<List<AddressDto>> mapType = new TypeReference<List<AddressDto>>() {
            };
            adress = maper.readValue(addresesJson, mapType);
            TypeReference<UserDto> mapType1 = new TypeReference<UserDto>() {
            };
            userDto = maper.readValue(jsonC, mapType1);
            TypeReference<List<Long>> mapType2 = new TypeReference<List<Long>>() {
            };
            coursesId = maper.readValue(jsonCourses, mapType2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        employer.setEmployerAddresses(adress);
        try {
            Employer employer1 = saveEmployer(employer);
            List<AssistenCourses> list = addAssistentCourse(userDto, employer1.getId(), coursesId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Employer getEmployerByCurrentUser() {
        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        List<AssistenCourses> assistent = asistentCoursesRepository.findByPersoneIdAndAcademicYear(user.getPerson().getId(),academicYear.getId());
        return !assistent.isEmpty() ? assistent.get(0).getEmployer() : new Employer() ;
    }

    /**
     * Pobieranie, oferujacych praktyki dla studenta
     * @return
     */
    @Override
    public List<EmployerDTO> getEmployerForStudents() {
        List<Employer>employerList = new ArrayList<>();
        List<EmployerDTO>dtoList = new ArrayList<>();
        Users user = (Users) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        Students student = studentRepository.findByPersonId(user.getPerson().getId());
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        List<StudentsCourses> studentsCourses = studentsCoursesRepository
                .getByAcademicYearIdAndStudentId(academicYear.getId(),student.getNumberAlbum());
        for(StudentsCourses studentsCourses1:  studentsCourses ){
            if(studentsCourses1.getYearbook().getStartChoice().before(new Date()) && studentsCourses1.getYearbook().getEndChoice().after(new Date()) ) {
                employerList.addAll(practiceRepository.getEmployerToStudent(studentsCourses1.getYearbook().getId(), false, PracticeType.SCHOOL));
                employerList.addAll(interviewRepository.getEmployerByStudentsCoursesId(studentsCourses1.getId(), InterviewStatus.PASSED));
            } else if(studentsCourses1.getYearbook().getEndInterviews().after(new Date()) && studentsCourses1.getYearbook().getStartInterviews().after(new Date())){
                employerList.addAll(practiceRepository.getEmployerToStudent(studentsCourses1.getYearbook().getId(), true, PracticeType.SCHOOL));
            }
        }
        for (Employer employer : employerList){
            EmployerDTO dto = new EmployerDTO();
            dto.setFullName(employer.getFullName());
            dto.setName(employer.getName());
            dto.setId(employer.getId().toString());
            dto.setDescription(employer.getDescription());
            dtoList.add(dto);
        }

        return dtoList;
    }

    @Override
    public List<Interview> getPracticeInterview(Long employerId, Long yearbookId) {
        List<Interview>list = new ArrayList<>();
        list = interviewRepository.getByEmployerIdAndYearbookId(employerId,yearbookId);
        return list;
    }

    private List<AssistenCourses> addAssistentCourse(UserDto dto, Long employerId, List<Long> yerbookId) {
        List<AssistenCourses> list = new ArrayList<>();
        Person person = new Person();
        Users users = new Users();
        person.setMobilePhone(dto.getPhone2());
        person.setEmail(dto.getEmail());
        person.setPhone(dto.getPhone());
        person.setFirstName(dto.getFirstName());
        person.setLastName(dto.getLastName());
        person.setDegree(dto.getDegree());
        person = personRepository.save(person);
        Roles role = new Roles();
        role.setId(3);
        users.setRoles(role);
        users.setPerson(person);
        users.setPassword(dto.getPassword());
        users.setLogin(dto.getLogin());
        users = userRepository.save(users);
        AssistentPractices assistentPractices = new AssistentPractices();
        assistentPractices.setPerson(person);
        assistentPractices.setPosition(dto.getPosition());
        assistentPractices = assistentsPracticeRepository.save(assistentPractices);
        Employer employer = new Employer();
        employer.setId(employerId);
        for (Long id : yerbookId) {
            AssistenCourses assistenCourses = new AssistenCourses();
            assistenCourses.setEmployer(employer);
            assistenCourses.setAssistentPractices(assistentPractices);
            Yearbook yearbook = new Yearbook();
            yearbook.setId(id);
            assistenCourses.setYearbook(yearbook);
            list.add(asistentCoursesRepository.save(assistenCourses));
        }
        return list;
    }

}
