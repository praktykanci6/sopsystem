package org.pwsz.sops.services;

import org.pwsz.sops.dto.UserDto;
import org.pwsz.sops.entities.AssistenCourses;
import org.pwsz.sops.entities.Employer;
import org.pwsz.sops.entities.Person;

/**
 * Created by tifet on 2017-05-03.
 */
public interface AssistentCourseService {

    AssistenCourses addNewAssistent(UserDto dto, Long yearbooId, Employer employer);

    AssistenCourses getByPersonId(Person person,Long yearbookId);

}
