package org.pwsz.sops.services;

import org.pwsz.sops.dto.YearbookDto;
import org.pwsz.sops.entities.AcademicYear;
import org.pwsz.sops.entities.Yearbook;

import java.util.List;

/**
 * Created by tifet on 2017-04-09.
 */
public interface YearbookService {
    Integer addConfigurationAcademicYear(AcademicYear academicYear, List<Yearbook> yearbooks) throws Exception;

    List<YearbookDto> getYerbookCoursesList();
    Yearbook getYearbookById(Long id);
}
