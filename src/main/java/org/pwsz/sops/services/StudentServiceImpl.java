package org.pwsz.sops.services;

import com.ebay.xcelite.Xcelite;
import com.ebay.xcelite.reader.SheetReader;
import com.ebay.xcelite.sheet.XceliteSheet;
import org.pwsz.sops.DtoImport.DtoStudent;
import org.pwsz.sops.dto.EmployerDTO;
import org.pwsz.sops.dto.Notification;
import org.pwsz.sops.dto.PracticeDto;
import org.pwsz.sops.dto.YearbookDto;
import org.pwsz.sops.entities.*;
import org.pwsz.sops.entities.Dictionary.InterviewStatus;
import org.pwsz.sops.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by tifet on 2017-04-23.
 */
@Service
public class StudentServiceImpl implements StudenstService {

    @Autowired
    StudentsCoursesRepository studentsCoursesRepository;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AcademicYearRepository academicYearRepository;
    @Autowired
    InterviewRepository interviewRepository;
    @Autowired
    YearbookService yearbookService;
    @Autowired
    PracticeRepository practiceRepository;
    @Autowired
    PracticesCoursesRepository practicesCoursesRepository;
    @Autowired
    ChoosePracticeRepository choosePracticeRepository;

    /**
     * Dodawanie studentów kieruku
     * @param file plik importu xlsx
     * @param id id rocznika
     * @return lista nowych studentów rocznika
     * @throws Exception
     */
    @Override
    @Transactional
    public List<StudentsCourses> getStudentsFromXsl(File file, Long id) throws Exception {

        //przetwarzanie pliku xls
        Xcelite xcelite = new Xcelite(file);
        XceliteSheet sheet = xcelite.getSheet("Arkusz1");
        SheetReader<DtoStudent> reader = sheet.getBeanReader(DtoStudent.class);
        Collection<DtoStudent> users = reader.read();
        Yearbook yearbook = new Yearbook();
        yearbook.setId(id);
        List<StudentsCourses> studentsCourses = new ArrayList<>();
        List<Students> studentsList = new ArrayList<>();
        List<DtoStudent> dtoStudents = new ArrayList<>();
        List<StudentsCourses> result = new ArrayList<>();
        for(DtoStudent student2 : users){
            Students students = studentRepository.findByNumberAlbumEquals(student2.getId());
            if(students != null){
                studentsList.add(students);
            } else {
                dtoStudents.add(student2);
            }
        }
        for(DtoStudent student : dtoStudents){
                Students students = new Students();
                Person person = new Person();
                person.setFirstName(student.getFirstName());
                person.setLastName(student.getLastName());
                person = personRepository.save(person);
                Users user = new Users();
                user.setLogin(student.getLastName().toLowerCase()+student.getId());
                user.setPerson(person);
                Roles role = new Roles();
                role.setId(2);
                user.setRoles(role);
                userRepository.save(user);
                students.setPerson(person);
                students.setNumberAlbum(student.getId());
                students = studentRepository.save(students);
                StudentsCourses studentsCourses1 = new StudentsCourses();
                studentsCourses1.setStudents(students);
                studentsCourses1.setYearbook(yearbook);
                studentsCourses1 = studentsCoursesRepository.save(studentsCourses1);
                studentsCourses.add(studentsCourses1);
        }
            for (Students student : studentsList){
                StudentsCourses studentsCourses1 = new StudentsCourses();
                studentsCourses1.setStudents(student);
                studentsCourses1.setYearbook(yearbook);
                StudentsCourses studentsCourses2 = studentsCoursesRepository.findByStudentsAndYearbook(student,yearbook);
                if(null == studentsCourses2) {
                    studentsCourses1 = studentsCoursesRepository.save(studentsCourses1);
                }
                studentsCourses.add(studentsCourses1);
            }

        return studentsCourses;
    }

    /**
     * Pobieranie kierunkuw studenta
     * @return
     */
    @Override
    public List<YearbookDto> getsStudentCourses(){
        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        Students students = studentRepository.findByPersonId(user.getPerson().getId());
        List<StudentsCourses> list = new ArrayList<>();
        List<YearbookDto> list1 = new ArrayList<>();
        if(academicYear != null && students != null){
            list = studentsCoursesRepository.getByAcademicYearIdAndStudentId(academicYear.getId(),
                    students.getNumberAlbum());
        }
        for(StudentsCourses courses : list){
            YearbookDto dto = new YearbookDto();
            dto.setCoursesOfStudeyName(courses.getYearbook().getCourseOfStudy().getNameCourses());
            dto.setCoursesOfStudtSepcilaization(courses.getYearbook().getCourseOfStudy().getSpecilaization());
            dto.setId(courses.getYearbook().getId());
            list1.add(dto);
        }
        return list1;
    }

    @Override
    public List<Practice> getStudetnPractice() {
        List<Practice> list = new ArrayList<>();
        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        Students student = studentRepository.findByPersonId(user.getPerson().getId());
        List<StudentsCourses> studentsCourses = studentsCoursesRepository.getByAcademicYearIdAndStudentId(academicYear.getId(),student.getNumberAlbum());
        for(StudentsCourses studentsCourses1 : studentsCourses){
            list.addAll(practiceRepository.findByStudentsCoursesId(studentsCourses1.getId()));
        }
        for(Practice practice : list){
            practice.getStudentsCourses().getStudents();
            practice.getStudentsCourses().getYearbook().getAcademicYear();
            if(practice.getCoordinatorCoursesEmployer()!= null)
                practice.getCoordinatorCoursesEmployer().getEmployer().getAddresses();
            practice.getEmployAddress();
            practice.getEmployer().getAssistenCourses();
            practice.getEmployer().getCoordinatorCourses();
        }
        return list;
    }
    @Override
    public List<Interview> getInterviewsStudent() {

        List<Interview> list = new ArrayList<>();
        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        Students student = studentRepository.findByPersonId(user.getPerson().getId());
        List<StudentsCourses> studentsCourses = studentsCoursesRepository.getByAcademicYearIdAndStudentId(academicYear.getId(),student.getNumberAlbum());
        for(StudentsCourses studentsCourses1 : studentsCourses){
            list.addAll(interviewRepository.findByStudentsCoursesId(studentsCourses1.getId()));
        }

        for(Interview interview : list){
            interview.getEmployer().getAddresses();
            interview.getStudentsCourses().getStudents();
            interview.getStudentsCourses().getYearbook().getAcademicYear();
        }

        return list;
    }

    @Override
    public Notification addStudentApplication(PracticeDto data) {
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(academicYear != null){
            Students student = studentRepository.findByPersonId(user.getPerson().getId());
            Yearbook yearbook = yearbookService.getYearbookById(data.getYearbookId());
            StudentsCourses studentsCourse = studentsCoursesRepository.findByYearbookIdAndStudentsNumberAlbum(data.getYearbookId(),student.getNumberAlbum());
            if(yearbook.getStartChoice().before(new Date()) && yearbook.getEndChoice().after(new Date())){
                List<Practice>practices = practiceRepository.findByStartDateAndEndDateAndEmployerIdAndStudentsCoursesIsNull(new java.sql.Date(data.getStar().getTime()),
                        new java.sql.Date(data.getEnd().getTime()),data.getEmployerId());
                Practice practiceToSave = null;
                for(Practice practice : practices){
                    practiceToSave = practice;
                }
                if(practiceToSave != null){
                    ChoosePractices choosePractice = new ChoosePractices();
                    choosePractice.setPractice(practiceToSave);
                    choosePractice.setStudentsCours(studentsCourse);
                    choosePractice.setPriority(data.getPreference());
                    choosePractice = choosePracticeRepository.save(choosePractice);
                    return new Notification("Dodano zgłoszenie na praktyki");
                }

            } else if(yearbook.getStartInterviews().before(new Date()) && yearbook.getEndInterviews().after(new Date())){
                Interview interview = new Interview();
                Employer employer = new Employer();
                employer.setId(data.getEmployerId());
                interview.setEmployer(employer);
                interview.setStudentsCourses(studentsCourse);
                interview.setStatus(InterviewStatus.NEW);
                interview = interviewRepository.save(interview);
                return new Notification("Zapisanmo na rozmowe kwalifikacyjną");
            }
        }
        return new Notification("Wystapił bład serwera!");
    }

    @Override
    public List<Interview> gestStudentInterviews() {
        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        List<Interview>interviews = new ArrayList<>();
        if(academicYear != null){
            Students student = studentRepository.findByPersonId(user.getPerson().getId());
            List<StudentsCourses> studentsCourses = studentsCoursesRepository
                    .getByAcademicYearIdAndStudentId(academicYear.getId(),student.getNumberAlbum());
            for (StudentsCourses studentsCourses1 : studentsCourses){
                interviews.addAll(interviewRepository.findByStudentsCoursesId(studentsCourses1.getId()));
            }
        }

        return interviews;
    }

    @Override
    public List<PracticeDto> getstudentApplicationes() {
        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        List<Practice> list = new ArrayList<>();
        List<PracticeDto> practiceList = new ArrayList<>();
        if(academicYear != null){
            Students student = studentRepository.findByPersonId(user.getPerson().getId());
            List<StudentsCourses> studentsCourses = studentsCoursesRepository
                    .getByAcademicYearIdAndStudentId(academicYear.getId(),student.getNumberAlbum());
            for(StudentsCourses courses : studentsCourses){
                list.addAll(choosePracticeRepository.getPracticeByStAndStudentsCoursId(courses.getId()));
            }
        }
        for(Practice practice : list){
            PracticeDto practiceDto = new PracticeDto(practice);
            EmployerDTO employe = new EmployerDTO(practice.getEmployer());
            practiceDto.setEmployer(employe);
            practiceList.add(practiceDto);
        }
        return practiceList;
    }
}
