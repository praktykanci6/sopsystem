package org.pwsz.sops.services;

import org.pwsz.sops.dto.CoordinatorCoursesDto;
import org.pwsz.sops.dto.UserDto;
import org.pwsz.sops.entities.*;
import org.pwsz.sops.repositories.CoordinatorCourseRepository;
import org.pwsz.sops.repositories.CoordinatorCoursesEmployerRepository;
import org.pwsz.sops.repositories.CoordinatorPracticeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tifet on 2017-05-02.
 */

@Service
public class CoordinatorCourseSericeImpl implements  CoordinatroCourseService{
    @Autowired
    UserService userService;
    @Autowired
    CoordinatorPracticeRepository coordinatorPracticeRepository;
    @Autowired
    CoordinatorCourseRepository coordinatorCourseRepository;
    @Autowired
    CoordinatorCoursesEmployerRepository coordinatorCoursesEmployerRepository;

    /**
     * Dodawanie nowego koordynatora kierunku oraz powiązanie go z praktykodawcami
     * @param coursesDto dane koordynatora
     * @return
     */
    @Override
    public CoordinatorCoursesDto addCoordinatorCourse(CoordinatorCoursesDto coursesDto) {
        List<CoordinatorCoursesEmployer>list = new ArrayList<>();
        CoordinatorsPractice coordinatorsPractice = new CoordinatorsPractice();
        Users user;
        Roles role = new Roles();
        role.setId(4);
        user = userService.adduserWithPersone(coursesDto.getUser(),role);
        coordinatorsPractice.setPerson(user.getPerson());
        coordinatorsPractice = coordinatorPracticeRepository.save(coordinatorsPractice);
        CoordinatorCourses coordinatorCourses = new CoordinatorCourses();
        coordinatorCourses.setCoordinatorsPractice(coordinatorsPractice);
        Yearbook yearbook = new Yearbook();
        yearbook.setId(coursesDto.getYearbookId());
        coordinatorCourses.setYearbook(yearbook);
        coordinatorCourses = coordinatorCourseRepository.save(coordinatorCourses);
        for(Long id : coursesDto.getEmployerIds()){
            CoordinatorCoursesEmployer coordinator = new CoordinatorCoursesEmployer();
            Employer employer = new Employer();
            employer.setId(id);
            coordinator.setEmployer(employer);
            coordinator.setCoordinatorCourses(coordinatorCourses);
            coordinator = coordinatorCoursesEmployerRepository.save(coordinator);
            list.add(coordinator);
        }
        return coursesDto;
    }

    @Override
    public List<UserDto> getCoordinatorCoursesList(Long yearbookId) {
        List<CoordinatorCourses>list = new ArrayList<>();
        List<UserDto> userDtoList = new ArrayList<>();
        list = coordinatorCourseRepository.findByYearbookId(yearbookId);
        for(CoordinatorCourses coordinatorCourses : list){
            Person person = coordinatorCourses.getCoordinatorsPractice().getPerson();
            UserDto user = new UserDto();
            user.setLastName(person.getLastName());
            user.setFirstName(person.getFirstName());
            user.setEmail(person.getEmail());
            user.setPhone(person.getPhone());
            user.setPhone2(person.getMobilePhone());
            user.setDegree(person.getDegree());
            userDtoList.add(user);
        }
        return userDtoList;
    }
}
