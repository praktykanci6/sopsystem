package org.pwsz.sops.services;

import org.pwsz.sops.entities.Address;

import java.util.List;

/**
 * Created by tifet on 2017-05-10.
 */
public interface EmployerAddressService {

    public List<Address>getEmployerAddres(Long employerId);
}
