package org.pwsz.sops.services;

import org.pwsz.sops.dto.UserDto;
import org.pwsz.sops.entities.*;
import org.pwsz.sops.repositories.AsistentCoursesRepository;
import org.pwsz.sops.repositories.AssistentsPracticeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by tifet on 2017-05-09.
 */
@Service
public class AssistentCourseServiceImpl implements AssistentCourseService {

    @Autowired
    AsistentCoursesRepository asistentCoursesRepository;
    @Autowired
    AssistentsPracticeRepository assistentsPracticeRepository;
    @Autowired
    UserService userService;

    @Override
    public AssistenCourses addNewAssistent(UserDto dto,Long yearbooId, Employer employer) {
        Roles role = new Roles();
        role.setId(3);
        Yearbook yearbook = new Yearbook();
        yearbook.setId(yearbooId);
        Users user = userService.adduserWithPersone(dto,role);
        AssistentPractices assistentPractices = new AssistentPractices();
        assistentPractices.setPosition(dto.getPosition());
        assistentPractices.setPerson(user.getPerson());
        assistentPractices = assistentsPracticeRepository.save(assistentPractices);
        //dodawanie asystena kierukowego praktykodawcy
        AssistenCourses assistenCourses = new AssistenCourses();
        assistenCourses.setYearbook(yearbook);
        assistenCourses.setAssistentPractices(assistentPractices);
        assistenCourses = asistentCoursesRepository.save(assistenCourses);

        return assistenCourses;
    }

    @Override
    public AssistenCourses getByPersonId(Person person,Long yearbookId) {
        return asistentCoursesRepository.findByPersoneId(person.getId(),yearbookId);
    }
}
