package org.pwsz.sops.services;

import org.pwsz.sops.dto.Notification;
import org.pwsz.sops.dto.PracticeDto;
import org.pwsz.sops.dto.YearbookDto;
import org.pwsz.sops.entities.Interview;
import org.pwsz.sops.entities.Practice;
import org.pwsz.sops.entities.StudentsCourses;

import java.io.File;
import java.util.List;

/**
 * Created by tifet on 2017-04-23.
 */
public interface StudenstService {

    /**
     * Import studentów kierunku z pliku xsl
     */
    public List<StudentsCourses> getStudentsFromXsl(File file, Long id) throws Exception;

    public List<YearbookDto> getsStudentCourses();

    public List<Practice> getStudetnPractice();

    public List<Interview> getInterviewsStudent();

    public Notification addStudentApplication(PracticeDto data);

    List<Interview> gestStudentInterviews();

    List<PracticeDto> getstudentApplicationes();
   }
