/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pwsz.sops.services;

import fr.opensagres.xdocreport.core.XDocReportException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.pwsz.sops.dto.ReportDto;
import org.pwsz.sops.entities.AcademicYear;
import org.pwsz.sops.entities.Users;

/**
 *
 * @author $k.kabat
 */
public interface ReportService {
    
    public ByteArrayOutputStream getWorkPlaceDeclarationReport(AcademicYear academicYear, List<Users> users) throws IOException, XDocReportException;
    
    public ByteArrayOutputStream getAgreementApplicationReport() throws IOException, XDocReportException;
    
    public ByteArrayOutputStream getWorkPlaceApplication() throws IOException, XDocReportException;

    public ByteArrayOutputStream getAgreementApplicationReport(ReportDto data) throws IOException, XDocReportException;

    public ByteArrayOutputStream getWorkPlaceApplicationReport(ReportDto data) throws IOException, XDocReportException;

}
