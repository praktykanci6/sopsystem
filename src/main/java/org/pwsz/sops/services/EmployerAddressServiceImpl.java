package org.pwsz.sops.services;

import org.pwsz.sops.entities.Address;
import org.pwsz.sops.repositories.EmployAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by tifet on 2017-05-10.
 */
@Service
public class EmployerAddressServiceImpl implements EmployerAddressService{

    @Autowired
    EmployAddressRepository employAddressRepository;

    @Override
    public List<Address> getEmployerAddres(Long employerId) {
        return employAddressRepository.getAddressByEmployerId(employerId);
    }
}
