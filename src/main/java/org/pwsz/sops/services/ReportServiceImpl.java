/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pwsz.sops.services;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;

import javafx.scene.input.DataFormat;
import org.pwsz.sops.dto.ReportDto;
import org.pwsz.sops.entities.AcademicYear;
import org.pwsz.sops.entities.Students;
import org.pwsz.sops.entities.Users;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.pwsz.sops.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;

/**
 *
 * @author $k.kabat
 */
@ComponentScan
@Service("reportService")
public class ReportServiceImpl implements ReportService{

    @Autowired
    StudentRepository  studentRepository;

    // Karta oświadczenia zakładu pracy
    private String WORK_PLACE_DECLARATION = "WorkPlaceDeclaration.docx";

    // Karta zgłoszenia umowy
    private String AGREEMENT_APPLICATION = "AgreementApplication.pdf";

    // Karta zgłoszenia zakładu pracy
    private String WORK_PLACE_APPLICATION = "WorkPlaceApplication.pdf";



    @Override
    public ByteArrayOutputStream getAgreementApplicationReport() throws IOException, XDocReportException {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("/reports/"+AGREEMENT_APPLICATION);
//        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(is, TemplateEngineKind.Velocity);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int byteSize;
        byte [] byteArray = new byte[8192];
        while((byteSize = is.read(byteArray)) != -1){
            out.write(byteArray, 0, byteSize);
        }

//        Options options = Options.getTo(ConverterTypeTo.PDF);
//        IContext context = report.createContext();
//        report.convert(context, options, out);
        return out;
    }

    @Override
    public ByteArrayOutputStream getWorkPlaceApplication() throws IOException, XDocReportException {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("/reports/"+WORK_PLACE_APPLICATION);
//        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(is, TemplateEngineKind.Velocity);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int byteSize;
        byte[] byteArray = new byte[8192];
        while ((byteSize = is.read(byteArray)) != -1) {
            out.write(byteArray, 0, byteSize);
        }
//        Options options = Options.getTo(ConverterTypeTo.PDF);
//        IContext context = report.createContext();
//        report.convert(context, options, out);
        return out;
    }

    @Override
    public ByteArrayOutputStream getWorkPlaceDeclarationReport(AcademicYear academicYear, List<Users> users) throws IOException, XDocReportException {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("/reports/"+WORK_PLACE_DECLARATION);
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(is,TemplateEngineKind.Velocity) ;
        IContext context = report.createContext();
        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        startDate.setTime(academicYear.getStart());
        endDate.setTime(academicYear.getEnd());

        String academicYearName = startDate.get(Calendar.YEAR)+"/"+endDate.get(Calendar.YEAR);
        context.put("academicYear",academicYearName);
        context.put("institute", "asd");
//        context.put("currentDate", new Date());
//        context.put("company", "sadsa");
//        context.put("companyBoss", "sdas");
//        context.put("practicePlace","sd");
//        context.put("practiceCoordinator","dd");
//        context.put("coordinatorInformations","dddd");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Options options = Options.getTo(ConverterTypeTo.PDF);
        report.convert(context,options, out);
        out.close();
        return out;
    }

    @Override
    public ByteArrayOutputStream getAgreementApplicationReport(ReportDto data) throws IOException, XDocReportException {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("/reports/"+AGREEMENT_APPLICATION);
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(is,TemplateEngineKind.Velocity) ;
        IContext context = report.createContext();
        Users user = (Users)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Students student = studentRepository.findByPersonId(user.getPerson().getId());
        String email ="";
        if(user.getPerson().getEmail() != null){
            email = user.getPerson().getEmail();
        }
        String yes = "TAK";
        String no = "NIE";
        String phone =data.getCompany().getEmployerAddresses().get(0).getPhone1();
        String addressPractice = "";
        String address = data.getCompany().getEmployerAddresses().get(0)
                .getStreet()+" "+data.getCompany().getEmployerAddresses().get(0).getNumberBuilding()+
                " "+data.getCompany().getEmployerAddresses().get(0).getPostCode()+
                " "+data.getCompany().getEmployerAddresses().get(0).getCity();
        if(data.getCompany().getEmployerAddresses().size()==2){
            addressPractice = data.getCompany().getEmployerAddresses().get(1)
                    .getStreet()+" "+data.getCompany().getEmployerAddresses().get(1).getNumberBuilding()+
                    " "+data.getCompany().getEmployerAddresses().get(1).getPostCode()+
                    " "+data.getCompany().getEmployerAddresses().get(1).getCity();
        }
        context.put("data",data);
        context.put("address",address);
        context.put("agreement", data.getAgreement()? yes: no );
        context.put("agreement2", data.getAgreement2()? yes : no);
        context.put("agreement3", data.getAgreement3()? yes : no);
        context.put("numberAlbum", student.getNumberAlbum());
        context.put("addressPractice",addressPractice);
        context.put("phone",phone);
        context.put("name",user.getPerson().getFirstName() + " " + user.getPerson().getLastName());
        context.put("email",email);
        context.put("start",new SimpleDateFormat("dd-MM-yyyy").format(data.getPractice().getStar()));
        context.put("end", new SimpleDateFormat("dd-MM-yyyy").format(data.getPractice().getEnd()));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Options options = Options.getTo(ConverterTypeTo.PDF);
        report.convert(context,options, out);
        out.close();
        return out;
    }

    @Override
    public ByteArrayOutputStream getWorkPlaceApplicationReport(ReportDto data) throws IOException, XDocReportException {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("/reports/"+WORK_PLACE_APPLICATION);
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(is,TemplateEngineKind.Velocity) ;
        IContext context = report.createContext();
        Users user = (Users)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Students student = studentRepository.findByPersonId(user.getPerson().getId());
        String email ="";
        if(user.getPerson().getEmail() != null){
            email = user.getPerson().getEmail();
        }
        String eName = data.getEmployer().getFirstName() != null ? data.getEmployer().getFirstName() : "";
        eName += " " + data.getEmployer().getLastName() != null ? data.getEmployer().getLastName() : "";
        String yes = "TAK";
        String no = "NIE";
        String employerName ="";
        String employerName1 ="";
        String employerPhone = "";
        String phone =data.getCompany().getEmployerAddresses().get(0).getPhone1();
        String addressPractice = "";
        String aDegree = "";
        String aName = "";
        String aEmail = "";
        String aPhone = "";
        String aPhone2 = "";
        String aPosition = "";
        if(data.getAssistant() !=null){
            aDegree = data.getAssistant().getDegree() != null ? data.getAssistant().getDegree():"";
            aName = data.getAssistant().getFirstName() != null ? data.getAssistant().getFirstName():"";
            eName += " "+ data.getAssistant().getLastName() != null? data.getAssistant().getLastName():"";
            aPhone = data.getAssistant().getPhone() != null ? data.getAssistant().getPhone():"";
            aPhone = data.getAssistant().getPhone2() != null ? data.getAssistant().getPhone2():"";
            aPosition = data.getAssistant().getPosition() != null? data.getAssistant().getPosition():"";
            aEmail = data.getAssistant().getEmail() != null? data.getAssistant().getEmail():"";
        }
        String address = data.getCompany().getEmployerAddresses().get(0)
                .getStreet()+" "+data.getCompany().getEmployerAddresses().get(0).getNumberBuilding()+
                " "+data.getCompany().getEmployerAddresses().get(0).getPostCode()+
                " "+data.getCompany().getEmployerAddresses().get(0).getCity();
        if(data.getCompany1() != null){
            employerName1 = data.getCompany1().getFullName();
            if(data.getCompany1().getEmployerAddresses() != null) {
                employerPhone = data.getCompany1().getEmployerAddresses().get(0).getPhone1() != null ?
                        data.getCompany1().getEmployerAddresses().get(0).getPhone1() + ", " : "";
                employerPhone += data.getCompany1().getEmployerAddresses().get(0).getPhone2() != null ?
                        data.getCompany1().getEmployerAddresses().get(0).getPhone2() : "";
                addressPractice = data.getCompany1().getEmployerAddresses().get(0)
                        .getStreet() + " " + data.getCompany1().getEmployerAddresses().get(0).getNumberBuilding() +
                        " " + data.getCompany1().getEmployerAddresses().get(0).getPostCode() +
                        " " + data.getCompany1().getEmployerAddresses().get(0).getCity();
            }
        }
        context.put("data",data);
        context.put("address",address);
        context.put("employerAddress1",addressPractice);
        context.put("employerName1",employerName1);
        context.put("phone1",employerPhone);
        context.put("trade",data.getCompany().getTrade());
        context.put("addressWWW",data.getCompany().getDescription() != null?
            data.getCompany().getDescription() : "");
        context.put("employerName",data.getCompany().getFullName());
        context.put("numberAlbumu", student.getNumberAlbum());
        context.put("addressPractice",addressPractice);
        context.put("phone",phone);
        context.put("ePosition",data.getEmployer().getDegree() != null? data.getEmployer().getDegree():"");
        context.put("email",data.getEmployer().getEmail() != null? data.getEmployer().getEmail():"");
        context.put("studentName",user.getPerson().getFirstName() + " " + user.getPerson().getLastName());
        context.put("studentEmail",email);
        context.put("degree",data.getEmployer().getDegree() != null? data.getEmployer().getDegree() :"");
        context.put("name", eName);
        context.put("ePhone",data.getEmployer().getPhone() != null? data.getEmployer().getPhone(): "");
        context.put("aDegree",aDegree);
        context.put("aPhone",aPhone);
        context.put("aPhone2",aPhone2);
        context.put("aPosition",aPosition);
        context.put("aName",aName);
        context.put("aEmail",aEmail);
        context.put("start",new SimpleDateFormat("dd-MM-yyyy").format(data.getPractice().getStar()));
        context.put("end", new SimpleDateFormat("dd-MM-yyyy").format(data.getPractice().getEnd()));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Options options = Options.getTo(ConverterTypeTo.PDF);
        report.convert(context,options, out);
        out.close();
        return out;
    }



}
