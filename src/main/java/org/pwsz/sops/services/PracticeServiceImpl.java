package org.pwsz.sops.services;

import org.pwsz.sops.dto.PracticeDto;
import org.pwsz.sops.dto.ReportDto;
import org.pwsz.sops.entities.*;
import org.pwsz.sops.entities.Dictionary.PracticeStatus;
import org.pwsz.sops.entities.Dictionary.PracticeType;
import org.pwsz.sops.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.jws.soap.SOAPBinding;
import java.util.*;

/**
 * Created by tifet on 2017-05-04.
 */
@Service
public class PracticeServiceImpl implements PracticeService{

    @Autowired
    PracticesCoursesRepository practicesCoursesRepository;
    @Autowired
    AcademicYearRepository academicYearRepository;
    @Autowired
    AssistentCourseService assistentCourseService;
    @Autowired
    CoordinatorCoursesEmployerRepository coordinatorCoursesEmployerRepository;
    @Autowired
    EmployAddressRepository employAddressRepository;
    @Autowired
    PracticeRepository practiceRepository;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    EmployerService employerService;
    @Autowired
    StudentsCoursesRepository studentsCoursesRepository;
    @Autowired
    CoordinatorCourseRepository coordinatorCourseRepository;
    @Autowired
    AsistentCoursesRepository asistentCoursesRepository;
    @Autowired
    CoordinatorPracticeRepository coordinatorPracticeRepository;

    @Override
    public Set<PracticeDto> getEmployerPracitce(Long employerId) {
        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Long> practiceCoursesId = new ArrayList<>();
        Set<PracticeDto>practices = new HashSet<>();
        Set<PracticeDto>practices1 = new HashSet<>();
        List<Practice>list = new ArrayList<>();
        AcademicYear academicYear ;
        academicYear = academicYearRepository.findActualAcademicYear();
        practiceCoursesId = practicesCoursesRepository.findByAcademicYearId(academicYear.getId());
        list = practiceRepository.findByEmployerIdAndYea(employerId,practiceCoursesId);
        for(Practice practice : list){
            Integer counter = practiceRepository.countPracticeByStartDateAndEndDateAndEmployerId
                    ((java.sql.Date) practice.getStartDate(),(java.sql.Date) practice.getEndDate(),practice.getEmployer().getId());
            PracticeDto dto = new PracticeDto();
            dto.setEnd(practice.getEndDate());
            dto.setStar(practice.getStartDate());
            dto.setInterview(practice.isInterwiev());
            dto.setCounter(counter);
            dto.setAddressId(practice.getEmployAddress().getId());
            dto.setYearbookId(practice.getAssistenCourses().getYearbook().getId());
            practices.add(dto);
        }
        if(user.getRoles().equals("STUDENT")) {
            Students student = studentRepository.findByPersonId(user.getPerson().getId());
            List<StudentsCourses> studentsCourses = studentsCoursesRepository.getByAcademicYearIdAndStudentId(academicYear.getId(), student.getNumberAlbum());
            for (StudentsCourses studentsCourses1 : studentsCourses) {
                for(PracticeDto dto : practices){
                    if(dto.getYearbookId() == studentsCourses1.getYearbook().getId()){
                        practices1.add(dto);
                    }
                }
            }
        } else {
            practices1 = practices;
        }
        return practices1;
    }

    @Override
    public List<Practice> addPractice(PracticeDto practice) {
        Users users = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AssistenCourses assistenCourses = assistentCourseService.getByPersonId(users.getPerson(),practice.getYearbookId());
        EmployAddress employAddress = employAddressRepository.getByAddressId(practice.getAddressId());
        PracticesCourses practicesCourses = practicesCoursesRepository.findByAcademicYearIdAnAndYearbookId(assistenCourses.getYearbook().getAcademicYear().getId(),
                practice.getYearbookId());
        Address address = new Address();
        address.setId(practice.getAddressId());
        List<Practice> practiceToAdd = new ArrayList<>();
        CoordinatorCoursesEmployer coordinatorCoursesEmployer = coordinatorCoursesEmployerRepository.
                findCoordinatorCoursesEmployerbyEemployerIdAndYearbookId(assistenCourses.getEmployer().getId(),practice.getYearbookId());
        for(int i = 0; i < practice.getCounter(); i++ ){
            Practice practice1 = new Practice();
            practice1.setAssistenCourses(assistenCourses);
            practice1.setStartDate(practice.getStar());
            practice1.setEndDate(practice.getEnd());
            practice1.setEmployer(assistenCourses.getEmployer());
            practice1.setEmployAddress(employAddress);
            practice1.setCreatDate(new Date());
            practice1.setPracticesCourses(practicesCourses);
            practiceToAdd.add(practice1);
            practice1.setInterwiev(practice.getInterview());
        }
        practiceToAdd = practiceRepository.save(practiceToAdd);
        return practiceToAdd;
    }

    @Override
    @Transactional(value = "defaultTransactionManager")
    public Practice addOwnPractice(ReportDto data, Long yearbookId, MultipartFile file) throws Exception {
        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        Students students  = studentRepository.findByPersonId(user.getPerson().getId());
        Yearbook yearbook = new Yearbook();
        yearbook.setId(yearbookId);
        StudentsCourses studentsCourses = studentsCoursesRepository.findByStudentsAndYearbook(students,yearbook);
        //praktykodawca
        Employer employer = employerService.saveEmployer(data.getCompany());
        List<CoordinatorCourses> coordinatorCourses = coordinatorCourseRepository.findByYearbookId(yearbookId);
        AssistenCourses assistenCourses = assistentCourseService.addNewAssistent(data.getAssistant(),yearbookId,employer);
        PracticesCourses practicesCourses = practicesCoursesRepository.findByAcademicYearIdAnAndYearbookId(academicYear.getId(),yearbookId);
        Practice practice = new Practice();
        practice.setPracticesCourses(practicesCourses);
        practice.setCreatDate(new Date());
        practice.setStartDate(data.getPractice().getStar());
        practice.setEndDate(data.getPractice().getEnd());
        practice.setStudentsCourses(studentsCourses);
        practice.setEmployer(employer);
        practice.setAssistenCourses(assistenCourses);
        practice.setReferraFileName(file.getOriginalFilename());
        practice.setPracticeType(PracticeType.STUDENT);
        if(employer.getAddresses().size()==1) {
            practice.setEmployAddress(employer.getAddresses().get(0));
        } else {
            practice.setEmployAddress(employer.getAddresses().get(1));
        }
        practice.setReferrals(file.getBytes());
        practice = practiceRepository.save(practice);
        return practice;
    }

    @Override
    public List<Practice> getStudentPractice(Users user) {
        return null;
    }

    @Override
    public List<PracticeDto> getStudentAvaliblePractice() {
        return null;
    }


    @Override
    public Practice getPracticeById(Long practiceId) {
        return practiceRepository.findOne(practiceId);
    }

    @Override
    public List<Practice> getEmployerPractice(Long employerId) {
        Students student = null;
        List<Practice> list = new ArrayList<>();
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        List<StudentsCourses> studentsCourses;
        List<AssistenCourses> assistenCourses;
        List<CoordinatorCourses> coordinatorCourses;
        List<Long>yearbooks = new ArrayList<>();
        Users user =(Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(user.getRoles().getRole().equals("STUDENT")){
            student = studentRepository.findByPersonId(user.getPerson().getId());
            studentsCourses = studentsCoursesRepository.getByAcademicYearIdAndStudentId(academicYear.getId(),student.getNumberAlbum());
            for(StudentsCourses studentsCourses1 : studentsCourses){
                for(PracticesCourses practicesCourses :  studentsCourses1.getYearbook().getPracticesCourses())
                    yearbooks.add(practicesCourses.getId());
            }
        } else if(user.getRoles().getRole().equals("PRAKTYKODAWCA")){
            assistenCourses = asistentCoursesRepository.findByPersoneIdAndAcademicYear(user.getPerson().getId(),academicYear.getId());
            for(AssistenCourses assistenCourses1 : assistenCourses){
                for(PracticesCourses practicesCourses : assistenCourses1.getYearbook().getPracticesCourses())
                    yearbooks.add(practicesCourses.getId());
            }
        } else if(user.getRoles().getRole().equals("OPIEKUN")){
            CoordinatorsPractice coordinatorsPractice = coordinatorPracticeRepository.findByPersonId(user.getPerson().getId());
            coordinatorCourses = coordinatorCourseRepository.findByCoordinatorsPracticeId(coordinatorsPractice.getId());
            for(CoordinatorCourses coordinatorCourses1 : coordinatorCourses){
                for(PracticesCourses practicesCourses : coordinatorCourses1.getYearbook().getPracticesCourses()){
                    if(practicesCourses.getAcademicYear().getId() == academicYear.getId()) {
                        yearbooks.add(practicesCourses.getId());
                    }
                }

            }
        } else if(user.getRoles().getRole().equals("ADMINISTRATOR")){
//            list.addAll(practiceRepository.f)
        }
        if(yearbooks.size()>0)
            list.addAll(practiceRepository.findByEmployerIdAndYea(employerId,yearbooks));
        return list;
    }

    @Override
    public Practice addStudentAgreement(ReportDto data, Long yearbookId, MultipartFile file) throws Exception {
        Users user =(Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Students studen = studentRepository.findByPersonId(user.getPerson().getId());
        Employer employer = employerService.saveEmployer(data.getCompany());
        AcademicYear academicYear = academicYearRepository.findActualAcademicYear();
        StudentsCourses studentsC = null;
        List<StudentsCourses> studentsCourses = studentsCoursesRepository.getByAcademicYearIdAndStudentId(academicYear.getId(),studen.getNumberAlbum());
        for(StudentsCourses studentsCourses1 : studentsCourses){
            if(studentsCourses1.getYearbook().getId().equals(yearbookId)){
                studentsC = studentsCourses1;
            }
        }
        PracticesCourses practicesCourses = practicesCoursesRepository.findByAcademicYearIdAnAndYearbookId(academicYear.getId(),yearbookId);
        Practice practice = new Practice();
        practice.setEmployAddress(employer.getAddresses().get(0));
        practice.setReferrals(file.getBytes());
        practice.setStartDate(data.getPractice().getStar());
        practice.setEndDate(data.getPractice().getEnd());
        practice.setReferraFileName(file.getOriginalFilename());
        practice.setPracticeType(PracticeType.AGREEMENT);
        practice.setCreatDate(new Date());
        practice.setStatus(PracticeStatus.NEW);
        practice.setStudentsCourses(studentsC);
        practice.setEmployer(employer);
        practice.setHours(data.getPractice().getHours());
        practice.setEmploymentDimension(data.getPractice().getEmploymentDimension());
        practice = practiceRepository.save(practice);
        return practice;
    }
}
