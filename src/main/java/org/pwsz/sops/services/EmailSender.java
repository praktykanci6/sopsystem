package org.pwsz.sops.services;

/**
 * Created by tifet on 2017-06-12.
 */
public interface EmailSender {
    void sendEmail(String to, String subject, String content);
}
