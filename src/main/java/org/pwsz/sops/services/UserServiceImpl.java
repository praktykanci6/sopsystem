package org.pwsz.sops.services;

import org.pwsz.sops.dto.UserDto;
import org.pwsz.sops.entities.Person;
import org.pwsz.sops.entities.Roles;
import org.pwsz.sops.entities.Users;
import org.pwsz.sops.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Created by tifet on 2017-04-25.
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;

    @Autowired
    PersonRepository personRepository;

    /**
     * podstawowe dane user
     * @return uzytkownika
     */
    @Override
    public UserDto getCurrentUsers() {
        Users currentUser = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDto user = new UserDto(currentUser);

        return null;
    }

    /**
     * Update lub zapis uzytkownika;
     * @param user
     * @return
     */
    @Override
    public Users updateOrSaveUser(UserDto user ) {
      Users  u = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
      u.getPerson().setEmail(user.getEmail());
      u.getPerson().setLastName(user.getLastName());
      u.getPerson().setFirstName(user.getFirstName());
      if(user.getPassword() != ""){
          u.setPassword(user.getPassword());
      }
      if(user.getPhone() != null){
          u.getPerson().setPhone(user.getPhone());
      }
      if(user.getPhone2() != null){
          u.getPerson().setMobilePhone(user.getPhone2());
      }

        Person p = personRepository.save(u.getPerson());
        u.setPerson(p);
        u = userRepository.save(u);
        return u;
    }

    @Override
    public Users adduserWithPersone(UserDto user,Roles roles) {
        Users user1 = new Users();
        Person person = new Person();
        person.setDegree(user.getDegree());
        person.setFirstName(user.getFirstName());
        person.setLastName(user.getLastName());
        person.setEmail(user.getEmail());
        person.setPhone(user.getPhone());
        person.setMobilePhone(user.getPhone2());
        person = personRepository.save(person);
        if(user.getLogin() == null ){
            user1.setLogin(user.getFirstName().toLowerCase()+"."+user.getLastName().toLowerCase());
        }else {
            user1.setLogin(user.getLogin());
        }
        user1.setPerson(person);
        user1.setRoles(roles);
        if(user.getPassword() != null){
            user1.setPassword(user.getPassword());
        }
        user1 = userRepository.save(user1);
        return user1;
    }
}
