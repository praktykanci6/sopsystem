package org.pwsz.sops.services;

/**
 * Created by tifet on 2017-05-06.
 */
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.pwsz.sops.dto.HolidayDate;
import org.springframework.stereotype.Service;


/**
 *
 * @author user
 */
@Service
public class HolidaysDateService {
    /**
     *
     * @param year we want Eastern date.
     * @return easter date in Date format.
     * @throws ParseException - when formating of string to date fails.
     */
    public Date calculateEstern(double year) throws ParseException {
        double rok = year;
        double a = rok % 19;
        double b = (int) (rok / 100);
        double c = rok % 100;
        double d = (int) (b / 4);
        double e = b % 4;
        double f = (int) ((b + 8) / 25);
        double g = (int) ((b - f + 1) / 3);
        double h = (19 * a + b - d - g + 15) % 30;
        double i = (int) (c / 4);
        double k = c % 4;
        double l = (32 + 2 * e + 2 * i - h - k) % 7;
        double m = (int) ((a + 11 * h + 22 * l) / 451);
        double dayOfMonth = (h + l - 7 * m + 114) % 31;
        dayOfMonth = (dayOfMonth + 1);
        double numberOfMonth = (int) ((h + l - 7 * m + 114) / 31);
        String day = String.valueOf((int) dayOfMonth);
        String month = String.valueOf((int) numberOfMonth);
        String yeartext = String.valueOf((int) rok);
        Date estern;
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        estern = format.parse(day + '-' + month + '-' + yeartext);
        return estern;
    }
    /**
     *
     * @param year we want holidays dates.
     * @return list of holidays dates in HolidayDate format.
     */
    public List<HolidayDate> holidays(Integer year) {
        List<HolidayDate> dates = new ArrayList<>();

        try {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            dates.add(new HolidayDate(format.parse("01-01-" + year), "Nowy rok"));
            dates.add(new HolidayDate(format.parse("06-01-" + year), "Trzech króli"));
            dates.add(new HolidayDate(format.parse("01-05-" + year), "Święto pracy"));
            Date easterDay = calculateEstern(year);
            dates.add(new HolidayDate(easterDay, "Pierwszy dzień wielkanocy"));
            dates.add(new HolidayDate(calculateEasterDay2(easterDay), "Drugi dzień wielkanocy"));
            dates.add(new HolidayDate(calculatePentecost(easterDay), "Zielone świątki,Zesłanie ducha świętego"));
            dates.add(new HolidayDate(calculateOpusChristi(easterDay), "Boże Ciało"));
            dates.add(new HolidayDate(format.parse("03-05-" + year), "Uchwalenie konstytucji"));
            dates.add(new HolidayDate(format.parse("15-08-" + year), "Wniebowzięcie Najświętszej Marii Panny"));
            dates.add(new HolidayDate(format.parse("01-11-" + year), "Uroczystość Wszystkich Świętych"));
            dates.add(new HolidayDate(format.parse("11-11-" + year), "Święto niepodległości"));
            dates.add(new HolidayDate(format.parse("25-12-" + year), "pierwszy dzień Bożego Narodzenia"));
            dates.add(new HolidayDate(format.parse("26-12-" + year), "drugi dzień Bożego Narodzenia"));
        } catch (ParseException ex) {

        }
        return dates;
    }
    /**
     *
     * @param easterDay Date of first easter day in Date format.
     * @return date of pentecost in Date format
     */
    public Date calculatePentecost(Date easterDay) {

        DateTime jodaPentecost = new DateTime(easterDay);
        int month = jodaPentecost.monthOfYear().get();
        if (month == 3) {
            jodaPentecost = jodaPentecost.plusDays(50);
        } else {
            jodaPentecost = jodaPentecost.plusDays(49);
        }
        Date pentecost = jodaPentecost.toDate();

        return pentecost;
    }
    /**
     *
     * @param easterDay Date of first easter day in Date format.
     * @return date of second easter day in Date format
     */
    public Date calculateEasterDay2(Date easterDay) {
        DateTime easterJoda = new DateTime(easterDay);
        DateTime jodaDate = easterJoda.plusDays(1);
        Date easterDay2 = jodaDate.toDate();
        return easterDay2;

    }
    /**
     *
     * @param easterDay Date of first easter day in Date format.
     * @return date of Opus Christi day in Date format
     */
    public Date calculateOpusChristi(Date easterDay) {
        DateTime easterJoda = new DateTime(easterDay);
        DateTime jodaDate = easterJoda.plusDays(60);
        Date opusChristi = jodaDate.toDate();
        return opusChristi;
    }
    /**
     *
     * @param start start date of holidays.
     * @param end end date of holidays.
     * @return number of working days between two dates.
     * @deprecated becouse it uses deprecated function and dont take holidays into account. Use differenceWorkingDays.
     */
    @Deprecated
    public int differenceWorkingDaysOld(Date start, Date end) {
        int numberOfWorkDays = 0;
        while (end.getTime() >= start.getTime()) {
            if (start.getDay() != 0 && start.getDay() != 6) {
                numberOfWorkDays++;
            }
            start = new Date(start.getTime() + 1000 * 60 * 60 * 24);
        }

        return numberOfWorkDays;
    }
    /**
     *
     * @param date day we are checking about begin holidays in Date format.
     * @param holidays list of holidays in Date format.
     * @return true if day is in holiday list, else false.
     */
    private boolean isHoliday(DateTime date, List<HolidayDate> holidays) {
        for (HolidayDate h : holidays) {
            DateTime holidayDate = new DateTime(h.getDate());
            if (holidayDate.getYear() == date.getYear() && holidayDate.getMonthOfYear() == date.getMonthOfYear() && holidayDate.getDayOfMonth() == date.getDayOfMonth()) {
                return true;
            }
        }
        return false;
    }


    public boolean isHoliday(DateTime date) throws ParseException {
        if(isWeekend(date)) {
            return true;
        }
        DateTime easternDateTime = new DateTime(calculateEstern((double) date.getYear()));
        if(easternDateTime.getYear() == date.getYear() && easternDateTime.getMonthOfYear() == date.getMonthOfYear() && easternDateTime.getDayOfMonth() == date.getDayOfMonth()) {
            return true;
        }
        List<HolidayDate> holidayDays = holidays(date.getYear());
        for(HolidayDate holiday : holidayDays) {
            DateTime holidayDateTime = new DateTime(holiday.getDate().getTime());
            if(holidayDateTime.getYear() == date.getYear() && holidayDateTime.getMonthOfYear() == date.getMonthOfYear() && holidayDateTime.getDayOfMonth() == date.getDayOfMonth()) {
                return true;
            }
        }
        return false;
    }

    public boolean isWeekend(DateTime date) {
        return date.dayOfWeek().get() == 6 || date.dayOfWeek().get() == 7;
    }

    public int countWorkingDays(DateTime start, DateTime end) throws ParseException {
        int counter = 0;

        while(start.isBefore(end.getMillis()) || start.isEqual(end.getMillis())) {
            if(!isHoliday(start)) {
                counter++;
            }
            start = start.plusDays(1);
        }

        return counter;
    }


    /**
     *
     * @param start date of holidays.
     * @param end date of holidays.
     * @return number of working days between two dates.
     */
    public int differenceWorkingDays(Date start, Date end) {
        int numberOfWorkDays = 0;
        DateTime startDate = new DateTime(start);
        DateTime endDate = new DateTime(end);
        List<HolidayDate> listOfHolidayDates = null;
        if (startDate.getYear() == endDate.getYear()) {
            listOfHolidayDates = holidays(startDate.getYear());
            listOfHolidayDates.addAll(holidays(endDate.getYear()));
        } else {
            listOfHolidayDates = holidays(startDate.getYear());
        }
        while (startDate.equals(endDate) || endDate.isAfter(startDate)) {
            if (startDate.getDayOfWeek() != DateTimeConstants.SUNDAY && startDate.getDayOfWeek() != DateTimeConstants.SATURDAY) {
                if (!isHoliday(startDate, listOfHolidayDates)) {
                    numberOfWorkDays++;
                }
            }
            startDate = startDate.plusDays(1);
        }
        return numberOfWorkDays;
    }
}
