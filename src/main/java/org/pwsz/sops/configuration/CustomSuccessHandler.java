package org.pwsz.sops.configuration;

import org.pwsz.sops.entities.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Konrad on 15.11.2016.
 */
@Component
public class CustomSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

//    private static Logger logger = LoggerFactory.getLogger(CustomSuccessHandler.class);

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String targetUrl = determineTargetUrl(authentication);

        if(response.isCommitted()){
            logger.debug("Unable to redirect to " + targetUrl);
            return;
        }
        redirectStrategy.sendRedirect(request,response,targetUrl);

    }

    private String determineTargetUrl(Authentication authentication) {
        String url = "/";

//        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
//
//        List<String> roles = new ArrayList<String>();
//        for(GrantedAuthority a : authorities){
//            roles.add(a.getAuthority());
//        }
        Users user = (Users)authentication.getPrincipal();
        String role = user.getRoles().getRole();

//        if(role.equals("ADMINISTRATOR")){
//            url = "/";
//        }else if(role.equals("STUDENT")){
//            url = "/";
//        }
//        else if(role.equals("EMPLOYER")){
//            url = "/";
//        }

        return url;
    }
}
