package org.pwsz.sops.configuration;

/**
 * Created by tifet on 2017-05-21.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;


/**
 *
 * @author user
 */

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "egeriaEntityManagerFactory",
        transactionManagerRef = "egeriaTransactionManager",
        basePackages = "org.pwsz.sops.egeriaRepository")
public class EgeriaDataSourceConfig {

    @Autowired
    @Qualifier("egeriaJpaVendorAdapter")
    JpaVendorAdapter jpaVendorAdapter;

    @Value("${egeria.jpa.show-sql:false}")
    private boolean showSql;

    @Value("${egeria.datasource.url}")
    private String databaseUrl;

    @Value("${egeria.datasource.username}")
    private String userName;

    @Value("${egeria.datasource.password}")
    private String password;

    @Value("${egeria.datasource.driverClassName}")
    private String driverClassName;

    @Value("${egeria.jpa.hibernate.ddl-auto}")
    private String hibernateDdlAuto;

    @Bean(name = "egeriaJpaVendorAdapter")
    public HibernateJpaVendorAdapter hibernateJpaVendorAdapter() {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setShowSql(showSql);
        adapter.getJpaPropertyMap().put("hibernate.hbm2ddl.auto", hibernateDdlAuto);
        adapter.setDatabase(Database.POSTGRESQL);
        return adapter;
    }

    @Bean(name = "egeriaDataSource")
    public DataSource dataSource1() {
        DriverManagerDataSource dataSource1 = new DriverManagerDataSource(databaseUrl, userName, password);
        dataSource1.setDriverClassName(driverClassName);
        return dataSource1;
    }

    @Bean(name = "egeriaEntityManager")
    public EntityManager entityManager() {
        return egeriaEntityManagerFactory().createEntityManager();
    }

    @Bean(name = "egeriaEntityManagerFactory")
    public EntityManagerFactory egeriaEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
        emfb.setDataSource(dataSource1());
        emfb.setJpaVendorAdapter(jpaVendorAdapter);
        emfb.setPackagesToScan("org.pwsz.sops.egeriaEntities");
        emfb.setPersistenceUnitName("defaultPersistenceUnit");
        emfb.afterPropertiesSet();
        return emfb.getObject();
    }


    @Bean(name = "egeriaTransactionManager")
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(egeriaEntityManagerFactory());
    }

}
