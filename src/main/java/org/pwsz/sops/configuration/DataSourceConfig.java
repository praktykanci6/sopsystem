/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pwsz.sops.configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author user
 */

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "defaultEntityManagerFactory",
        transactionManagerRef = "defaultTransactionManager",
        basePackages = "org.pwsz.sops.repositories")
public class DataSourceConfig {

    @Autowired
    @Qualifier("defaultJpaVendorAdapter")
    JpaVendorAdapter jpaVendorAdapter;

    @Value("${default.jpa.show-sql:false}")
    private boolean showSql;

    @Value("${default.datasource.url}")
    private String databaseUrl;

    @Value("${default.datasource.username}")
    private String userName;

    @Value("${default.datasource.password}")
    private String password;

    @Value("${default.datasource.driverClassName}")
    private String driverClassName;

    @Value("${default.jpa.hibernate.ddl-auto}")
    private String hibernateDdlAuto;

    @Bean(name = "defaultJpaVendorAdapter")
    public HibernateJpaVendorAdapter hibernateJpaVendorAdapter() {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setShowSql(showSql);
        adapter.getJpaPropertyMap().put("hibernate.hbm2ddl.auto", hibernateDdlAuto);
        adapter.setDatabase(Database.POSTGRESQL);
        return adapter;
    }

    @Primary
    @Bean(name = "defaultDataSource")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource(databaseUrl, userName, password);
        dataSource.setDriverClassName(driverClassName);
        return dataSource;
    }
    @Primary
    @Bean(name = "defaultEntityManager")
    public EntityManager entityManager() {
        return defaultEntityManagerFactory().createEntityManager();
    }

    @Bean(name = "defaultEntityManagerFactory")
    public EntityManagerFactory defaultEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
        emfb.setDataSource(dataSource());
        emfb.setJpaVendorAdapter(jpaVendorAdapter);
        emfb.setPackagesToScan("org.pwsz.sops.entities");
        emfb.setPersistenceUnitName("defaultPersistenceUnit");
        emfb.afterPropertiesSet();
        return emfb.getObject();
    }


    @Bean(name = "defaultTransactionManager")
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(defaultEntityManagerFactory());
    }

}
