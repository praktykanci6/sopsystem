package org.pwsz.sops.sheduled;

import org.pwsz.sops.entities.AcademicYear;
import org.pwsz.sops.entities.Users;
import org.pwsz.sops.repositories.UserRepository;
import org.pwsz.sops.services.EmailSender;
import org.pwsz.sops.services.PracticeService;
import org.pwsz.sops.services.YearbookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * class odpowiedziala za zadnia cykliczne
 * Created by tifet on 2017-06-12.
 */
@Component
public class SheduledTask {

    @Autowired
    UserRepository userRepository;
    @Autowired
    PracticeService practiceService;
    @Autowired
    YearbookService yearbookService;

    @Autowired
    EmailSender emailSender;

    @Autowired
    MailSender mailSender;


    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
            "MM/dd/yyyy HH:mm:ss");

    @Scheduled(cron = "0 0 22 * * ?")
    public void performTaskUsingCron() {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setText("Hello from Spring Boot Application");
        message.setTo("dawidsaramak@gmail.com");
        message.setFrom("goliat82@wp.pl");
        System.out.println("Regular task performed using Cron at "
                + dateFormat.format(new Date()));
        List<Users> list =  userRepository.findAll();
        System.out.println("pobrani user"+ list.size());
        yearbookService.getYerbookCoursesList();
//        emailSender.sendEmail("dawidsaramak@gmial.com","Spring boot","wysłanoz z sops");


    }
}
