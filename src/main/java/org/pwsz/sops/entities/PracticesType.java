package org.pwsz.sops.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by dawid-ss on 17.10.2016.
 */
@Entity
@Table(name = "typy_praktyk")
public class PracticesType {
    @Id
    @Column(name = "id_typu_praktyki")
    private short id;
    @Column(name = "typ_praktyki",length = 20)
    private String type;
//    @OneToMany(mappedBy = "practicesType")
//    private Set<PracticesCourses>practicesCourses;

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

//    public Set<PracticesCourses> getPracticesCourses() {
//        return practicesCourses;
//    }
//
//    public void setPracticesCourses(Set<PracticesCourses> practicesCourses) {
//        this.practicesCourses = practicesCourses;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PracticesType)) return false;

        PracticesType that = (PracticesType) o;

        if (id != that.id) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = (int) id;
        result = 31 * result + type.hashCode();
        return result;
    }
}
