package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dawid-ss on 17.10.2016.
 */
@Entity
@Table(name= "wybory_praktyk" )
public class ChoosePractices implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_wyboru_praktyki")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_praktyki_studenckiej",referencedColumnName = "id_praktyki_studenckiej",nullable = false)
    private Practice practice;

    @Column(name = "priotytet")
    private short priority;

    @ManyToOne
    @JoinColumn(name = "id_studenta_kierunku",referencedColumnName = "id_studenta_kierunku",nullable = false)
    private StudentsCourses studentsCours;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Practice getPractice() {
        return practice;
    }

    public void setPractice(Practice practice) {
        this.practice = practice;
    }

    public short getPriority() {
        return priority;
    }

    public void setPriority(short priority) {
        this.priority = priority;
    }

    public StudentsCourses getStudentsCours() {
        return studentsCours;
    }

    public void setStudentsCours(StudentsCourses studentsCours) {
        this.studentsCours = studentsCours;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChoosePractices that = (ChoosePractices) o;

        if (priority != that.priority) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!practice.equals(that.practice)) return false;
        return studentsCours.equals(that.studentsCours);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + practice.hashCode();
        result = 31 * result + (int) priority;
        result = 31 * result + studentsCours.hashCode();
        return result;
    }
}
