package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dawid-ss on 18.10.2016.
 */
@Entity
@Table(name = "opiekunowie_kierunkowi")
public class AssistenCourses implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_opiekuna_kierunkowego")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy" ,nullable = false)
    private Employer employer;
    @ManyToOne
    @JoinColumn(name = "id_opiekuna_praktyk",nullable = false)
    private AssistentPractices assistentPractices;
    @ManyToOne
    @JoinColumn(name = "id_rocznika",referencedColumnName = "id_rocznika")
    private Yearbook yearbook;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public AssistentPractices getAssistentPractices() {
        return assistentPractices;
    }

    public void setAssistentPractices(AssistentPractices assistentPractices) {
        this.assistentPractices = assistentPractices;
    }

    public Yearbook getYearbook() {
        return yearbook;
    }

    public void setYearbook(Yearbook yearbook) {
        this.yearbook = yearbook;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AssistenCourses that = (AssistenCourses) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!employer.equals(that.employer)) return false;
        if (!assistentPractices.equals(that.assistentPractices)) return false;
        return yearbook.equals(that.yearbook);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + employer.hashCode();
        result = 31 * result + assistentPractices.hashCode();
        result = 31 * result + yearbook.hashCode();
        return result;
    }
}
