package org.pwsz.sops.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.pwsz.sops.entities.Dictionary.PracticeStatus;
import org.pwsz.sops.entities.Dictionary.PracticeType;

/**
 * Created by dawid-ss on 01.11.2016.
 */
@Entity
@Table(name = "praktyki")
public class Practice {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_praktyki_studenckiej")
    private Long id;
//    @ManyToOne
//    @JoinColumn(name = "id_szablonu")
//    private PatternPractices patternPractices;
    @JsonIgnoreProperties
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy")
    private Employer employer;
    @Temporal(TemporalType.DATE)
    @Column(name = "data_rozpoczecia")
    private Date startDate;
    @Temporal(TemporalType.DATE)
    @Column(name = "data_zakonczenia")
    private Date endDate;
    @ManyToOne
    @JoinColumn(name = "id_porozumienia")
    private Agreement agreement;
    @Temporal(TemporalType.DATE)
    @Column(name="data_zapisu")
    private Date enrolDate;
    @Column(name = "ocena")
    private Short grade;
    //skierowanie na praktyki oraz w zalerznosi od typu praktyk karta dodania zakladu pracy lib umowa o prace,
    @Column(columnDefinition="BINARY(32)", name = "skierowanie")
    private byte[] referrals;
    @Column(name = "skierowanie_nazwa")
    private String referraFileName;
    @Column(columnDefinition="BINARY(32)", name = "karta_oceny")
    private byte[] evaluationCard;
//    @Type(type="org.hibernate.type.BlobType")
    @Column(columnDefinition="BINARY(32)", name = "oswiadczenie_praktykodawcy")
    private Byte[] statment;
    @Column(name="wymagana_kwalifikacja")
    private boolean interwiev;
    @Temporal(TemporalType.DATE)
    @Column(name = "data_zgloszenia")
    private Date creatDate;
    @ManyToOne
    @JoinColumn(name = "id_koordynatora_kierunkowego_praktykodawcy",referencedColumnName = "id_koordynatora_kierunkowego_praktykodawcy")
    private CoordinatorCoursesEmployer coordinatorCoursesEmployer;
    @ManyToOne
    @JoinColumn(name = "id_studenta_kierunku",referencedColumnName = "id_studenta_kierunku")
    private StudentsCourses studentsCourses;
    @ManyToOne
    @JoinColumn(name = "id_adresu_praktykodawcy")
    private EmployAddress employAddress;
    @ManyToOne
    @JoinColumn(name = "id_opiekuna_kierunkowego",referencedColumnName = "id_opiekuna_kierunkowego")
    private AssistenCourses assistenCourses;
    @ManyToOne
    @JoinColumn(name = "id_praktyki_kierunkowej")
    private PracticesCourses practicesCourses;

    @Column(name = "typ_praktyk")
    @Enumerated(EnumType.STRING)
    private PracticeType practiceType;

    @Column(name="status_praktyki")
    @Enumerated(EnumType.STRING)
    private PracticeStatus status;

    @Column(name="wymiar_etatu")
    String employmentDimension;

    @Column(name="liczba_osobogodzin")
    private BigDecimal hours;

    public PracticeType getPracticeType() {
        return practiceType;
    }

    public void setPracticeType(PracticeType practiceType) {
        this.practiceType = practiceType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public PatternPractices getPatternPractices() {
//        return patternPractices;
//    }
//
//    public void setPatternPractices(PatternPractices patternPractices) {
//        this.patternPractices = patternPractices;
//    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Agreement getAgreement() {
        return agreement;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }

    public Date getEnrolDate() {
        return enrolDate;
    }

    public void setEnrolDate(Date enrolDate) {
        this.enrolDate = enrolDate;
    }

    public Short getGrade() {
        return grade;
    }

    public void setGrade(Short grade) {
        this.grade = grade;
    }

    public byte[] getReferrals() {
        return referrals;
    }

    public void setReferrals(byte[] referrals) {
        this.referrals = referrals;
    }

    public byte[] getEvaluationCard() {
        return evaluationCard;
    }

    public void setEvaluationCard(byte[] evaluationCard) {
        this.evaluationCard = evaluationCard;
    }

    public Byte[] getStatment() {
        return statment;
    }

    public void setStatment(Byte[] statment) {
        this.statment = statment;
    }

    public boolean isInterwiev() {
        return interwiev;
    }

    public void setInterwiev(boolean interwiev) {
        this.interwiev = interwiev;
    }

    public Date getCreatDate() {
        return creatDate;
    }

    public void setCreatDate(Date creatDate) {
        this.creatDate = creatDate;
    }

    public CoordinatorCoursesEmployer getCoordinatorCoursesEmployer() {
        return coordinatorCoursesEmployer;
    }

    public void setCoordinatorCoursesEmployer(CoordinatorCoursesEmployer coordinatorCoursesEmployer) {
        this.coordinatorCoursesEmployer = coordinatorCoursesEmployer;
    }

    public StudentsCourses getStudentsCourses() {
        return studentsCourses;
    }

    public void setStudentsCourses(StudentsCourses studentsCourses) {
        this.studentsCourses = studentsCourses;
    }

    public EmployAddress getEmployAddress() {
        return employAddress;
    }

    public void setEmployAddress(EmployAddress employAddress) {
        this.employAddress = employAddress;
    }

    public AssistenCourses getAssistenCourses() {
        return assistenCourses;
    }

    public void setAssistenCourses(AssistenCourses assistenCourses) {
        this.assistenCourses = assistenCourses;
    }

    public PracticesCourses getPracticesCourses() {
        return practicesCourses;
    }

    public void setPracticesCourses(PracticesCourses practicesCourses) {
        this.practicesCourses = practicesCourses;
    }

    public String getReferraFileName() {
        return referraFileName;
    }

    public void setReferraFileName(String referraFileName) {
        this.referraFileName = referraFileName;
    }

    public PracticeStatus getStatus() {
        return status;
    }

    public void setStatus(PracticeStatus status) {
        this.status = status;
    }

    public String getEmploymentDimension() {
        return employmentDimension;
    }

    public void setEmploymentDimension(String employmentDimension) {
        this.employmentDimension = employmentDimension;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Practice)) return false;

        Practice practice = (Practice) o;

        if (!employer.equals(practice.employer)) return false;
        if (!startDate.equals(practice.startDate)) return false;
        if (!endDate.equals(practice.endDate)) return false;
        if (!employAddress.equals(practice.employAddress)) return false;
        if (!assistenCourses.equals(practice.assistenCourses)) return false;
        return practicesCourses.equals(practice.practicesCourses);
    }

    @Override
    public int hashCode() {
        int result = employer.hashCode();
        result = 31 * result +  startDate.hashCode();
        result = 31 * result + endDate.hashCode();
        result = 31 * result + employAddress.hashCode();
        result = 31 * result + assistenCourses.hashCode();
        result = 31 * result + practicesCourses.hashCode();
        return result;
    }


}
