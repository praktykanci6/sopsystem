package org.pwsz.sops.entities;

import javax.persistence.*;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name="stopnie_studiow")
public class StudiesDegree {

    @Id
    @Column(name="id_stopnia_studiow")
    private short id;
    @Column(name="stopien_studiow")
    private String degreeStudies;

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getDegreeStudies() {
        return degreeStudies;
    }

    public void setDegreeStudies(String degreeStudies) {
        this.degreeStudies = degreeStudies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudiesDegree that = (StudiesDegree) o;

        if (id != that.id) return false;
        return degreeStudies.equals(that.degreeStudies);

    }

    @Override
    public int hashCode() {
        int result = (int) id;
        result = 31 * result + degreeStudies.hashCode();
        return result;
    }
}
