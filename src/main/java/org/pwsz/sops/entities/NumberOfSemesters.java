package org.pwsz.sops.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name="liczba_semestrow")
public class NumberOfSemesters {
    @Id
    @Column(name="id_liczby_semestrow")
    private Integer id;
    @Column(name="liczba_semestrow")
    private Integer numberSemesters;
    @JsonIgnore
    @Column(name="archiwum")
    private Boolean archives;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumberSemesters() {
        return numberSemesters;
    }

    public void setNumberSemesters(Integer numberSemesters) {
        this.numberSemesters = numberSemesters;
    }

    public Boolean getArchives() {
        return archives;
    }

    public void setArchives(Boolean archives) {
        this.archives = archives;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NumberOfSemesters)) return false;

        NumberOfSemesters that = (NumberOfSemesters) o;

        if (!id.equals(that.id)) return false;
        if (!numberSemesters.equals(that.numberSemesters)) return false;
        return archives.equals(that.archives);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + numberSemesters.hashCode();
        result = 31 * result + archives.hashCode();
        return result;
    }
}
