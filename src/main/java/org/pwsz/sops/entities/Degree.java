package org.pwsz.sops.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name ="tytuly_zawodowe" )
public class Degree implements Serializable{

    @Id
    @Column(name = "id_tytulu_zawodowego")
    private Short id;
    @Column(name = "tytul_zawodowy",nullable = false)
    private String degreeTitle;
    @JsonIgnore
    @Column(name = "archiwum",nullable = false)
    private Boolean archiwes;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getDegreeTitle() {
        return degreeTitle;
    }

    public void setDegreeTitle(String degreeTitle) {
        this.degreeTitle = degreeTitle;
    }

    public Boolean getArchiwes() {
        return archiwes;
    }

    public void setArchiwes(Boolean archiwes) {
        this.archiwes = archiwes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Degree degree = (Degree) o;

        if (!id.equals(degree.id)) return false;
        if (!degreeTitle.equals(degree.degreeTitle)) return false;
        return archiwes.equals(degree.archiwes);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + degreeTitle.hashCode();
        result = 31 * result + archiwes.hashCode();
        return result;
    }
}
