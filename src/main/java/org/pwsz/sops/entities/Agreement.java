package org.pwsz.sops.entities;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by dawid-ss on 20.11.2016.
 */
@Entity
@Table(name = "porozumienia")
public class Agreement {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_porozumienia")
    private Long id;
    @Column(name = "nr_porozumienia")
    private String number;
    @Column(name ="data_zawarcia" )
    @Temporal(TemporalType.DATE)
    private Date createDate;
    @Lob
    @Column(name = "porozumienie")
    private Byte[]agreement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Byte[] getAgreement() {
        return agreement;
    }

    public void setAgreement(Byte[] agreement) {
        this.agreement = agreement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Agreement agreement1 = (Agreement) o;

        if (id != null ? !id.equals(agreement1.id) : agreement1.id != null) return false;
        if (!number.equals(agreement1.number)) return false;
        if (!createDate.equals(agreement1.createDate)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(agreement, agreement1.agreement);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + number.hashCode();
        result = 31 * result + createDate.hashCode();
        result = 31 * result + Arrays.hashCode(agreement);
        return result;
    }
}
