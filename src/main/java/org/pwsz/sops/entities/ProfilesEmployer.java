package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dawidss on 04.10.2016.
 */
@Entity
@Table(name="profile_praktykodawcy")
public class ProfilesEmployer implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy",referencedColumnName = "id_praktykodawcy")
    private Employer employer;
    @Id
    @ManyToOne
    @JoinColumn(name = "id_profilu",referencedColumnName = "id_profilu")
    private Profil profil;

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfilesEmployer that = (ProfilesEmployer) o;

        if (!employer.equals(that.employer)) return false;
        return profil.equals(that.profil);

    }

    @Override
    public int hashCode() {
        int result = employer.hashCode();
        result = 31 * result + profil.hashCode();
        return result;
    }
}
