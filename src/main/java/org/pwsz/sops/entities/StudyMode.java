package org.pwsz.sops.entities;

import javax.persistence.*;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name="tryby_studiow")
public class StudyMode {
    @Id
    @Column(name = "id_trybu_studiow")
    private Short id;
    @Column(name="tryb_studiow")
    private String modeOfStudy;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getModeOfStudy() {
        return modeOfStudy;
    }

    public void setModeOfStudy(String modeOfStudy) {
        this.modeOfStudy = modeOfStudy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudyMode studyMode = (StudyMode) o;

        if (!id.equals(studyMode.id)) return false;
        return modeOfStudy.equals(studyMode.modeOfStudy);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + modeOfStudy.hashCode();
        return result;
    }
}
