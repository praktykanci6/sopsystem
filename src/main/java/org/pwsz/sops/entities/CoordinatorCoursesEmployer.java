package org.pwsz.sops.entities;

import org.hibernate.annotations.Columns;

import javax.persistence.*;
import javax.print.attribute.standard.MediaSize;
import java.io.Serializable;

/**
 * Created by dawid-ss on 01.11.2016.
 */
@Entity
@Table(name = "koordynatorzy_kierunkowi_praktykodawcow")
public class CoordinatorCoursesEmployer  {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_koordynatora_kierunkowego_praktykodawcy")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy")
    private Employer employer;
    @ManyToOne
    @JoinColumn(name = "id_kordynatora_kierunkowego")
    private CoordinatorCourses coordinatorCourses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public CoordinatorCourses getCoordinatorCourses() {
        return coordinatorCourses;
    }

    public void setCoordinatorCourses(CoordinatorCourses coordinatorCourses) {
        this.coordinatorCourses = coordinatorCourses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CoordinatorCoursesEmployer that = (CoordinatorCoursesEmployer) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!employer.equals(that.employer)) return false;
        return coordinatorCourses.equals(that.coordinatorCourses);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + employer.hashCode();
        result = 31 * result + coordinatorCourses.hashCode();
        return result;
    }
}
