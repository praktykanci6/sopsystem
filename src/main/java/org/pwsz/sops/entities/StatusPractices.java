package org.pwsz.sops.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by dawid-ss on 18.10.2016.
 */
@Entity
@Table(name = "statusy_praktyk")
public class StatusPractices {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_statusu_praktkyki")
    private long id;
    @ManyToOne
    @JoinColumn(name = "id_praktyki_studenckiej")
    private Practice practices;
    @Temporal(TemporalType.DATE)
    @Column(name = "data_rozpoczecia")
    private Date startDate;
    @Temporal(TemporalType.DATE)
    @Column(name = "data_konca")
    private Date endDate;
    @Column(name = "biezacy")
    private boolean current;
    @ManyToOne
    @JoinColumn(name = "id_statusu")
    private Status status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Practice getPractices() {
        return practices;
    }

    public void setPractices(Practice practices) {
        this.practices = practices;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
