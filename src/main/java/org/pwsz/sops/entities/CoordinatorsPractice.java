package org.pwsz.sops.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dawidss on 04.10.2016.
 */
@Entity
@Table(name="koordynatorzy_praktyk")
public class CoordinatorsPractice {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_koordynatora_praktyk")
    private  Long id;
    @ManyToOne
    @JoinColumn(name="id_osoby")
    private Person person ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CoordinatorsPractice)) return false;

        CoordinatorsPractice that = (CoordinatorsPractice) o;

        if (!id.equals(that.id)) return false;
        return person.equals(that.person);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + person.hashCode();
        return result;
    }
}
