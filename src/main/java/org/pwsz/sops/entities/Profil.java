package org.pwsz.sops.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by dawid-ss on 20.11.2016.
 */
@Entity
@Table(name = "profil")
public class Profil {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_profilu")
    private int id;
    @Column(name="branza")
    private String sector;
    @OneToMany(mappedBy = "profil" )
    private List<ProfilesEmployer> employer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public List<ProfilesEmployer> getEmployer() {
        return employer;
    }

    public void setEmployer(List<ProfilesEmployer> employer) {
        this.employer = employer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Profil profil = (Profil) o;

        if (id != profil.id) return false;
        return sector.equals(profil.sector);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + sector.hashCode();
        return result;
    }
}
