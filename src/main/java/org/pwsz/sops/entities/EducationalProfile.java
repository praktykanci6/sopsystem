package org.pwsz.sops.entities;

import javax.persistence.*;

/**
 * Created by dawidss on 25.08.2016.
 */

@Entity
@Table(name="profile_ksztalcenia")
public class EducationalProfile {

    @Id
    @Column(name="id_profilu_ksztalcenia")
    private Long id;
    @Column(name="profil_ksztalcenia")
    private String educationProfile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEducationProfile() {
        return educationProfile;
    }

    public void setEducationProfile(String educationProfile) {
        this.educationProfile = educationProfile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EducationalProfile that = (EducationalProfile) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return educationProfile != null ? educationProfile.equals(that.educationProfile) : that.educationProfile == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (educationProfile != null ? educationProfile.hashCode() : 0);
        return result;
    }
}

