package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name = "adresy")

public class Address implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_adresu")
    private Long id;
    @Column(name = "ulica")
    private String street;
    @Column(name = "nr_budynku")
    private String numberBuilding;
    @Column(name = "miejscowosc")
    private String city;
    @Column(name = "kraj")
    private String country;
    @Column(name="telefon1")
    private String phone1;
    @Column(name = "telefon2")
    private String phone2;
    @Column(name = "fax")
    private String fax;
    @Column(name = "kod_pocztowy")
    private String postCode;
//    @OneToMany(mappedBy = "address")
//    private Set<EmployAddress> employAddresses;

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumberBuilding() {
        return numberBuilding;
    }

    public void setNumberBuilding(String numberBuilding) {
        this.numberBuilding = numberBuilding;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;

        Address address = (Address) o;

        if (!id.equals(address.id)) return false;
        if (!street.equals(address.street)) return false;
        if (!numberBuilding.equals(address.numberBuilding)) return false;
        if (!city.equals(address.city)) return false;
        if (!country.equals(address.country)) return false;
        if (phone1 != null ? !phone1.equals(address.phone1) : address.phone1 != null) return false;
        if (phone2 != null ? !phone2.equals(address.phone2) : address.phone2 != null) return false;
        if (fax != null ? !fax.equals(address.fax) : address.fax != null) return false;
        return postCode.equals(address.postCode);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + street.hashCode();
        result = 31 * result + numberBuilding.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + country.hashCode();
        result = 31 * result + (phone1 != null ? phone1.hashCode() : 0);
        result = 31 * result + (phone2 != null ? phone2.hashCode() : 0);
        result = 31 * result + (fax != null ? fax.hashCode() : 0);
        result = 31 * result + postCode.hashCode();
        return result;
    }
}
