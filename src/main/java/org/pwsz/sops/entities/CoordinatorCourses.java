package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name = "koordynatorzy_kierunkowi")
public class CoordinatorCourses implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_kordynatora_kierunkowego")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_koordynatora_praktyk")
    private CoordinatorsPractice coordinatorsPractice;
    @Temporal(TemporalType.DATE)
    @Column(name = "data_powolania")
    private Date create_date;
    @ManyToOne
    @JoinColumn(name = "id_rocznika")
    private Yearbook yearbook;

    @OneToMany(mappedBy = "coordinatorCourses")
    private List<CoordinatorCoursesEmployer>coordinatorCoursesEmployerList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CoordinatorsPractice getCoordinatorsPractice() {
        return coordinatorsPractice;
    }

    public void setCoordinatorsPractice(CoordinatorsPractice coordinatorsPractice) {
        this.coordinatorsPractice = coordinatorsPractice;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Yearbook getYearbook() {
        return yearbook;
    }

    public void setYearbook(Yearbook yearbook) {
        this.yearbook = yearbook;
    }

    public List<CoordinatorCoursesEmployer> getCoordinatorCoursesEmployerList() {
        return coordinatorCoursesEmployerList;
    }

    public void setCoordinatorCoursesEmployerList(List<CoordinatorCoursesEmployer> coordinatorCoursesEmployerList) {
        this.coordinatorCoursesEmployerList = coordinatorCoursesEmployerList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CoordinatorCourses that = (CoordinatorCourses) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!coordinatorsPractice.equals(that.coordinatorsPractice)) return false;
        if (create_date != null ? !create_date.equals(that.create_date) : that.create_date != null) return false;
        return yearbook.equals(that.yearbook);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + coordinatorsPractice.hashCode();
        result = 31 * result + (create_date != null ? create_date.hashCode() : 0);
        result = 31 * result + yearbook.hashCode();
        return result;
    }
}
