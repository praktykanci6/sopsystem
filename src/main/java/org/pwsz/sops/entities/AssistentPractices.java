package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by dawid-ss on 18.10.2016.
 */
@Entity
@Table(name = "opiekunowie_praktyk")
public class AssistentPractices implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_opiekuna_praktyk")
    private Long id;
    @Column(name =  "stanowisko",length = 50)
    private String position;
    @ManyToOne
    @JoinColumn(name = "id_osoby")
    private Person person;

    @OneToMany(mappedBy = "assistentPractices")
    private Set<AssistenCourses> assistenCourses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Set<AssistenCourses> getAssistenCourses() {
        return assistenCourses;
    }

    public void setAssistenCourses(Set<AssistenCourses> assistenCourses) {
        this.assistenCourses = assistenCourses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AssistentPractices that = (AssistentPractices) o;

        if (id != that.id) return false;
        if (!position.equals(that.position)) return false;
        return person != null ? person.equals(that.person) : that.person == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + position.hashCode();
        result = 31 * result + (person != null ? person.hashCode() : 0);
        return result;
    }
}
