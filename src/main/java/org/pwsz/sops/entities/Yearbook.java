package org.pwsz.sops.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name = "roczniki_studiow")
public class Yearbook implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_rocznika")
    private Long id;    
    @ManyToOne
    @JoinColumn(name = "id_roku_akademickiego")
    private AcademicYear academicYear;    
    @ManyToOne    
    @JoinColumn(name = "id_kierunku")
    private CoursesOfStudy courseOfStudy;
    @Column(name = "rozpoczecie_zapisow_rozmowy")
    @Temporal(TemporalType.DATE)
    private Date startInterviews;
    @Column(name="zakoczenie_zapisow_rozmowy")
    @Temporal(TemporalType.DATE)
    private Date endInterviews;
    @Column(name = "rozpoczecie_wyborow_praktyk")
    @Temporal(TemporalType.DATE)
    private Date startChoice;
    @Column(name = "zakonczenie_wyborow_praktyk")
    @Temporal(TemporalType.DATE)
    private Date endChoice;
    @Column(name="rozpoczecie_praktyk")
    @Temporal(TemporalType.DATE)
    private Date startPractice;
    @Column(name="zakonczenie_praktyk")
    @Temporal(TemporalType.DATE)
    private Date endPractice;
    @JsonIgnore
    @OneToMany(mappedBy = "yearbook")
    private Set<StudentsCourses> studentsCourses;
    @JsonIgnore
    @OneToMany(mappedBy = "yearbook")
    private Set<AssistenCourses> assistenCourses;
    @JsonIgnore
    @OneToMany(mappedBy = "yearbook")
    private Set<PracticesCourses> practicesCourses;
    @JsonIgnore
    @OneToMany(mappedBy = "yearbook")
    private List<CoordinatorCourses> coordinatorCourses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AcademicYear getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(AcademicYear academicYear) {
        this.academicYear = academicYear;
    }

    public CoursesOfStudy getCourseOfStudy() {
        return courseOfStudy;
    }

    public void setCourseOfStudy(CoursesOfStudy courseOfStudy) {
        this.courseOfStudy = courseOfStudy;
    }

    public Date getStartInterviews() {
        return startInterviews;
    }

    public void setStartInterviews(Date startInterviews) {
        this.startInterviews = startInterviews;
    }

    public Date getEndInterviews() {
        return endInterviews;
    }

    public void setEndInterviews(Date endInterviews) {
        this.endInterviews = endInterviews;
    }

    public Date getStartChoice() {
        return startChoice;
    }

    public void setStartChoice(Date startChoice) {
        this.startChoice = startChoice;
    }

    public Date getEndChoice() {
        return endChoice;
    }

    public void setEndChoice(Date endChoice) {
        this.endChoice = endChoice;
    }

    public Date getStartPractice() {
        return startPractice;
    }

    public void setStartPractice(Date startPractice) {
        this.startPractice = startPractice;
    }

    public Date getEndPractice() {
        return endPractice;
    }

    public void setEndPractice(Date endPractice) {
        this.endPractice = endPractice;
    }

    public Set<StudentsCourses> getStudentsCourses() {
        return studentsCourses;
    }

    public void setStudentsCourses(Set<StudentsCourses> studentsCourses) {
        this.studentsCourses = studentsCourses;
    }

    public Set<AssistenCourses> getAssistenCourses() {
        return assistenCourses;
    }

    public void setAssistenCourses(Set<AssistenCourses> assistenCourses) {
        this.assistenCourses = assistenCourses;
    }

    public Set<PracticesCourses> getPracticesCourses() {
        return practicesCourses;
    }

    public void setPracticesCourses(Set<PracticesCourses> practicesCourses) {
        this.practicesCourses = practicesCourses;
    }

    public List<CoordinatorCourses> getCoordinatorCourses() {
        return coordinatorCourses;
    }

    public void setCoordinatorCourses(List<CoordinatorCourses> coordinatorCourses) {
        this.coordinatorCourses = coordinatorCourses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Yearbook)) return false;

        Yearbook yearbook = (Yearbook) o;

        if (!id.equals(yearbook.id)) return false;
        if (!academicYear.equals(yearbook.academicYear)) return false;
        if (!courseOfStudy.equals(yearbook.courseOfStudy)) return false;
        if (startInterviews != null ? !startInterviews.equals(yearbook.startInterviews) : yearbook.startInterviews != null)
            return false;
        if (endInterviews != null ? !endInterviews.equals(yearbook.endInterviews) : yearbook.endInterviews != null)
            return false;
        if (startChoice != null ? !startChoice.equals(yearbook.startChoice) : yearbook.startChoice != null)
            return false;
        if (endChoice != null ? !endChoice.equals(yearbook.endChoice) : yearbook.endChoice != null) return false;
        if (startPractice != null ? !startPractice.equals(yearbook.startPractice) : yearbook.startPractice != null)
            return false;
        if (endPractice != null ? !endPractice.equals(yearbook.endPractice) : yearbook.endPractice != null)
            return false;
        return coordinatorCourses != null ? coordinatorCourses.equals(yearbook.coordinatorCourses) : yearbook.coordinatorCourses == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + academicYear.hashCode();
        result = 31 * result + courseOfStudy.hashCode();
        result = 31 * result + (startInterviews != null ? startInterviews.hashCode() : 0);
        result = 31 * result + (endInterviews != null ? endInterviews.hashCode() : 0);
        result = 31 * result + (startChoice != null ? startChoice.hashCode() : 0);
        result = 31 * result + (endChoice != null ? endChoice.hashCode() : 0);
        result = 31 * result + (startPractice != null ? startPractice.hashCode() : 0);
        result = 31 * result + (endPractice != null ? endPractice.hashCode() : 0);
        result = 31 * result + (coordinatorCourses != null ? coordinatorCourses.hashCode() : 0);
        return result;
    }
}
