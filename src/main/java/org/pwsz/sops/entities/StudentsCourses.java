package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dawid-ss on 01.11.2016.
 */
@Entity
@Table(name = "studenci_kierunkow")
public class StudentsCourses implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_studenta_kierunku")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "nr_albumu",referencedColumnName = "nr_albumu")
    private Students students;

    @ManyToOne
    @JoinColumn(name = "id_rocznika",referencedColumnName = "id_rocznika")
    private Yearbook yearbook;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Students getStudents() {
        return students;
    }

    public void setStudents(Students students) {
        this.students = students;
    }

    public Yearbook getYearbook() {
        return yearbook;
    }

    public void setYearbook(Yearbook yearbook) {
        this.yearbook = yearbook;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudentsCourses that = (StudentsCourses) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!students.equals(that.students)) return false;
        return yearbook.equals(that.yearbook);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + students.hashCode();
        result = 31 * result + yearbook.hashCode();
        return result;
    }
}
