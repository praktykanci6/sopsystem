package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dawid-ss on 17.10.2016.
 */
@Entity
@Table(name = "praktyki_kierunkowe")
public class PracticesCourses  implements Serializable{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_praktyki_kierunkowej")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_typu_praktyki" )
    private PracticesType practicesType;
    @ManyToOne
    @JoinColumn(name = "id_roku_akademickiego")
    private AcademicYear academicYear;
    @Column(name = "liczba_godzin")
    private Short numberHoures;
    @Column(name = "czas_trwania")
    private Short durtion;
    @ManyToOne
    @JoinColumn(name = "id_jednostki",referencedColumnName = "id_jednostki_kalendarzowej")
    private UnitCalendar unitCalendar;
    @ManyToOne
    @JoinColumn(name = "id_rocznika")
    private Yearbook yearbook;

//    @OneToMany(mappedBy = "practicesCourses")
//    private List<Practice> practice;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getNumberHoures() {
        return numberHoures;
    }

    public void setNumberHoures(Short numberHoures) {
        this.numberHoures = numberHoures;
    }

    public Short getDurtion() {
        return durtion;
    }

    public void setDurtion(Short durtion) {
        this.durtion = durtion;
    }

    public PracticesType getPracticesType() {
        return practicesType;
    }

    public void setPracticesType(PracticesType practicesType) {
        this.practicesType = practicesType;
    }

    public AcademicYear getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(AcademicYear academicYear) {
        this.academicYear = academicYear;
    }

    public UnitCalendar getUnitCalendar() {
        return unitCalendar;
    }

    public void setUnitCalendar(UnitCalendar unitCalendar) {
        this.unitCalendar = unitCalendar;
    }


    public Yearbook getYearbook() {
        return yearbook;
    }

    public void setYearbook(Yearbook yearbook) {
        this.yearbook = yearbook;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PracticesCourses that = (PracticesCourses) o;


        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!practicesType.equals(that.practicesType)) return false;
        if (!academicYear.equals(that.academicYear)) return false;
        if (numberHoures != null ? !numberHoures.equals(that.numberHoures) : that.numberHoures != null) return false;
        if (durtion != null ? !durtion.equals(that.durtion) : that.durtion != null) return false;
        return yearbook.equals(that.yearbook);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + practicesType.hashCode();
        result = 31 * result + academicYear.hashCode();
        result = 31 * result + (numberHoures != null ? numberHoures.hashCode() : 0);
        result = 31 * result + (durtion != null ? durtion.hashCode() : 0);
        result = 31 * result + yearbook.hashCode();
        return result;
    }
}
