package org.pwsz.sops.entities.Dictionary;

/**
 * Created by tifet on 2017-05-24.
 */
public enum PracticeType {
    SCHOOL,
    STUDENT,
    AGREEMENT
}