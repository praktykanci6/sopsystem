package org.pwsz.sops.entities.Dictionary;

/**
 * Created by tifet on 2017-05-24.
 */
public enum PracticeStatus{
    NEW,
    ACCEPTED,
    REJECTED,
    COMPLETED,
    DELETED
}