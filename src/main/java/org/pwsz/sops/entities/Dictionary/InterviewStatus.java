package org.pwsz.sops.entities.Dictionary;

/**
 * Created by tifet on 2017-05-30.
 */
public enum InterviewStatus {

    NEW,
    PASSED,
    FAILED,
    NOT_ATTEMPTED
}
