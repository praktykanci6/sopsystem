package org.pwsz.sops.entities;



import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.List;
import javax.validation.constraints.Size;


/**
 * Created by dawidss on 25.08.2016.
 */

@Entity
@Table(name = "praktykodawcy")
public class Employer {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_praktykodawcy")
    private Long id;
    @Column(name="nazwa",nullable = false)    
    @Size(max = 30)
    private String name;    
    @Size(max = 100)
    @Column(name="pelna_nazwa",nullable = false)
    private String fullName;
    @Column(name="opis")
    private String description;
    @Column(name = "zaufany",nullable = false)
    private boolean confidental;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER , mappedBy = "employer")
    private List<EmployAddress> addresses ;

    @JsonIgnore
    @OneToMany(mappedBy = "employer")
    private List<ProfilesEmployer>profilEmployerses ;
    @JsonIgnore
    @OneToMany(mappedBy = "employer")
    private List< CoordinatorCoursesEmployer> coordinatorCourses ;
    @JsonIgnore
    @OneToMany(mappedBy = "employer")
    private List<AssistenCourses> assistenCourses;
    @JsonIgnore
    @OneToMany(mappedBy = "employer")
    List<Interview> interviews;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean isConfidental() {
        return confidental;
    }

    public void setConfidental(boolean confidental) {
        this.confidental = confidental;
    }

    public List<EmployAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<EmployAddress> addresses) {
        this.addresses = addresses;
    }

    public List<ProfilesEmployer> getProfilEmployerses() {
        return profilEmployerses;
    }

    public void setProfilEmployerses(List<ProfilesEmployer> profilEmployerses) {
        this.profilEmployerses = profilEmployerses;
    }

    public List<CoordinatorCoursesEmployer> getCoordinatorCourses() {
        return coordinatorCourses;
    }

    public void setCoordinatorCourses(List<CoordinatorCoursesEmployer> coordinatorCourses) {
        this.coordinatorCourses = coordinatorCourses;
    }

    public List<AssistenCourses> getAssistenCourses() {
        return assistenCourses;
    }

    public void setAssistenCourses(List<AssistenCourses> assistenCourses) {
        this.assistenCourses = assistenCourses;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Interview> getInterviews() {
        return interviews;
    }

    public void setInterviews(List<Interview> interviews) {
        this.interviews = interviews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employer)) return false;

        Employer employer = (Employer) o;

        if (confidental != employer.confidental) return false;
        if (!id.equals(employer.id)) return false;
        if (!name.equals(employer.name)) return false;
        if (!fullName.equals(employer.fullName)) return false;
        return description != null ? description.equals(employer.description) : employer.description == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + fullName.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (confidental ? 1 : 0);
        return result;
    }
}
