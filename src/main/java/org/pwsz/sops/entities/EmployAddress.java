package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name = "adresy_praktykodawcy")
public class EmployAddress implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_adresu_praktykodawcy")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_adresu",nullable = false)
    private Address address;
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy",nullable = false)
    private Employer employer;
    @Column(name = "oddzial",nullable = false)
    private Boolean branch;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public Boolean getBranch() {
        return branch;
    }

    public void setBranch(Boolean branch) {
        this.branch = branch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmployAddress that = (EmployAddress) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!address.equals(that.address)) return false;
        if (!employer.equals(that.employer)) return false;
        return branch.equals(that.branch);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + address.hashCode();
        result = 31 * result + employer.hashCode();
        result = 31 * result + branch.hashCode();
        return result;
    }
}
