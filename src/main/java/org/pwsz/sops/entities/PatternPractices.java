package org.pwsz.sops.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by dawid-ss on 17.10.2016.
 */
@Entity
@Table(name = "szablon_praktyk")
public class PatternPractices {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_szablonu")
    private int id;
    @Column(name = "szablon",length = 50)
    private String pattern;
    @Column(name ="opis_szablonu",length = 1000)
    private String descriptin;
//    @OneToMany(mappedBy = "patternPractices")
//    private Set<Practices> practices;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getDescriptin() {
        return descriptin;
    }

    public void setDescriptin(String descriptin) {
        this.descriptin = descriptin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PatternPractices that = (PatternPractices) o;

        if (id != that.id) return false;
        if (pattern != null ? !pattern.equals(that.pattern) : that.pattern != null) return false;
        return descriptin != null ? descriptin.equals(that.descriptin) : that.descriptin == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (pattern != null ? pattern.hashCode() : 0);
        result = 31 * result + (descriptin != null ? descriptin.hashCode() : 0);
        return result;
    }
}
