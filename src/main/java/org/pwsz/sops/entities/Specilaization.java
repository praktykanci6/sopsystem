package org.pwsz.sops.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name="specjalnosci")
public class Specilaization {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_specjalnosci")
    private Long id;
    @Column(name = "nazwa_specjalnosci")
    private String nameSpecialization;
    @Column(name="archiwum")
    private boolean arhives;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameSpecialization() {
        return nameSpecialization;
    }

    public void setNameSpecialization(String nameSpecialization) {
        this.nameSpecialization = nameSpecialization;
    }

    public boolean isArhives() {
        return arhives;
    }

    public void setArhives(boolean arhives) {
        this.arhives = arhives;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Specilaization that = (Specilaization) o;

        if (arhives != that.arhives) return false;
        if (!id.equals(that.id)) return false;
        return nameSpecialization.equals(that.nameSpecialization);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + nameSpecialization.hashCode();
        result = 31 * result + (arhives ? 1 : 0);
        return result;
    }
}
