package org.pwsz.sops.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by dawidss on 04.10.2016.
 */
@Entity
@Table(name="studenci")
public class Students {

    @Id
    @Column(name = "nr_albumu",length = 6)
    private String numberAlbum;
    @OneToOne
    @JoinColumn(name ="id_adresu" )
    private Address address;
    @ManyToOne
    @JoinColumn(name = "id_osoby",nullable = false)
    private Person person;
//    @OneToMany(mappedBy = "students")
//    private Set<StudentsCourses> studentsCourses;
    public String getNumberAlbum() {
        return numberAlbum;
    }

    public void setNumberAlbum(String numberAlbum) {
        this.numberAlbum = numberAlbum;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

//    public Set<StudentsCourses> getStudentsCourses() {
//        return studentsCourses;
//    }
//
//    public void setStudentsCourses(Set<StudentsCourses> studentsCourses) {
//        this.studentsCourses = studentsCourses;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Students students = (Students) o;

        if (!numberAlbum.equals(students.numberAlbum)) return false;
        if (!address.equals(students.address)) return false;
        return person.equals(students.person);

    }

    @Override
    public int hashCode() {
        int result = numberAlbum.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + person.hashCode();
        return result;
    }
}
