package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name = "jednostki_kalendarzowe")
public class UnitCalendar implements Serializable {

    @Id
    @Column(name = "id_jednostki_kalendarzowej")
    private Short id;
    @Column(name = "jednostka_kalendarzowa")
    private String unitCalendar;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getUnitCalendar() {
        return unitCalendar;
    }

    public void setUnitCalendar(String unitCalendar) {
        this.unitCalendar = unitCalendar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnitCalendar that = (UnitCalendar) o;

        if (!id.equals(that.id)) return false;
        return unitCalendar.equals(that.unitCalendar);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + unitCalendar.hashCode();
        return result;
    }
}
