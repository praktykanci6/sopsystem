package org.pwsz.sops.entities;

import org.pwsz.sops.entities.Dictionary.InterviewStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by tifet on 2017-05-29.
 */
@Entity
@Table(name="rozmowy_kwalifikacyjne")
public class Interview implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_rozmowy_kwalifikacyjnej")
    Long id;
    @Column(name="termin_rozmowy",columnDefinition= "TIMESTAMP WITH TIME ZONE")
    @Temporal(TemporalType.TIMESTAMP)
    Date dataInterview;
    @Column(name = "ocena")
    Integer grade;
    @Enumerated(EnumType.STRING)
    @Column(name="status")
    private InterviewStatus status;
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy")
    private Employer employer;
    @ManyToOne
    @JoinColumn(name = "id_studenta_kierunku", referencedColumnName = "id_studenta_kierunku")
    StudentsCourses studentsCourses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataInterview() {
        return dataInterview;
    }

    public void setDataInterview(Date dataInterview) {
        this.dataInterview = dataInterview;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public StudentsCourses getStudentsCourses() {
        return studentsCourses;
    }

    public void setStudentsCourses(StudentsCourses studentsCourses) {
        this.studentsCourses = studentsCourses;
    }

    public InterviewStatus getStatus() {
        return status;
    }

    public void setStatus(InterviewStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Interview)) return false;

        Interview interview = (Interview) o;

        if (id != null ? !id.equals(interview.id) : interview.id != null) return false;
        if (!dataInterview.equals(interview.dataInterview)) return false;
        if (grade != null ? !grade.equals(interview.grade) : interview.grade != null) return false;
        if (!employer.equals(interview.employer)) return false;
        return studentsCourses.equals(interview.studentsCourses);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + dataInterview.hashCode();
        result = 31 * result + (grade != null ? grade.hashCode() : 0);
        result = 31 * result + employer.hashCode();
        result = 31 * result + studentsCourses.hashCode();
        return result;
    }
}
