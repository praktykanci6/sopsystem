package org.pwsz.sops.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by dawid-ss on 18.10.2016.
 */
@Entity
@Table(name = "statusy")
public class Status {
    @Id
    @Column(name = "id_statusu")
    private short id;
    @Column(name = "status",length = 25)
    private String status;
//    @OneToMany(mappedBy = "status")
//    private Set<StatusPractices>statusPractices;

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Status status1 = (Status) o;

        if (id != status1.id) return false;
        return status.equals(status1.status);

    }

    @Override
    public int hashCode() {
        int result = (int) id;
        result = 31 * result + status.hashCode();
        return result;
    }
}
