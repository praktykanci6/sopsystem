package org.pwsz.sops.entities;


import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dawidss on 25.08.2016.
 */
@Entity
@Table(name = "kierunki_studiow")
public class CoursesOfStudy implements Serializable{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_kierunku")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_stopnia_studiow")
    private StudiesDegree studiesDegree;
    @ManyToOne
    @JoinColumn(name="id_trybu_studiow")
    private StudyMode studyMode;
    @ManyToOne
    @JoinColumn(name = "id_tytulu_zawodowego" )
    private Degree degree;
    @ManyToOne
    @JoinColumn(name = "id_liczby_semestrow")
    private NumberOfSemesters numberOfSemesters;
    @RestResource(exported = true)
    @ManyToOne
    @JoinColumn(name = "id_profilu_ksztalcenia")
    private EducationalProfile profile;
    @Column(name = "specjalnosc")
    private String specilaization;
    @Column(name = "nazwa_kierunku")
    private String nameCourses;
    @Column(name="polon")
    private String polon;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StudiesDegree getStudiesDegree() {
        return studiesDegree;
    }

    public void setStudiesDegree(StudiesDegree studiesDegree) {
        this.studiesDegree = studiesDegree;
    }

    public StudyMode getStudyMode() {
        return studyMode;
    }

    public void setStudyMode(StudyMode studyMode) {
        this.studyMode = studyMode;
    }

    public Degree getDegree() {
        return degree;
    }

    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    public NumberOfSemesters getNumberOfSemesters() {
        return numberOfSemesters;
    }

    public void setNumberOfSemesters(NumberOfSemesters numberOfSemesters) {
        this.numberOfSemesters = numberOfSemesters;
    }

    public EducationalProfile getProfile() {
        return profile;
    }

    public void setProfile(EducationalProfile profile) {
        this.profile = profile;
    }

    public String getSpecilaization() {
        return specilaization;
    }

    public void setSpecilaization(String specilaization) {
        this.specilaization = specilaization;
    }

    public String getNameCourses() {
        return nameCourses;
    }

    public void setNameCourses(String nameCourses) {
        this.nameCourses = nameCourses;
    }

    public String getPolon() {
        return polon;
    }

    public void setPolon(String polon) {
        this.polon = polon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CoursesOfStudy that = (CoursesOfStudy) o;

        if (!id.equals(that.id)) return false;
        if (!studiesDegree.equals(that.studiesDegree)) return false;
        if (!studyMode.equals(that.studyMode)) return false;
        if (degree != null ? !degree.equals(that.degree) : that.degree != null) return false;
        if (!numberOfSemesters.equals(that.numberOfSemesters)) return false;
        if (profile != null ? !profile.equals(that.profile) : that.profile != null) return false;
        if (specilaization != null ? !specilaization.equals(that.specilaization) : that.specilaization != null)
            return false;
        if (!nameCourses.equals(that.nameCourses)) return false;
        return polon != null ? polon.equals(that.polon) : that.polon == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + studiesDegree.hashCode();
        result = 31 * result + studyMode.hashCode();
        result = 31 * result + (degree != null ? degree.hashCode() : 0);
        result = 31 * result + numberOfSemesters.hashCode();
        result = 31 * result + (profile != null ? profile.hashCode() : 0);
        result = 31 * result + (specilaization != null ? specilaization.hashCode() : 0);
        result = 31 * result + nameCourses.hashCode();
        result = 31 * result + (polon != null ? polon.hashCode() : 0);
        return result;
    }
}
