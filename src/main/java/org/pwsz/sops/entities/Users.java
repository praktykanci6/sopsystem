package org.pwsz.sops.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dawidss on 04.10.2016.
 */
@Entity
@Table(name="uzytkownicy")
public class Users implements UserDetails {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_uzytkownika")
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "id_osoby")
    private Person person;
    @ManyToOne
    @JoinColumn(name = "id_roli")
    private Roles roles ;
    @Column(name = "login")
    private String login;
    @Column(name = "haslo")
    private String password;
    @Column(name="blokada")
    private boolean blockade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.blockade ? false : true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.blockade ? false : true;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isBlockade() {
        return blockade;
    }

    public void setBlockade(boolean blockade) {
        this.blockade = blockade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Users users = (Users) o;

        if (id != users.id) return false;
        if (blockade != users.blockade) return false;
        if (!person.equals(users.person)) return false;
        if (roles != null ? !roles.equals(users.roles) : users.roles != null) return false;
        if (!login.equals(users.login)) return false;
        return password.equals(users.password);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + person.hashCode();
        result = 31 * result + roles.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + (blockade ? 1 : 0);
        return result;
    }
}
