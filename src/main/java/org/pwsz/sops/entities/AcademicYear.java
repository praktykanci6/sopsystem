package org.pwsz.sops.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by dawidss on 04.10.2016.
 */
@Entity
@Table(name = "lata_akademickie")
public class AcademicYear {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_roku_akademickiego")
    private Integer id;
    @Column(name = "rok_akademicki")
    @NotNull
    @Size(max = 10)
    private String name;
    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "data_rozpoczecia")
    private Date start;
    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "data_zakonczenia")
    private Date end;
    @JsonIgnore
    @OneToMany(mappedBy = "academicYear")
    private List<PracticesCourses> practicesCoursesSet;

    public List<PracticesCourses> getPracticesCoursesSet() {
        return practicesCoursesSet;
    }

    public void setPracticesCoursesSet(List<PracticesCourses> practicesCoursesSet) {
        this.practicesCoursesSet = practicesCoursesSet;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AcademicYear that = (AcademicYear) o;

        if (!id.equals(that.id)) return false;
        if (!name.equals(that.name)) return false;
        if (!start.equals(that.start)) return false;
        return end.equals(that.end);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + start.hashCode();
        result = 31 * result + end.hashCode();
        return result;
    }
}
