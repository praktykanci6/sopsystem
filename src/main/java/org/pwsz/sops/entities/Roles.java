package org.pwsz.sops.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dawidss on 04.10.2016.
 */
@Entity
@Table(name="role")
public class Roles implements Serializable {
    @Id
    @Column(name = "id_roli")
    private Integer id;
    @Column(name="rola")
    private String role ;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Roles)) return false;

        Roles roles = (Roles) o;

        if (!id.equals(roles.id)) return false;
        return role.equals(roles.role);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + role.hashCode();
        return result;
    }
}
