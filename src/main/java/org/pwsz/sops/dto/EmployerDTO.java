package org.pwsz.sops.dto;

import org.pwsz.sops.entities.Employer;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Konrad on 15.11.2016.
 */
public class EmployerDTO implements Serializable{
    private String id;
    private String name;
    private String fullName;
    private String description;
    private String trade;
    private List<AddressDto> employerAddresses;

    public EmployerDTO() {
    }

    public EmployerDTO(Employer employer) {
        this.name = employer.getName();
        this.fullName = employer.getFullName();
        this.description = employer.getDescription();
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AddressDto> getEmployerAddresses() {
        return employerAddresses;
    }

    public void setEmployerAddresses(List<AddressDto> employerAddresses) {
        this.employerAddresses = employerAddresses;
    }
}

