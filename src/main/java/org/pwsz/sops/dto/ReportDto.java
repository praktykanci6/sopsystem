package org.pwsz.sops.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dawid-ss on 2017-05-20.
 */
public class ReportDto implements Serializable {
    private EmployerDTO company;
    //miejsce odbywania praktyk
    private EmployerDTO company1;
    private UserDto employer;
    private UserDto assistant;
    private PracticeDto practice;
    private Boolean agreement = new Boolean(false);
    private Boolean agreement2 = new Boolean(false);
    private Boolean agreement3 = new Boolean(false);
    private String other;

    public EmployerDTO getCompany1() {
        return company1;
    }

    public void setCompany1(EmployerDTO company1) {
        this.company1 = company1;
    }

    public Boolean getAgreement() {
        return agreement;
    }

    public void setAgreement(Boolean agreement) {
        this.agreement = agreement;
    }

    public Boolean getAgreement2() {
        return agreement2;
    }

    public void setAgreement2(Boolean agreement2) {
        this.agreement2 = agreement2;
    }

    public Boolean getAgreement3() {
        return agreement3;
    }

    public void setAgreement3(Boolean agreement3) {
        this.agreement3 = agreement3;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public EmployerDTO getCompany() {
        return company;
    }

    public void setCompany(EmployerDTO company) {
        this.company = company;
    }

    public UserDto getEmployer() {
        return employer;
    }

    public void setEmployer(UserDto employer) {
        this.employer = employer;
    }

    public UserDto getAssistant() {
        return assistant;
    }

    public void setAssistant(UserDto assistant) {
        this.assistant = assistant;
    }

    public PracticeDto getPractice() {
        return practice;
    }

    public void setPractice(PracticeDto practice) {
        this.practice = practice;
    }
}
