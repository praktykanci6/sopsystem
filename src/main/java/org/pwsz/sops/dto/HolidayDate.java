package org.pwsz.sops.dto;

/**
 * Created by tifet on 2017-05-06.
 */
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author user
 */
public class HolidayDate implements Serializable {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date date;

    private String name;

    /**
     *Holiday data
     */
    public HolidayDate() {
    }

    /**
     *
     * @param date the date to set
     * @param name the name to set
     */
    public HolidayDate(Date date, String name) {
        this.date = date;
        this.name = name;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}

