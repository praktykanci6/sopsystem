package org.pwsz.sops.dto;


import java.io.Serializable;

/**
 * Created by tifet on 2017-03-18.
 */
public class CoursesOfStudyDto implements Serializable {
    String name;
    String polon;
    Short studiesDegreeId;
    Short studyModeId;
    Short degreeId;
    Integer numberofSemestersId;
    Long educationalProfileId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPolon() {
        return polon;
    }

    public void setPolon(String polon) {
        this.polon = polon;
    }

    public Short getStudiesDegreeId() {
        return studiesDegreeId;
    }

    public void setStudiesDegreeId(Short studiesDegreeId) {
        this.studiesDegreeId = studiesDegreeId;
    }

    public Short getStudyModeId() {
        return studyModeId;
    }

    public void setStudyModeId(Short studyModeId) {
        this.studyModeId = studyModeId;
    }

    public Short getDegreeId() {
        return degreeId;
    }

    public void setDegreeId(Short degreeId) {
        this.degreeId = degreeId;
    }

    public Integer getNumberofSemestersId() {
        return numberofSemestersId;
    }

    public void setNumberofSemestersId(Integer numberofSemestersId) {
        this.numberofSemestersId = numberofSemestersId;
    }

    public Long getEducationalProfileId() {
        return educationalProfileId;
    }

    public void setEducationalProfileId(Long educationalProfileId) {
        this.educationalProfileId = educationalProfileId;
    }
}
