package org.pwsz.sops.dto;

import org.pwsz.sops.entities.AcademicYear;
import org.pwsz.sops.entities.CoursesOfStudy;
import org.pwsz.sops.entities.Yearbook;

import java.util.Date;

/**
 * Created by tifet on 2017-05-02.
 */
public class YearbookDto {

    private Long id;
    private Integer academicYearId;
    private Long courseOfStudyId;
    private String coursesOfStudeyName;
    private String coursesOfStudtSepcilaization;
    private Date startInterviews;
    private Date endInterviews;
    private Date startChoice;
    private Date endChoice;
    private Date startPractice;
    private Date endPractice;
    private Short hour;
    private Short day;

    public Short getHour() {
        return hour;
    }

    public void setHour(Short hour) {
        this.hour = hour;
    }

    public Short getDay() {
        return day;
    }

    public void setDay(Short day) {
        this.day = day;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAcademicYearId() {
        return academicYearId;
    }

    public void setAcademicYearId(Integer academicYearId) {
        this.academicYearId = academicYearId;
    }

    public Long getCourseOfStudyId() {
        return courseOfStudyId;
    }

    public void setCourseOfStudyId(Long courseOfStudyId) {
        this.courseOfStudyId = courseOfStudyId;
    }

    public Date getStartInterviews() {
        return startInterviews;
    }

    public void setStartInterviews(Date startInterviews) {
        this.startInterviews = startInterviews;
    }

    public Date getEndInterviews() {
        return endInterviews;
    }

    public void setEndInterviews(Date endInterviews) {
        this.endInterviews = endInterviews;
    }

    public Date getStartChoice() {
        return startChoice;
    }

    public void setStartChoice(Date startChoice) {
        this.startChoice = startChoice;
    }

    public Date getEndChoice() {
        return endChoice;
    }

    public void setEndChoice(Date endChoice) {
        this.endChoice = endChoice;
    }

    public Date getStartPractice() {
        return startPractice;
    }

    public void setStartPractice(Date startPractice) {
        this.startPractice = startPractice;
    }

    public Date getEndPractice() {
        return endPractice;
    }

    public void setEndPractice(Date endPractice) {
        this.endPractice = endPractice;
    }

    public String getCoursesOfStudeyName() {
        return coursesOfStudeyName;
    }

    public void setCoursesOfStudeyName(String coursesOfStudeyName) {
        this.coursesOfStudeyName = coursesOfStudeyName;
    }

    public String getCoursesOfStudtSepcilaization() {
        return coursesOfStudtSepcilaization;
    }

    public void setCoursesOfStudtSepcilaization(String coursesOfStudtSepcilaization) {
        this.coursesOfStudtSepcilaization = coursesOfStudtSepcilaization;
    }

    public Yearbook getYearbook(){
        Yearbook yearbook = new Yearbook();
        yearbook.setId(this.getId());
        AcademicYear year = new AcademicYear();
        year.setId(this.academicYearId);
        yearbook.setAcademicYear(year);
        CoursesOfStudy coursesOfStudy = new CoursesOfStudy();
        coursesOfStudy.setId(this.courseOfStudyId);
        yearbook.setCourseOfStudy(coursesOfStudy);
        yearbook.setStartChoice(this.startChoice);
        yearbook.setEndChoice(this.endChoice);
        yearbook.setStartInterviews(this.startInterviews);
        yearbook.setEndInterviews(this.endInterviews);
        yearbook.setStartPractice(this.startPractice);
        yearbook.setEndPractice(this.endPractice);
        return yearbook;
    }
}
