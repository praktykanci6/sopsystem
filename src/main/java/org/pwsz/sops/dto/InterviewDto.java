package org.pwsz.sops.dto;

import org.pwsz.sops.entities.Dictionary.InterviewStatus;

import java.util.Date;

/**
 * Created by tifet on 2017-06-04.
 */
public class InterviewDto {
    Long id;
    UserDto student;
    Date interviewDate;
    Short points;
    String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDto getStudent() {
        return student;
    }

    public void setStudent(UserDto student) {
        this.student = student;
    }

    public Date getInterviewDate() {
        return interviewDate;
    }

    public void setInterviewDate(Date interviewDate) {
        this.interviewDate = interviewDate;
    }

    public Short getPoints() {
        return points;
    }

    public void setPoints(Short points) {
        this.points = points;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
