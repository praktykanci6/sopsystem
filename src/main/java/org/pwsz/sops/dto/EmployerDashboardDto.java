package org.pwsz.sops.dto;

import java.io.Serializable;
import java.time.Year;
import java.util.List;

/**
 * Created by tifet on 2017-05-04.
 */
public class EmployerDashboardDto implements Serializable {
    YearbookDto yearbook;
    Long employerId;
    Long assistentId;
    List<PracticeDto> practices;

    public YearbookDto getYearbook() {
        return yearbook;
    }

    public void setYearbook(YearbookDto yearbook) {
        this.yearbook = yearbook;
    }

    public Long getEmployerId() {
        return employerId;
    }

    public void setEmployerId(Long employerId) {
        this.employerId = employerId;
    }

    public Long getAssistentId() {
        return assistentId;
    }

    public void setAssistentId(Long assistentId) {
        this.assistentId = assistentId;
    }

    public List<PracticeDto> getPractices() {
        return practices;
    }

    public void setPractices(List<PracticeDto> practices) {
        this.practices = practices;
    }
}
