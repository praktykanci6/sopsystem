package org.pwsz.sops.dto;

import java.io.Serializable;

/**
 * Created by tifet on 2017-05-03.
 */
public class AssistantCourseDto implements Serializable{
    Long id;
    Long employerId;
    AssistantPracticeDto assistantPracticeDto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEmployerId() {
        return employerId;
    }

    public void setEmployerId(Long employerId) {
        this.employerId = employerId;
    }

    public AssistantPracticeDto getAssistantPracticeDto() {
        return assistantPracticeDto;
    }

    public void setAssistantPracticeDto(AssistantPracticeDto assistantPracticeDto) {
        this.assistantPracticeDto = assistantPracticeDto;
    }
}
