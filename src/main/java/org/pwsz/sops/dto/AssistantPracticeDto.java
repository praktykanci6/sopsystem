package org.pwsz.sops.dto;

import org.pwsz.sops.entities.Yearbook;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tifet on 2017-05-03.
 */
public class AssistantPracticeDto implements Serializable{
    Long id;
    UserDto userDto;
    List<YearbookDto>yearbookDtoList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public List<YearbookDto> getYearbookDtoList() {
        return yearbookDtoList;
    }

    public void setYearbookDtoList(List<YearbookDto> yearbookDtoList) {
        this.yearbookDtoList = yearbookDtoList;
    }
}
