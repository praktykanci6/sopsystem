package org.pwsz.sops.dto;

import java.io.Serializable;

/**
 * Created by tifet on 2017-05-02.
 */
public class SimpleEmployerDto implements Serializable {

    private Long id;
    private String name;
    private String fullName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
