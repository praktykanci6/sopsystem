package org.pwsz.sops.dto;

import org.pwsz.sops.entities.Users;

import java.io.Serializable;

/**
 * Created by tifet on 2017-04-25.
 */
public class UserDto implements Serializable{
    Integer id;
    String login;
    String  firstName;
    String lastName;
    String email;
    String phone;
    String phone2;
    String degree;
    String position;
    String password = "";

    public UserDto() {
    }

    public UserDto(Users user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.firstName = user.getPerson().getFirstName();
        this.lastName = user.getPerson().getLastName();
        this.email = user.getPerson().getEmail();
        this.phone = user.getPerson().getPhone();
        this.phone2 = user.getPerson().getMobilePhone();

    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public String getDegree() {
        return degree;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
