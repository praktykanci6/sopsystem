package org.pwsz.sops.dto;

import org.pwsz.sops.entities.EmployAddress;
import org.pwsz.sops.entities.Practice;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by tifet on 2017-05-05.
 */
public class PracticeDto implements Serializable {
    Long id;
    Date star;
    Date end;
    Boolean interview;
    Integer counter;
    Long addressId;
    Long yearbookId;
    Integer number;
    EmployAddress employAddress;
    BigDecimal hours;
    String employmentDimension;
    Long employerId;
    Short preference;
    EmployerDTO employer;

    public PracticeDto() {
    }

    public PracticeDto(Practice practice) {
        this.star = practice.getStartDate();
        this.end = practice.getEndDate();

    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStar() {
        return star;
    }

    public void setStar(Date star) {
        this.star = star;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Long getYearbookId() {
        return yearbookId;
    }

    public void setYearbookId(Long yearbookId) {
        this.yearbookId = yearbookId;
    }

    public Boolean getInterview() {
        return interview;
    }

    public void setInterview(Boolean interview) {
        this.interview = interview;
    }

    public EmployAddress getEmployAddress() {
        return employAddress;
    }

    public void setEmployAddress(EmployAddress employAddress) {
        this.employAddress = employAddress;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }

    public String getEmploymentDimension() {
        return employmentDimension;
    }

    public void setEmploymentDimension(String employmentDimension) {
        this.employmentDimension = employmentDimension;
    }

    public Long getEmployerId() {
        return employerId;
    }

    public void setEmployerId(Long employerId) {
        this.employerId = employerId;
    }

    public Short getPreference() {
        return preference;
    }

    public void setPreference(Short preference) {
        this.preference = preference;
    }

    public EmployerDTO getEmployer() {
        return employer;
    }

    public void setEmployer(EmployerDTO employer) {
        this.employer = employer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PracticeDto)) return false;

        PracticeDto that = (PracticeDto) o;

        if (!star.equals(that.star)) return false;
        if (!end.equals(that.end)) return false;
        if (interview != null ? !interview.equals(that.interview) : that.interview != null) return false;
        if (counter != null ? !counter.equals(that.counter) : that.counter != null) return false;
        if (!addressId.equals(that.addressId)) return false;
        return yearbookId.equals(that.yearbookId);
    }

    @Override
    public int hashCode() {
        int result = star.hashCode();
        result = 31 * result + end.hashCode();
        result = 31 * result + (interview != null ? interview.hashCode() : 0);
        result = 31 * result + (counter != null ? counter.hashCode() : 0);
        result = 31 * result + addressId.hashCode();
        result = 31 * result + yearbookId.hashCode();
        return result;
    }
}
