package org.pwsz.sops.dto;

/**
 * Created by tifet on 2017-06-03.
 */
public class Notification {
    String notification;

    public Notification() {
    }

    public Notification(String notification) {
        this.notification = notification;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }
}
