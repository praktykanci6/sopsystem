package org.pwsz.sops.dto;

import org.pwsz.sops.entities.Users;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Konrad on 10.11.2016.
 */
public class UserAuthenticationDTO implements Serializable{

    private String role;
    private String login;
    private String firstName;
    private String lastName;
    private String email;

    public UserAuthenticationDTO(Users user) {
        if(user != null){
            this.login = user.getLogin();
            this.firstName = user.getPerson().getFirstName();
            this.lastName = user.getPerson().getLastName();
            this.email = user.getPerson().getEmail();
            this.role = user.getRoles().getRole();
        }
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
