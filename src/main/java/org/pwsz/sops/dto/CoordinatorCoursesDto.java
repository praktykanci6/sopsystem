package org.pwsz.sops.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tifet on 2017-05-02.
 */
public class CoordinatorCoursesDto implements Serializable {
    private Long coordinatorPracticeId;
    private UserDto user;
    private Long yearbookId;
    private List<Long> employerIds;

    public Long getCoordinatorPracticeId() {
        return coordinatorPracticeId;
    }

    public void setCoordinatorPracticeId(Long coordinatorPracticeId) {
        this.coordinatorPracticeId = coordinatorPracticeId;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public List<Long> getEmployerIds() {
        return employerIds;
    }

    public void setEmployerIds(List<Long> employerIds) {
        this.employerIds = employerIds;
    }

    public Long getYearbookId() {
        return yearbookId;
    }

    public void setYearbookId(Long yearbookId) {
        this.yearbookId = yearbookId;
    }
}
