package org.pwsz.sops.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by tifet on 2017-05-03.
 */
public class AcademicYearDto implements Serializable {

    private Integer id;
    private String name;
    private Date start;
    private Date end;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
