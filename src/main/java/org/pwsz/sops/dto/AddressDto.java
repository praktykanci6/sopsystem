package org.pwsz.sops.dto;

import java.io.Serializable;

/**
 * Created by tifet on 2017-03-25.
 */
public class AddressDto implements Serializable {
    String street;
    String city;
    String country;
    String postCode;
    String phone1;
    String phone2;
    String fax;
    String numberBuilding;
    boolean departament;

    public boolean isDepartament() {
        return departament;
    }

    public void setDepartament(boolean departament) {
        this.departament = departament;
    }

    public String getNumberBuilding() {
        return numberBuilding;
    }

    public void setNumberBuilding(String numberBuilding) {
        this.numberBuilding = numberBuilding;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
