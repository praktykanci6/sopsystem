/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pwsz.sops.repositories;

import java.util.List;
import org.pwsz.sops.entities.Report;
import org.pwsz.sops.repositories.projection.ReportProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author $k.kabat
 */
@RepositoryRestResource(path = "/reports", excerptProjection = ReportProjection.class)
public interface ReportRepository extends PagingAndSortingRepository<Report, Short> {

    @Override
    @Query("SELECT r FROM Report r")
    public List<Report> findAll();

}
