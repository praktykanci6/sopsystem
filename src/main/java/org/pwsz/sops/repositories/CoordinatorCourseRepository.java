package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.CoordinatorCourses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by tifet on 2017-05-04.
 */
@RepositoryRestResource(path = "/coordinatorCourse")
public interface CoordinatorCourseRepository extends JpaRepository<CoordinatorCourses,Long>{
    List<CoordinatorCourses> findByYearbookId(@Param("id")Long id);
    List<CoordinatorCourses> findByCoordinatorsPracticeId(@Param("id")Long id);
}
