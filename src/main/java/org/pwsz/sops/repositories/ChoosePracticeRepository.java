package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.ChoosePractices;
import org.pwsz.sops.entities.Practice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by tifet on 2017-06-04.
 */
@RepositoryRestResource
public interface ChoosePracticeRepository extends CrudRepository<ChoosePractices,Long>{

    @Query("SELECT p.practice FROM ChoosePractices p WHERE p.studentsCours.id =:id")
    List<Practice> getPracticeByStAndStudentsCoursId(@Param("id")Long studentCourseId);
}
