package org.pwsz.sops.repositories;

import jdk.nashorn.internal.runtime.Specialization;
import org.pwsz.sops.entities.Specilaization;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by dawid-ss on 2017-03-11.
 */

@RepositoryRestResource(path = "/specilization")
public interface SpecilizationRepository extends CrudRepository<Specilaization,Long> {

}
