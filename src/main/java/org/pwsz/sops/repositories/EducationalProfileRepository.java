package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.EducationalProfile;
import org.pwsz.sops.repositories.projection.EducationalProfileProjection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


/**
 * Created by tifet on 2017-03-16.
 */
@RepositoryRestResource(excerptProjection = EducationalProfileProjection.class, path = "/educationalProfile")
public interface EducationalProfileRepository extends CrudRepository<EducationalProfile,Long>{

}
