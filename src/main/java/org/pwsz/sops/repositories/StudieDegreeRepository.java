package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.StudiesDegree;
import org.pwsz.sops.repositories.projection.CoursOfStudyProjection;
import org.pwsz.sops.repositories.projection.StudiesDegreeProjection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by dawid-ss on 2017-03-11.
 */

@RepositoryRestResource(excerptProjection = StudiesDegreeProjection.class ,path = "/studiesDegree")
public interface StudieDegreeRepository extends CrudRepository<StudiesDegree, Short>{

}
