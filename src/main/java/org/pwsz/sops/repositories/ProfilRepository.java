package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Profil;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by tifet on 2017-03-11.
 */
@RepositoryRestResource(path = "/profile")
public interface ProfilRepository extends CrudRepository<Profil,Integer>{
}
