package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Address;
import org.pwsz.sops.entities.EmployAddress;
import org.pwsz.sops.repositories.projection.EmployerAddresses;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by tifet on 2017-03-25.
 */
@RepositoryRestResource(path = "/employerAddresses",excerptProjection = EmployerAddresses.class)
public interface EmployAddressRepository extends CrudRepository<EmployAddress,Integer>{

    List<EmployAddress> getByEmployerId(@Param("id")Long id);
    @Query("SELECT e.address FROM EmployAddress e WHERE e.employer.id = :id")
    List<Address> getAddressByEmployerId(@Param("id")Long employerId);
    EmployAddress getByAddressId(@Param("id")Long id);

}
