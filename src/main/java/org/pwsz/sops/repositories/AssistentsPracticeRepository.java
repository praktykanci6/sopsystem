package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.AssistentPractices;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Set;

/**
 * Created by tifet on 2017-05-03.
 */
@RepositoryRestResource
public interface AssistentsPracticeRepository  extends CrudRepository<AssistentPractices,Long>{
    @Query("SELECT a FROM AssistentPractices a JOIN FETCH a.assistenCourses ac JOIN FETCH ac.employer e WHERE e.id =:id")
    Set<AssistentPractices> findByEmployerId(@Param("id") Long id );
    AssistentPractices findByPersonId(@Param("id")Long id);
}
