package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.AssistenCourses;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by tifet on 2017-04-11.
 */
@RepositoryRestResource(path = "/assistentCourse")
public interface AsistentCoursesRepository extends CrudRepository<AssistenCourses,Long>{
    List<AssistenCourses> findByEmployerId(@Param("id") Long id);
    List<AssistenCourses> findByAssistentPracticesId(@Param("id")Long id);
    @Query("SELECT ac FROM AssistenCourses ac JOIN FETCH ac.assistentPractices ap WHERE ac.yearbook.id =:yearbookId AND ap.person.id =:personeId ")
    AssistenCourses findByPersoneId(@Param("personeId")Long id,@Param("yearbookId")Long yearbookId);
    @Query("SELECT ac FROM AssistenCourses ac JOIN FETCH ac.assistentPractices ap JOIN FETCH  ac.yearbook y WHERE   y.academicYear.id =:academicYearId AND ap.person.id =:personeId ")
    List<AssistenCourses> findByPersoneIdAndAcademicYear(@Param("personeId")Long id,@Param("academicYearId")Integer academicYearId);
}
