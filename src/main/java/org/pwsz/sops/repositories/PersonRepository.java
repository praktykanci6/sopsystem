package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by tifet on 2017-04-23.
 */
@RepositoryRestResource(path = "/persone")
public interface PersonRepository extends CrudRepository<Person,Long>{
}
