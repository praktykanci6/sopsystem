package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Students;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by tifet on 2017-04-23.
 */
@RepositoryRestResource
public interface StudentRepository extends CrudRepository<Students,String>{
    Students findByNumberAlbumEquals(String numberAlbumu);
    Students findByPersonId(Long personId);
}
