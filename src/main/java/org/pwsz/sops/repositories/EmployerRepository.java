package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Employer;
import org.pwsz.sops.repositories.projection.EmployerProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Set;

/**
 * Created by k.kabat on 02.11.2016.
 */
@RepositoryRestResource(path = "/employer",excerptProjection = EmployerProjection.class)
//@PreAuthorize("hasRole('STUDENT')")
public interface EmployerRepository extends PagingAndSortingRepository<Employer,Long> {

    @Override
    @Query("SELECT e FROM Employer e")
    List<Employer> findAll();
    Page<Employer> findByNameIsContainingOrderByNameDesc(@Param("name")String name, Pageable pageable);
    @Query("Select e from Employer e JOIN FETCH e.addresses a JOIN FETCH a.address ad where lower(ad.city) like '%'||:fragments||'%' " +
            "Or lower(e.name) like '%'||:fragments||'%' Or lower(e.fullName) like '%'||:fragments||'%'" +
            "Or lower(e.description) like '%'||:fragments||'%'")
    Set<Employer> fullTsxtSearch(@Param("fragments") String framents);

    @Query("SELECT e FROM Employer e JOIN FETCH e.assistenCourses a WHERE a.yearbook.id =:id")
    Set<Employer> findByYearbooId(@Param("id") Long id);

}
