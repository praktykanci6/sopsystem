package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Address;
import org.pwsz.sops.repositories.projection.AddressProjection;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by tifet on 2017-03-25.
 */
@RepositoryRestResource(excerptProjection = AddressProjection.class)
public interface AddressRepository  extends CrudRepository<Address, Long> {

    public Address findByStreetAndNumberBuildingAndCityAndCountryAndPostCodeAndPhone1AndPhone2AndFax(String street,String numberBuilding,
                                                                                                     String city,String country,String postCode,
                                                                                                     String phone1,String phone2, String fax);
}
