package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.CoordinatorCoursesEmployer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by tifet on 2017-05-04.
 */
@RepositoryRestResource
public interface CoordinatorCoursesEmployerRepository extends JpaRepository<CoordinatorCoursesEmployer,Long>{
    @Query("SELECT e FROM CoordinatorCoursesEmployer e JOIN FETCH e.coordinatorCourses c WHERE e.employer.id = :employerId AND c.yearbook.id = :yearbookId")
    CoordinatorCoursesEmployer findCoordinatorCoursesEmployerbyEemployerIdAndYearbookId(@Param("employerId")Long employerId,@Param("yearbookId") Long yearbookId);
}
