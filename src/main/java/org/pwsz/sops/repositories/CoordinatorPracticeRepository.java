package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.CoordinatorsPractice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by tifet on 2017-05-04.
 */
@RepositoryRestResource
public interface CoordinatorPracticeRepository extends JpaRepository<CoordinatorsPractice,Long>{
    CoordinatorsPractice findByPersonId(@Param("id") Long id);
}
