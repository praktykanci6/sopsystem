package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.CoursesOfStudy;
import org.pwsz.sops.repositories.projection.CoursOfStudyProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by dawid-ss on 07.03.2017.
 */
@RepositoryRestResource(excerptProjection = CoursOfStudyProjection.class , path = "coursesOfStudy")
public interface CourseOfStudyRepository extends PagingAndSortingRepository<CoursesOfStudy,Long>{
    @Override
    Page<CoursesOfStudy> findAll(Pageable pageable);


}
