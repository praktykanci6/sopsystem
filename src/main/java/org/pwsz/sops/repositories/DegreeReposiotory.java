package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Degree;
import org.pwsz.sops.repositories.projection.DegreeProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by dawid-ss on 2017-03-11.
 */

@RepositoryRestResource(excerptProjection = DegreeProjection.class,path = "/degree")
public interface DegreeReposiotory extends CrudRepository<Degree,Short> {
    @Query("select d from Degree d where d.archiwes = false")
    public List<Degree> findActive();
}
