package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.PracticesCourses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by tifet on 2017-05-04.
 */
@RepositoryRestResource
public interface PracticesCoursesRepository extends JpaRepository<PracticesCourses,Long>{
    @Query("SELECT p FROM PracticesCourses p WHERE p.academicYear.id = :academicYearId " +
            "AND p.yearbook.id =:yearbookId")
    PracticesCourses findByAcademicYearIdAnAndYearbookId(@Param("academicYearId")Integer academicYearId,@Param("yearbookId")Long yearbookId);

    @Query("SELECT p.id FROM PracticesCourses p WHERE p.academicYear.id =:academicYearId")
    List<Long>findByAcademicYearId(@Param("academicYearId") Integer academicYearId);
}
