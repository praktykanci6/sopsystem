package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.NumberOfSemesters;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by tifet on 2017-03-18.
 */
@Projection(name = "numberOfSemesterInline" ,types = {NumberOfSemesters.class})
public interface NumberOfSemestrProjection {
    int getId();
    int getNumberSemesters();
}
