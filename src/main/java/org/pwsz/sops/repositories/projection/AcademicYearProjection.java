package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.AcademicYear;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

/**
 * Created by tifet on 2017-04-09.
 */
@Projection(name = "AcademicYearProjection",types = AcademicYear.class)
public interface AcademicYearProjection {
    Integer getId();
    String getName();
    Date getStart();
    Date getEnd();

}
