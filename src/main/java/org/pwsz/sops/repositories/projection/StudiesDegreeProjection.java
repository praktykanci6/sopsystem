package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.StudiesDegree;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by tifet on 2017-03-18.
 */
@Projection(name = "studiesDegreeProjection", types = {StudiesDegree.class})
public interface StudiesDegreeProjection {
    Short getId();
    String getDegreeStudies();
}
