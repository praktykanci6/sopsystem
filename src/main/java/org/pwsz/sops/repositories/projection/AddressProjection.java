package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.Address;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by tifet on 2017-05-09.
 */
@Projection(types = Address.class,name = "fullAddress")
public interface AddressProjection {

    public Long getId();
    public String getStreet();
    public String getNumberBuilding();
    public String getCity();
    public String getCountry();
    public String getPhone1();
    public String getPhone2();
    public String getFax();
    public String getPostCode();
}
