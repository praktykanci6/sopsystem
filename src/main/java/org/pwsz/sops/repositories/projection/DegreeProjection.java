package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.Degree;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by tifet on 2017-03-18.
 */
@Projection(name = "degree",types = {Degree.class})
public interface DegreeProjection {
   Short getId();
    String getDegreeTitle();
}

