/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.Report;
import org.springframework.data.rest.core.config.Projection;

/**
 *
 * @author $k.kabat
 */
@Projection(name = "reportProjection", types = {Report.class})
public interface ReportProjection {    
    Short getId();
    String getDescription();
}
