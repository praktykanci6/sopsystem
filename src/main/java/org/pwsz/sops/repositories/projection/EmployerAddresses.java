package org.pwsz.sops.repositories.projection;

import com.mysema.query.annotations.QueryProjection;
import org.pwsz.sops.entities.Address;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

/**
 * Created by tifet on 2017-04-07.
 */
@Projection(name = "employerAddresses",types = EmployerAddresses.class)
public interface EmployerAddresses  {
    Address getAddress();
}
