package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.Address;
import org.pwsz.sops.entities.EmployAddress;
import org.pwsz.sops.entities.Employer;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by tifet on 2017-03-26.
 */
@Projection(name = "employerProjection",types = Employer.class)
public interface EmployerProjection {
    Long getId();
    String getName();
    String getFullName();
    String getDescription();
}
