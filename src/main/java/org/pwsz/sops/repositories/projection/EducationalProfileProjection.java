package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.EducationalProfile;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by tifet on 2017-03-18.
 */
@Projection(name = "educationalProfileProjection", types = {EducationalProfile.class})
public interface EducationalProfileProjection {
    Long getId();
    String getEducationProfile();
}
