package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.StudyMode;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by tifet on 2017-03-18.
 */
@Projection(name = "studyModeProjection", types = {StudyMode.class})
public interface StudyModeProjection {
    Short getId();
    String getModeOfStudy();
}
