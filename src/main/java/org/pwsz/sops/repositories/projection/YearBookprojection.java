package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.AcademicYear;
import org.pwsz.sops.entities.CoursesOfStudy;
import org.pwsz.sops.entities.Yearbook;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

/**
 * Created by dawid-ss on 2017-04-09.
 */
@Projection(name = "YerbookWithCourses" ,types = Yearbook.class)
public interface YearBookprojection {
    Long getId();
    AcademicYear getAcademicYear();
    CoursesOfStudy getCourseOfStudy();
    Date getStartInterviews();
    Date getEndInterviews();
    Date  getStartChoice();
    Date getEndChoice();
    Date getStartPractice();
    Date getEndPractice();
}
