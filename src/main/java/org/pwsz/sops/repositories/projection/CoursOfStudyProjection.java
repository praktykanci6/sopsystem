package org.pwsz.sops.repositories.projection;

import org.pwsz.sops.entities.*;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by tifet on 2017-03-14.
 */
@Projection(name = "ccursOfStudyProjection",types = {CoursesOfStudy.class})
public interface CoursOfStudyProjection {
    int getId();
    StudiesDegree getStudiesDegree();
    StudyMode getStudyMode();
    Degree getDegree();
    NumberOfSemesters getNumberOfSemesters();
    EducationalProfile getProfile();
    String getSpecilaization();
    String getNameCourses();
    String getPolon();
}
