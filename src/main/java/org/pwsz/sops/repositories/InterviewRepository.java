package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Dictionary.InterviewStatus;
import org.pwsz.sops.entities.Employer;
import org.pwsz.sops.entities.Interview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by tifet on 2017-05-29.
 */
public interface InterviewRepository extends JpaRepository<Interview,Long> {

    List<Interview>findByStudentsCoursesId(Long id);
    @Query("SELECT i.employer FROM Interview i WHERE i.studentsCourses.id = :id AND i.status = :status")
    List<Employer> getEmployerByStudentsCoursesId(@Param("id")Long id, @Param("status")InterviewStatus status);
    @Query("SELECT i FROM Interview i WHERE i.studentsCourses.yearbook.id = :yearbookId AND i.employer.id = :employerId")
    List<Interview> getByEmployerIdAndYearbookId(@Param("employerId")Long employerId,@Param("yearbookId")Long yearbookId);
}
