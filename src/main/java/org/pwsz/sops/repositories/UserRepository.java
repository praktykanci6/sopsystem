package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Users;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by Konrad on 14.10.2016.
 */
@Repository
@Transactional
public interface UserRepository extends JpaRepository<Users,Long> {

    @Query("SELECT u FROM Users u WHERE u.login = :id")
    public Users findOneByLogin(@Param("id") String login);

    public Users findById(Integer id);
}
