package org.pwsz.sops.repositories;

import org.pwsz.sops.dto.AcademicYearDto;
import org.pwsz.sops.entities.AcademicYear;
import org.pwsz.sops.repositories.projection.AcademicYearProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Date;

/**
 * Created by tifet on 2017-04-08.
 */
@RepositoryRestResource(path = "/academicYear",excerptProjection = AcademicYearProjection.class)
public interface AcademicYearRepository  extends CrudRepository<AcademicYear,Integer>{
    @Query("SELECT a FROM AcademicYear a WHERE a.start <= CURRENT_DATE  AND a.end >= CURRENT_DATE")
    AcademicYear findActualAcademicYear();
}
