package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.StudyMode;
import org.pwsz.sops.repositories.projection.StudyModeProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by tifet on 2017-03-11.
 */
@RepositoryRestResource(excerptProjection = StudyModeProjection.class ,path = "/studyMode")
public interface StudyModeRepository extends CrudRepository<StudyMode,Short> {

}
