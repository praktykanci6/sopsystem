package org.pwsz.sops.repositories;//package org.pwsz.sops.repositories;
//
import org.pwsz.sops.entities.Dictionary.PracticeType;
import org.pwsz.sops.entities.Employer;
import org.pwsz.sops.entities.Practice;
import org.pwsz.sops.entities.PracticesType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.*;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Konrad on 02.11.2016.
 */
@Repository
public interface PracticeRepository extends JpaRepository<Practice,Long> {

    @Query("SELECT p FROM Practice p WHERE p.employer.id =:employerId  And p.practicesCourses.id IN :ids")
    List<Practice> findByEmployerIdAndYea(@Param("employerId")Long employerId, @Param("ids") List<Long>practiceCoursesId);
    Set<Practice>findByAssistenCoursesId(@Param("id")Long id);
    Integer countPracticeByStartDateAndEndDateAndEmployerId(@Param("start")Date start, @Param("end")Date end, @Param("id")Long id);
    @Query("SELECT DISTINCT p.employer FROM Practice p JOIN  p.practicesCourses pc WHERE pc.yearbook.id = :id AND p.practiceType = :status")
    List<Employer> getEmployerByYerbokId(@Param("id")Long id, @Param("status")PracticeType status);
    @Query("SELECT DISTINCT p.employer FROM Practice p JOIN  p.practicesCourses pc WHERE pc.yearbook.id = :id AND p.practiceType = :typePractice AND p.interwiev = :interview")
    List<Employer> getEmployerToStudent(@Param("id")Long id, @Param("interview")boolean interview,@Param("typePractice") PracticeType type );
    List<Practice> findByStudentsCoursesId(Long id);
    List<Practice> findByStartDateAndEndDateAndEmployerIdAndStudentsCoursesIsNull(Date start, Date end, Long employerId);
}
