package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Yearbook;
import org.pwsz.sops.repositories.projection.YearBookprojection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.time.Year;
import java.util.List;

/**
 * Created by tifet on 2017-04-09.
 */
@RepositoryRestResource(path = "yearbooks",excerptProjection = YearBookprojection.class)
public interface YearbookRepository extends CrudRepository<Yearbook,Long> {


    List<Yearbook> findByAcademicYearId(@Param("id") Integer id);
}
