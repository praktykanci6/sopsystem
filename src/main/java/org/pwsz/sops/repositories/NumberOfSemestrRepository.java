package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.NumberOfSemesters;
import org.pwsz.sops.repositories.projection.NumberOfSemestrProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by tifet on 2017-03-11.
 */
@RepositoryRestResource(excerptProjection = NumberOfSemestrProjection.class ,path = "/numberOfSemester")
public interface NumberOfSemestrRepository extends CrudRepository<NumberOfSemesters,Integer>{
    @Query("SELECT nfs FROM NumberOfSemesters nfs WHERE nfs.archives = false")
    public List<NumberOfSemesters> findActive();
}
