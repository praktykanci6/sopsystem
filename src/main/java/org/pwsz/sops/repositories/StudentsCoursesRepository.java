package org.pwsz.sops.repositories;

import org.pwsz.sops.entities.Students;
import org.pwsz.sops.entities.StudentsCourses;
import org.pwsz.sops.entities.Yearbook;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by tifet on 2017-04-23.
 */
@RepositoryRestResource(path = "/studenciKierunku")
public interface StudentsCoursesRepository extends CrudRepository<StudentsCourses,Long> {

    StudentsCourses findByStudentsAndYearbook(Students students , Yearbook yearbook);
    @Query("SELECT s.students FROM StudentsCourses s WHERE s.yearbook.id = :id")
    List<Students> findByYearbookId(@Param("id") Long id);
    @Query("SELECT ac FROM StudentsCourses ac JOIN FETCH ac.yearbook y WHERE ac.students.numberAlbum =:numberAlbum" +
            " AND  y.academicYear.id =:academicYearId")
    List<StudentsCourses> getByAcademicYearIdAndStudentId(@Param("academicYearId") Integer academicYearId,@Param("numberAlbum") String numberAlbum);
    StudentsCourses findByYearbookIdAndStudentsNumberAlbum(@Param("yearbookId") Long yearbookId,@Param("studentId")String  studentId);
    StudentsCourses findByYearbookIdAndId(Long yearbookId,Long id);
}
