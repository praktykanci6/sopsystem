package org.pwsz.sops.egeriaRepository;

import org.pwsz.sops.egeriaEntities.EegeriaData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by tifet on 2017-04-23.
 */
@RepositoryRestResource(path = "/osoba")
public interface EgeriaDataRepository extends CrudRepository<EegeriaData,Long>{
}
