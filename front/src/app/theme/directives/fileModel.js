/**
 * Created by dawid-ss on 2017-04-15.
 */
// var app = angular.module('BlurAdmin.theme', []);
//
// app.directive('fileModel', [ '$parse', function($parse) {
//     return {
//         restrict : 'A',
//         link : function(scope, element, attrs) {
//             var model = $parse(attrs.fileModel);
//             var modelSetter = model.assign;
//
//             element.bind('change', function() {
//                 scope.$apply(function() {
//                     modelSetter(scope, element[0].files[0]);
//                 });
//             });
//         }
//     };
// } ]);
// (function () {
//     'use strict';
//
//     angular.module('BlurAdmin.theme')
//         .directive('fileModel', fileModel);
//
//     /** @ngInject */
//     function fileModel($parse) {
//         return {
//             link: function (scope, element, attrs,ngModel) {
//                 var model = $parse(attrs.fileModel);
//                 var modelSetter = model.assign;
//
//                element.bind('change',function () {
//                    scope.$apply(function () {
//                        modelSetter(scope,element[0].files[0]);
//                        // ngModel = element[0].files[0];
//                        console.log('file model directive',element[0].files[0]);
//                        console.log('ngModel ',model);
//                    })
//                })
//             }
//         }
//     }
// })();