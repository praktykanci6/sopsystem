/**
 * Created by dawid-ss on 2017-03-14.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme')
        .service('StudentService', StudentService);

    /** @ngInject */
    function StudentService($http) {
        var students = {};
        var getStudents = function (id) {
            $http.get('/students/search/getById?id='+id)
                .success(function (data) {
                    return data;
                })
                .error(function (message) {
                    return message;
                })
        }
        return {
            getstudents : getStudents
        }
    }
})();