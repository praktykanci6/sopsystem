/**
 * Created by dawid-ss on 2017-04-09.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme')
        .service('AcademicYearService', AcademicYearService);
    /** @ngInject */
    function AcademicYearService($http) {
       var getAllYears = function () {
            $http.get('/academicYear')
                .success(function (data) {
                    console.log(data);
                    return data._embedded.academicYears;
                })
                .error(function (data) {
                    return data;
                });
        };
        return {
            getAllYears : getAllYears
        }
    }
})();
