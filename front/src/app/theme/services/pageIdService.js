/**
 * Created by dawid-ss on 2017-03-14.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme')
        .service('pageIdService', pageIdServiceProvider);

    /** @ngInject */
    function pageIdServiceProvider($window) {

        var KEY = 'App.Id';
        var addData = function(newObj) {
            var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata.push(newObj);
            console.log(newObj)
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };

        var getData = function(){
            var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            }
            return mydata || [];
        };

        return {
            addData: addData,
            getData: getData
        };
    }
})();