/**
 * Created by tifet on 2017-05-01.
 */

(function () {
    'use strict';

    angular.module('BlurAdmin.theme')
        .provider('urlGetService', urlGetService);

    /** @ngInject */
    function urlGetService() {


        var KEY = 'App.Id';
        this.myData='';
        this.addData = function(newObj) {
            // var mydata = $window.sessionStorage.getItem(KEY);
            // if (mydata) {
            //     mydata = JSON.parse(mydata);
            // } else {
            //     mydata = [];
            // }
            // mydata.push(newObj);
            // console.log(newObj)
            // $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
            console.log(newObj);
            this.myData = newObj;
        };

            this.getData = function(){
            return this.myData;
        };

        this.$get= function () {
            return {
                addData: function (myData) {
                   this.myData = myData;
                },
                getData: function () {
                    return this.myData;
                }
            };
        }

    }
})();