/**
 * Created by tifet on 2017-03-26.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme')
        .service('employerService', employerService);

    /** @ngInject */
    function employerService($http) {

        // var KEY = 'App.Cours';
        // var addData = function(newObj) {
        //     var mydata = $window.sessionStorage.getItem(KEY);
        //     if (mydata) {
        //         mydata = JSON.parse(mydata);
        //     } else {
        //         mydata = [];
        //     }
        //     mydata.push(newObj);
        //     console.log(newObj)
        //     $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        // };
        //
        // var getData = function(){
        //     var mydata = $window.sessionStorage.getItem(KEY);
        //     if (mydata) {
        //         mydata = JSON.parse(mydata);
        //     }
        //     return mydata || [];
        // };
        //
        // return {
        //     addData: addData,
        //     getData: getData
        // };

            var getEmplyerList = function () {
            $http.defaults.headers.post["Content-Type"] = "application/json";
            $http.get('/employer')
                .success(function (data){
                    return data._embedded.employers;
                });
        }

        }
    })();