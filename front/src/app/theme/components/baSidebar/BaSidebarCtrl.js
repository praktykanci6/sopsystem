/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')
    .controller('BaSidebarCtrl', BaSidebarCtrl);

  /** @ngInject */
  function BaSidebarCtrl($scope,$rootScope, baSidebarService) {
      $scope.menuItems=[];
     var preparItem = function() {
         var menuItems = baSidebarService.getMenuItems();
          angular.forEach(menuItems,function (item) {
              console.log(item,$rootScope.role);
              if($rootScope.role == item.role || item.role == 'DASHBOARD') {
                  console.log('dodaje menu');
                  $scope.menuItems.push(item);
              }
          });
         $scope.defaultSidebarState = $scope.menuItems[0].stateRef;
     };
     preparItem();
     console.log($scope.menuItems);

    $scope.hoverItem = function ($event) {
      $scope.showHoverElem = true;
      $scope.hoverElemHeight =  $event.currentTarget.clientHeight;
      var menuTopValue = 66;
      $scope.hoverElemTop = $event.currentTarget.getBoundingClientRect().top - menuTopValue;
    };

    $scope.$on('$stateChangeSuccess', function () {
      if (baSidebarService.canSidebarBeHidden()) {
        baSidebarService.setMenuCollapsed(true);
      }
    });
  }
})();