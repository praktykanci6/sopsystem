/**
 * Created by tifet on 2017-05-14.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.dashboard')
        .controller('StudentDashboardCtrl',StudentDashboardCtrl);
    /** @ngInject */
    function StudentDashboardCtrl($scope,$http,$state) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.data;
        $scope.interviews = [];
        $scope.applications = [];
        $scope.tabs=[{active:true,title:"zapis na rozmowy kwalifikacyjne"},
            {active:false,title:'Wybór praktyki'}];
        $scope.progres=50;
        console.log('Laduje strone startowa studenta');
        $http.get('/studentPractice')
            .success(function (data) {
                $scope.data = data;
                console.log('praktyki studenta',data);
            });
        $http.get('/studentInterview')
            .success(function (data) {
                $scope.interviews = data;
                console.log('rozmowy studenta',data);
            });
        $http.get('/studentApplication')
            .success(function (data) {
                $scope.applications = data;
            });
        $scope.getTemplate = function () {
            window.open(window.location.origin + window.location.pathname + '/getTemplate?id=' + $scope.data[0].id);
        }
    }
})();