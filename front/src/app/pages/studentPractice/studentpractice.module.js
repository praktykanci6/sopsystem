/**
 * Created by tifet on 2017-05-14.
 */
(function(){
    'use strict';
    angular.module('BlurAdmin.pages.studentpractice',[])
        .config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('studentpractice', {
                url: '/praktyki',
                template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
                abstract: true,
                title: 'Praktyki',
                sidebarMeta: {
                    icon: 'ion-document-text',
                    order: 100,
                },
                role: 'STUDENT'
            })
            .state('studentpractice.addPractice', {
                url: '/dodajPraktyke',
                templateUrl: 'app/pages/studentPractice/pages/addPractice.html',
                title: 'Dodaj praktyke',
                controller: 'AddPracticeCtrl',
                sidebarMeta: {
                    order: 50
                },
                role: 'STUDENT'
            })
            .state('studentpractice.practiceList', {
                url: '/listaPraktyk',
                templateUrl: 'app/pages/studentPractice/pages/practiceList.html',
                title: 'Wybierz praktyke',
                controller: 'PracticeListCtrl',
                sidebarMeta: {
                    order: 0
                },
                role: 'STUDENT'
            })
            .state('studentpractice.addContract', {
                url: '/dodajUmowe',
                templateUrl: 'app/pages/studentPractice/pages/addContract.html',
                title: 'Umowa o prace',
                controller: 'AddContractCtrl',
                sidebarMeta: {
                    order: 50
                },
                role: 'STUDENT'
            })
            .state('studentpractice.generateTemplate', {
                url: '/dodajPraktykeSzablon',
                templateUrl: 'app/pages/studentPractice/pages/addPracticeTemplate.html',
                title: 'Szablon praktyk',
                controller: 'AddPracticeTemplateCtrl',
                sidebarMeta: {
                    order: 100
                },
                role: 'STUDENT'
            })
            .state('studentpractice.agreementApplicationTemplate', {
                url: '/dodajumowe',
                templateUrl: 'app/pages/studentPractice/pages/agreementApplicationTemplate.html',
                title: 'Szablon umowy',
                controller: 'AgreementApplicationTemplateCtrl',
                sidebarMeta: {
                    order: 150
                },
                role: 'STUDENT'
            })
            .state('studentpractice.employer', {
                url: '/praktykodawca',
                templateUrl: 'app/pages/studentPractice/pages/employer.html',
                title: 'Praktykodawca',
                controller: 'EmployerCtrl',
                // sidebarMeta: {
                //     order: 150
                // },
                role: 'STUDENT'
            })
    }
})();