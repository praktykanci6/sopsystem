/**
 * Created by tifet on 2017-05-30.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.studentpractice')
        .controller('EmployerCtrl',EmployerCtrl)
    function EmployerCtrl($scope,$http,$state,toastr,$timeout,$rootScope, $uibModal){
        $scope.stage =true;
        $scope.assistant=[];
        var id = $rootScope.employer.id;
        $http.defaults.headers.post["Content-Type"] = "application/json";
        //tabs dane praktykodawcy
        $http.get('/employer/'+id).success(function (data) {
            $scope.employer = data;
        });
        $http.get('/employerAddresses/search/getByEmployerId?id='+id)
            .success(function(data1) {
                $scope.addresses = data1._embedded.employAddresses;
            });
        var tab = 1;
        var getEmployerPractice = function () {
            $http.get('/practicesEmployer?id=' + $rootScope.employer.id)
                .success(function (data) {
                    $scope.practice = data;
                })
                .error(function (data) {
                    console.log(data.message);
                });
        };
        getEmployerPractice();

        $scope.setTab = function (tabId) {
            tab = tabId;
        };
        $scope.isSet = function (tabId) {
            return tab === tabId;
        };
        $scope.form={name:''};
        $scope.$watch('form.name', function(newVal, oldVal){
            console.log('changed :', newVal);

        });

        //inicjalizacja map google
        function initialize() {
            var mapCanvas = document.getElementById('google-maps');
            var mapOptions = {
                center: new google.maps.LatLng( 50.014688, 20.92079),
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(mapCanvas, mapOptions);
            var CentralPark = new google.maps.LatLng(50.014688, 20.92079);
            new google.maps.Marker({
                position: CentralPark,
                title:'Tarnów',
                map: map
            });
        }
        $timeout(function(){
            initialize();
        }, 100);
         $scope.addApplication = function(item){
             console.log('item',item);
             item.employerId = id;
             $http.post('/addStudentApplication',item)
                 .success(function(data){
                     console.log(data);
                     toastr.success(data.notification , 'Info',
                         {
                             "autoDismiss": false,
                             "positionClass": "toast-top-full-width",
                             "type": "success",
                             "timeOut": "10000",
                             "extendedTimeOut": "2000",
                             "allowHtml": false,
                             "closeButton": true,
                             "tapToDismiss": false,
                             "progressBar": false,
                             "newestOnTop": true,
                             "maxOpened": 0,
                             "preventDuplicates": false,
                             "preventOpenDuplicates": false
                         });
                     $scope.stage = false;
                     $state.go('studentpractice.practiceList');
                 })
                 .error(function (data) {
                     toastr.error('Wystąpił wewnetrzny bład servera!' , 'Błąd',
                         {
                             "autoDismiss": false,
                             "positionClass": "toast-top-full-width",
                             "type": "success",
                             "timeOut": "10000",
                             "extendedTimeOut": "2000",
                             "allowHtml": false,
                             "closeButton": true,
                             "tapToDismiss": false,
                             "progressBar": false,
                             "newestOnTop": true,
                             "maxOpened": 0,
                             "preventDuplicates": false,
                             "preventOpenDuplicates": false
                         });
                 });

         };

        $scope.showModal = function (item) {
            $uibModal.open({
                animation: false,
                controller: 'ModalApplicationPreferenceCtrl',
                templateUrl: 'app/pages/studentPractice/pages/widgets/modalApplicationPreference.html'
            }).result.then(function (link) {
                console.log('link retun :',link);
                item.preference = $rootScope.link;
                $scope.addApplication(item);
            });
        };
    }
})();
