/**
 * Created by tifet on 2017-06-03.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.studentpractice')
        .controller('ModalApplicationPreferenceCtrl', ModalApplicationPreferenceCtrl);

    /** @ngInject */
    function ModalApplicationPreferenceCtrl($scope, $uibModalInstance,$rootScope) {
        $scope.item=[1,2,3,4,5,6,7,8,9];
        $scope.link;
        $scope.ok = function () {
            console.log($scope.link);
            $rootScope.link = $scope.link;
            $uibModalInstance.close();
        };
    }

})();
