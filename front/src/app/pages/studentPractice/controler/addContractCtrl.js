/**
 * Created by tifet on 2017-05-14.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.studentpractice')
        .controller('AddContractCtrl',AddContractCtrl);
    /** @ngInject */
    function AddContractCtrl($scope,$http,$state,toastr) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.file;
        $scope.fileName = 'Nie wybrano pliku';
        $scope.practice={};
        $scope.entities=[];
        $scope.selectedStudyFields;
        $http.get('/studentCourses')
            .success(function (data) {
                console.log('kierunki studenta',data);
                $scope.entities = data;
            });
        $scope.fileNameChanged = function (ele) {
            var files = ele.files;
            var l = files.length;
            console.log('plik dane rozmiar ', files[0]);
            $scope.file = files[0];
            $scope.fileName =  ele.files[0].name;
            $scope.$apply();
        };
        $scope.updateSelection = function(position, entities) {
            angular.forEach(entities, function(subscription, index) {
                if (position != index) {
                    subscription.checked = false;
                }else {
                    $scope.selectedStudyFields = subscription;
                }
            });
        };

        $scope.uploadFile = function () {
            var fileInput = document.getElementById('uploadFile');
            fileInput.click();
        };

        $scope.cancel = function () {
            $state.go('dashboard');
        };

        $scope.tabs=[{active:true,title:"zapis na rozmowy kwalifikacyjne"},
            {active:false,title:'Wybór praktyki'}];
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.template=true;
        $scope.data={};
        $scope.employer={
            name:"",
            fullName:"",
            description:"",
            employerAddresses:[]
        };
        $scope.practice={};
        var address={
            departament:false,
            street:'',
            numberBuilding:'',
            city:'',
            postCode:'',
            country:'',
            phone1:'',
            phone2:'',
            fax:''
        };

        $scope.employer.employerAddresses.push(address);
        $scope.data={
            employer : $scope.employer,
            practice : $scope.practice,
        };

        $scope.saveAgreement = function(){
            if($scope.entities.length == 1){
                $scope.selectedStudyFields = $scope.entities[0];
            }
            console.log('plik',$scope.file);
            if($scope.file != undefined ) {
                var data = {
                    employer: $scope.coordinator,
                    assistant: $scope.assistant,
                    practice: $scope.practice,
                    company: $scope.employer,
                    company1: $scope.employer1
                };
                var fd = new FormData();
                fd.append('data',angular.toJson(data));
                fd.append('file',$scope.file);
                fd.append('id',$scope.selectedStudyFields.id);
                $http.post('/addAgreement',fd,
                    {
                        transformRequest : angular.identity,
                        headers : {
                            'Content-Type' : undefined
                        }})
                    .success(function (data) {
                        toastr.success('Dodano praktyke umowe' , 'Info',
                            {
                                "autoDismiss": false,
                                "positionClass": "toast-top-full-width",
                                "type": "success",
                                "timeOut": "10000",
                                "extendedTimeOut": "2000",
                                "allowHtml": false,
                                "closeButton": true,
                                "tapToDismiss": false,
                                "progressBar": false,
                                "newestOnTop": true,
                                "maxOpened": 0,
                                "preventDuplicates": false,
                                "preventOpenDuplicates": false
                            });
                        $state.go('dashboard');
                        console.log('Dodano praktykr');
                    })
                    .error(function (data) {
                        alert('Nie udało dodac sie praktyki');
                    });
                }
                if($scope.selectedStudyFields == null){
                    toastr.error('Nie wybrano kierunku' , 'Błąd',
                        {
                            "autoDismiss": false,
                            "positionClass": "toast-top-full-width",
                            "type": "error",
                            "timeOut": "10000",
                            "extendedTimeOut": "2000",
                            "allowHtml": false,
                            "closeButton": true,
                            "tapToDismiss": false,
                            "progressBar": false,
                            "newestOnTop": true,
                            "maxOpened": 0,
                            "preventDuplicates": false,
                            "preventOpenDuplicates": false
                        })
                }
                if($scope.file == undefined){
                    toastr.error('Nie dodano pliku' , 'Błąd',
                        {
                            "autoDismiss": false,
                            "positionClass": "toast-top-full-width",
                            "type": "error",
                            "timeOut": "10000",
                            "extendedTimeOut": "2000",
                            "allowHtml": false,
                            "closeButton": true,
                            "tapToDismiss": false,
                            "progressBar": false,
                            "newestOnTop": true,
                            "maxOpened": 0,
                            "preventDuplicates": false,
                            "preventOpenDuplicates": false
                        })
                }

        };
    }
})();

