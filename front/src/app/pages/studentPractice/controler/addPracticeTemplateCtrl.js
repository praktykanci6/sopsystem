/**
 * Created by tifet on 2017-05-17.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.studentpractice')
        .controller('AddPracticeTemplateCtrl',AddPracticeTemplateCtrl);
    /** @ngInject */
    function AddPracticeTemplateCtrl($scope,$http,$state,$filter,employerService,$window) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.template=true;
        $scope.employer1={
            name:"",
            fullName:"",
            description:"",
            addresses:[]
        };
        $scope.employer={
            name:"",
            fullName:"",
            description:"",
            addresses:[]
        };
        $scope.assistant={};
        $scope.coordinator={};
        $scope.practice={};
        var address={
            departament:false,
            street:'',
            numberBuilding:'',
            city:'',
            postCode:'',
            country:'',
            phone1:'',
            phone2:'',
            fax:''
        };
        var address1={
            departament:true,
            street:'',
            numberBuilding:'',
            city:'',
            postCode:'',
            country:'',
            phone1:'',
            phone2:'',
            fax:''
        };
        $scope.employer.addresses.push(address);
        $scope.employer1.addresses.push(address1);
        $scope.cancel = function () {
            $state.go('dashboard');
        };
        $scope.cancel1 = function () {
            $state.go('dashboard');
        };

        //generowanie szablonu
        $scope.generateTemplate = function(){
            $scope.employer.employerAddresses = $scope.employer.addresses;
            $scope.employer1.employerAddresses = $scope.employer1.addresses;
            var data = {
                employer : $scope.coordinator,
                assistant : $scope.assistant,
                practice : $scope.practice,
                company : $scope.employer,
                company1 : $scope.employer1

            };
            $http.post('/aPT1',data,{
                responseType: 'arraybuffer'
            })
                .success(function (data, status, headers) {
                    headers = headers();
                    console.log(headers);
                    var filename = 'KartaZgloszeniePraktyki.pdf';
                    var contentType = headers['content-type'];

                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], { type: contentType});
                        console.log(blob);
                        var url = $window.URL.createObjectURL(blob);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", filename);

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        console.log(ex);
                    }
                })
                .error(function (data) {
                    alert(data);
                    console.error(data.message)
                });
        };

    }
})();
