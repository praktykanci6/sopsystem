/**
 * Created by tifet on 2017-05-14.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.studentpractice')
        .controller('PracticeListCtrl',PracticeListCtrl);
    /** @ngInject */
    function PracticeListCtrl($scope,$http,$state,$filter,employerService,$rootScope) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        console.log('Lista praktyk');
        $scope.smartTablePageSize = 100;
        $scope.searchText="";
        $http.get('/employersForStudent')
            .success(function (data){
                $scope.rowCollection =[];
                $scope.rowCollection = data;
            })
            .error(function (data) {
                console.log(data.message);
            });
        var employ = {
            name:'alibaba',
            fullName:'ali',
            description:'opis'
        };
        $scope.displayedCollection=[].concat($scope.rowCollection);
        $scope.displayedCollection.push(employ);
        // $scope.searchEmployer = function(searchText){
        //     $http.get("/employer/search/fullTsxtSearch?fragments="+searchText)
        //         .success(function (data) {
        //             $scope.rowCollection = data._embedded.employers;
        //             console.log(data);
        //         })
        // };
        $scope.employerDetail = function (item) {
            $rootScope.employer=item;
            $state.go('studentpractice.employer')
        }
    }
})();
