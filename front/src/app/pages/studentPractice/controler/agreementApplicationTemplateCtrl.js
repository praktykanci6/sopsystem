/**
 * Created by dawid-ss on 2017-05-17.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.studentpractice')
        .controller('AgreementApplicationTemplateCtrl',AgreementApplicationTemplateCtrl);
    /** @ngInject */
    function AgreementApplicationTemplateCtrl($scope,$http,$state,$filter,employerService,$window) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.template=true;
        $scope.data={};
        $scope.employer={
            name:"",
            fullName:"",
            description:"",
            addresses:[]
        };
        $scope.practice={};
        var address={
            departament:false,
            street:'',
            numberBuilding:'',
            city:'',
            postCode:'',
            country:'',
            phone1:'',
            phone2:'',
            fax:''
        };

        $scope.employer.addresses.push(address);
        $scope.data={
            employer : $scope.employer,
            practice : $scope.practice,
        };
        $scope.cancel = function () {
            $state.go('dashboard');
        };
        $scope.cancel1 = function () {
            $state.go('dashboard');
        };

        //generowanie szablonu
        $scope.generateTemplate = function(){
            $scope.employer.employerAddresses = $scope.employer.addresses;
            $scope.data.company = $scope.employer;
            $http.post('/aPT2',$scope.data,{
                responseType: 'arraybuffer'
            })
                .success(function (data, status, headers) {
                    headers = headers();
                    console.log(headers);
                    var filename = 'umowa.pdf';
                    var contentType = headers['content-type'];

                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], { type: contentType});
                        console.log(blob);
                        var url = $window.URL.createObjectURL(blob);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", filename);

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        console.log(ex);
                    }
                })
                .error(function (data) {
                    alert(data);
                });
        };

    }
})();
