/**
 * Created by tifet on 2017-05-14.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.studentpractice')
        .controller('AddPracticeCtrl',AddPracticeCtrl);
    /** @ngInject */
    function AddPracticeCtrl($scope,$http,$state,toastr) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.entities=[];
        $scope.selectedStudyFields;
        $http.get('/studentCourses')
            .success(function (data) {
                console.log('kierunki studenta',data);
                $scope.entities = data;
            });
        $scope.template=true;
        $scope.employer1={
            name:"",
            fullName:"",
            description:"",
            employerAddresses:[]
        };
        $scope.employer={
            name:"",
            fullName:"",
            description:"",
            employerAddresses:[]
        };
        $scope.assistant={};
        $scope.coordinator={};
        $scope.practice={};
        var address={
            departament:false,
            street:'',
            numberBuilding:'',
            city:'',
            postCode:'',
            country:'',
            phone1:'',
            phone2:'',
            fax:''
        };
        var address1={
            departament:true,
            street:'',
            numberBuilding:'',
            city:'',
            postCode:'',
            country:'',
            phone1:'',
            phone2:'',
            fax:''
        };
        $scope.employer.employerAddresses.push(address);
        $scope.employer1.employerAddresses.push(address1);
        $scope.cancel = function () {
            $state.go('dashboard');
        };
        $scope.cancel1 = function () {
            $state.go('dashboard');
        };


        $scope.updateSelection = function(position, entities) {
            angular.forEach(entities, function(subscription, index) {
                if (position != index) {
                    subscription.checked = false;
                }else {
                    $scope.selectedStudyFields = subscription;
                }
            });
        };

        $scope.savePractice = function(){
            console.log('plik',$scope.file);
            if($scope.entities.length == 1){
                $scope.selectedStudyFields = $scope.entities[0];
            }
            if($scope.file != null && $scope.selectedStudyFields != null) {
                var data = {
                    employer: $scope.coordinator,
                    assistant: $scope.assistant,
                    practice: $scope.practice,
                    company: $scope.employer,
                    company1: $scope.employer1
                };
                var fd = new FormData();
                fd.append('data',angular.toJson(data));
                fd.append('file',$scope.file);
                fd.append('id',$scope.selectedStudyFields.id);
                $http.post('/addOwenPractice',fd,
                {
                    transformRequest : angular.identity,
                        headers : {
                    'Content-Type' : undefined
                }})
                    .success(function (data) {
                        toastr.error('Dodano praktyke studencka' , 'Info',
                            {
                                "autoDismiss": false,
                                "positionClass": "toast-top-full-width",
                                "type": "success",
                                "timeOut": "10000",
                                "extendedTimeOut": "2000",
                                "allowHtml": false,
                                "closeButton": true,
                                "tapToDismiss": false,
                                "progressBar": false,
                                "newestOnTop": true,
                                "maxOpened": 0,
                                "preventDuplicates": false,
                                "preventOpenDuplicates": false
                            })
                        $state.go('dashboard');
                        console.log('Dodano praktykr');
                    })
                    .error(function (data) {
                        alert('Nie udało dodac sie praktyki');
                    })
            }else{
                var error = '';
                if($scope.selectedStudyFields == null){
                    toastr.error('Nie wybrano kierunku' , 'Błąd',
                        {
                            "autoDismiss": false,
                            "positionClass": "toast-top-full-width",
                            "type": "error",
                            "timeOut": "10000",
                            "extendedTimeOut": "2000",
                            "allowHtml": false,
                            "closeButton": true,
                            "tapToDismiss": false,
                            "progressBar": false,
                            "newestOnTop": true,
                            "maxOpened": 0,
                            "preventDuplicates": false,
                            "preventOpenDuplicates": false
                        })
                }
                if($scope.file == null){
                    toastr.error('Nie dodano pliku' , 'Błąd',
                        {
                            "autoDismiss": false,
                            "positionClass": "toast-top-full-width",
                            "type": "error",
                            "timeOut": "10000",
                            "extendedTimeOut": "2000",
                            "allowHtml": false,
                            "closeButton": true,
                            "tapToDismiss": false,
                            "progressBar": false,
                            "newestOnTop": true,
                            "maxOpened": 0,
                            "preventDuplicates": false,
                            "preventOpenDuplicates": false
                        })
                }
            }

        };

        $scope.file;
        $scope.fileName = 'Nie wybrano pliku';
        $scope.tabs1=true;
        $scope.tabs2=false;
        $scope.practice={};
        $scope.fileNameChanged = function (ele) {
            var files = ele.files;
            var l = files.length;
            console.log('plik dane rozmiar ', files[0]);
            $scope.file = files[0];
            $scope.fileName =  ele.files[0].name;
            $scope.$apply();
        };

        $scope.uploadPicture = function () {
            var fileInput = document.getElementById('uploadFile');
            fileInput.click();
        };

        $scope.cancel = function () {
            $state.go('dashboard');
        };

    }
})();
