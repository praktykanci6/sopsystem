/**
 * Created by k.kabat on 29.04.17
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.reports', [])
            .config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
                .state('report', {
                    url: '/raporty',
                    template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
                    abstract: true,
                    title: 'Raporty',
                    sidebarMeta: {
                        icon: 'ion-document-text',
                        order: 200,
                    },
                    role:'ADMINISTRATOR'
                })
                .state('report.reportList',{
                    url: '/raporty',
                    templateUrl: 'app/pages/reports/Reports.html',
                    title: 'Raporty',
                    controller: 'ReportsController',
                    sidebarMeta:{
                        order:0,
                    },
                    role:'ADMINISTRATOR'
                })
                .state('report.workPlaceDeclaration', {
                    url: '/raporty/oswiadczeniaZakladuPracy',
                    templateUrl: 'app/pages/reports/WorkPlaceDeclaration.html',
                    title: 'Oswiadczenie zakladu pracy',
                    controller: 'WorkPlaceDeclarationCtrl',
                 
                    role:'ADMINISTRATOR'
                })


        // sidebarMeta:{
        //     order:50,
        // },


    }
})();