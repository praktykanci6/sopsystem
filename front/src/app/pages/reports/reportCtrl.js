(function () {
    'use strict';
    angular.module('BlurAdmin.pages.reports')
            .controller('ReportsController', ReportsController)

    function ReportsController($scope, $http, $state) {
        $scope.baseUrl = window.location.origin + window.location.pathname + '/generateReport?id=';

        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.rowCollection = {};

        $http.get('/availableReports')
                .success(function (data) {
                    $scope.rowCollection = data;                    
                });

        $scope.generateReport = function (item) {
            
            if (item !== 'Karta oświadczenia zakładu pracy') {                
                window.open(window.location.origin + window.location.pathname + '/generateReport?id=' + item);
            } else {
                console.log('Karta oświadczniea pracy');
                $state.go('report.workPlaceDeclaration');
            }
        }        
    }
})();