(function () {
    'use strict';

    angular.module('BlurAdmin.pages.newAcademicYearWizard')
            .controller('AcademicYearWizardCtrl', AcademicYearWizardCtrl);

    /** @ngInject */
    function AcademicYearWizardCtrl($scope, $http) {
        var vm = this;
        
        vm.tabs = [];

        vm.tabNum = 0;
        vm.progress = 0;

        vm.addTab = function (tab) {
            tab.setPrev(vm.tabs[vm.tabs.length - 1]);
            vm.tabs.push(tab);
            vm.selectTab(0);
        };

        $scope.$watch(angular.bind(vm, function () {
            return vm.tabNum;
        }), calcProgress);

        vm.selectTab = function (tabNum) {
            vm.tabs[vm.tabNum].submit();
            if(vm.isLastTab()){
                $http({
                    url: '/sopsystem/save/academicYear',
                    method: 'POST',
                    data: vm.academicYear
                }).success(function(response){
                    
                });                                     
//                $http.post('/sopsystem/save/academicYear',vm.academicYear)
            }
            else if (vm.tabs[tabNum].isAvailiable()) {
                vm.tabNum = tabNum;
                vm.tabs.forEach(function (t, tIndex) {
                    tIndex == vm.tabNum ? t.select(true) : t.select(false);
                });
            } 
        };

        vm.isFirstTab = function () {
            return vm.tabNum == 0;
        };

        vm.isLastTab = function () {
            return vm.tabNum == vm.tabs.length - 1;
        };

        vm.nextTab = function () {
            vm.selectTab(vm.tabNum + 1)
        };

        vm.previousTab = function () {
            vm.selectTab(vm.tabNum - 1)
        };

        function calcProgress() {
            vm.progress = ((vm.tabNum + 1) / vm.tabs.length) * 100;
        }
        
        vm.academicYear = {};
        vm.academicYearStudyField = {};        

        $scope.studyFields = {};
        $scope.selectedStudyFields = {};

        $scope.getStudyFields = function () {
            $http.get('/sopsystem/coursesOfStudy').success(function (data) {
                $scope.studyFields = data._embedded.coursesOfStudies;
            })
        };
        $scope.getStudyFields();
        
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        
        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };
    }

})();

