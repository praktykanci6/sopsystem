/**
 * Created by tifet on 2017-04-25.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.profile')
        .controller('UserProfileCtrl', UserProfileCtrl);

    function UserProfileCtrl($scope, $http, $uibModal) {
        $scope.user = {};
        var userCopy ={};
        var changed = false;
        var getUserProfile = function () {
            $http.get('/currentUser')
                .success(function (data) {
                    $scope.user = data;
                    userCopy = angular.copy($scope.user);
                    console.log(userCopy);
                })
                .error(function (message) {
                    console.log(message);
                })
        };

        getUserProfile();
        $scope.isEditable = false;
        $scope.isDisabled = true;
        $scope.edit = function () {
            if($scope.isEditable){
                $scope.isEditable = false;
                $scope.isDisabled = true;
            } else {
                $scope.isEditable = true;
                $scope.isDisabled = false;
            }
        };

        $scope.saveProfile = function () {
            var f = new FormData();
            f.append("user",$scope.user);
            $http.post('/saveUser',$scope.user)
                .success(function (data) {
                    console.log(data);
                    $scope.usser = data;
                    $scope.isEditable = false;
                    $scope.isDisabled = true;

                })
                .error(function (message) {
                    console.log(message);
                    alert(message.error);

                })
        };
        $scope.showModal = function () {
            $uibModal.open({
                animation: false,
                controller: 'ProfileModalCtrl',
                templateUrl: 'app/pages/profile/profileModal.html'
            }).result.then(function () {
                $scope.saveProfile();
            });
        };
        $scope.checkChange = function () {

            if($scope.user.firstName != userCopy.firstName){
                changed = true;
            }else if($scope.user.lastName != userCopy.lastName){
                changed = true;
            }else if($scope.user.password != userCopy.password){
                changed = true;
            }else if($scope.user.phone = userCopy.phone){
                changed = true;
            } else if ($scope.user.phone2 != userCopy.phone2){
                changed = true;
            } else if ($scope.user.email != userCopy.email){
                changed = true;
            }
            if(changed){
                $scope.showModal();
                changed=false;
            } else{
                $scope.isEditable = false;
                $scope.isDisabled = true;
            }
        }
    }
})();
