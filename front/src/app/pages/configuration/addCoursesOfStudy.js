/**
 * Created by tifet on 2017-03-14.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.configuration')
        .controller('AddCoursesOfStudy', AddCoursesOfStudy);
    /** @ngInject */
    function AddCoursesOfStudy($scope, $state, $filter, $http, $location, $stateParams, toastr, toastrConfig, editableOptions, editableThemes) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        var objectId = $stateParams.id;
        var error = '';
        var re = '';
        var req = '';
        var errorCheck = false;
        var errorList = ['Pole Nazwa kierunku jest wymagane <br />', 'Pole Stopień studiów jest wymagane <br />', 
            'Pole Tryb studiów jest wymagane <br />', 'Pole Liczba semestrów jest wymagane <br />', 'Pole Profil kształcenia jest wymagane <br />'
            , 'Pole Tytuł zawodowy jest wymagane <br />'];
        $scope.studiesDegreesList = [];
        $scope.numberOfsemestrList = [];
        $scope.coursesOfStudy = {
            id: null,
            polo: null
        };
        if (objectId != undefined) {
            $http.get('/coursesOfStudy/' + objectId)
                .success(function (data) {
                    $scope.coursesOfStudy = data._embedded;
                    $scope.coursesOfStudy.nameCourses = data.nameCourses;
                    $scope.coursesOfStudy.polon = data.polon;
                    $scope.coursesOfStudy.specilaization = data.specilaization
                })
                .error(function (data) {
                });
        }
        $scope.studyModes = [];
        $scope.educationalProfileList = [];
        $scope.degreeList = [];
        $http.get('/studyMode')
            .success(function (data) {
                console.log(data._embedded);
                $scope.studyModes = data._embedded.studyModes;
            })
            .error(function (data) {

            });
        var validate = function () {
            re = '';
            var errorCheck = false;
            if ($scope.coursesOfStudy.nameCourses === undefined) {
                error += errorList[0],
                    errorCheck = true
            }
            if ($scope.coursesOfStudy.studiesDegree === undefined) {
                error += errorList[1],
                    errorCheck = true
            }
            if ($scope.coursesOfStudy.studyMode === undefined) {
                error += errorList[2],
                    errorCheck = true
            }
            if ($scope.coursesOfStudy.numberOfSemesters === undefined) {
                error += errorList[3],
                    errorCheck = true
            }
            if ($scope.coursesOfStudy.profile === undefined) {
                error += errorList[4],
                    errorCheck = true
            }
            if ($scope.coursesOfStudy.degree === undefined) {
                error += errorList[5],
                    errorCheck = true
            }
            re = error + req;
//            error = 'Pole ';
            return errorCheck;
        }
        $http.get('/numberOfSemester')
            .success(function (data) {
                console.log(data._embedded);
                $scope.numberOfSemesterList = data._embedded.numberOfSemesterses;
                console.log($scope.numberOfsemestrList);
            })
            .error(function (data) {

            });
        $http.get('/studiesDegree')
            .success(function (data) {
                console.log(data._embedded);
                $scope.studiesDegreesList = data._embedded.studiesDegrees;
            })
            .error(function (data) {

            });
        $http.get('/educationalProfile')
            .success(function (data) {
                console.log(data._embedded);
                $scope.educationalProfileList = data._embedded.educationalProfiles;
            })
            .error(function (data) {

            });
        $http.get('/degree')
            .success(function (data) {
                console.log(data._embedded);
                $scope.degreeList = data._embedded.degrees;
            })
            .error(function (data) {

            });
        $scope.backToCoursesOfStudyList = function () {
            $state.go('configuration.coursesOfStudy')
        }

        $scope.addCourses = function () {
            console.log($scope.coursesOfStudy);
            if (validate()) {
                toastr.error(re, 'Błąd',
                    {
                        "autoDismiss": false,
                        "positionClass": "toast-top-full-width",
                        "type": "error",
                        "timeOut": "10000",
                        "extendedTimeOut": "2000",
                        "allowHtml": true,
                        "closeButton": true,
                        "tapToDismiss": false,
                        "progressBar": false,
                        "newestOnTop": true,
                        "maxOpened": 0,
                        "preventDuplicates": false,
                        "preventOpenDuplicates": false
                    })
            } else {
                if(objectId == null) {
                    $stateParams.id = null,
                    $http.post("/coursesOfStudy", {

                        nameCourses: $scope.coursesOfStudy.nameCourses,
                        studiesDegree: $scope.coursesOfStudy.studiesDegree._links.self.href,
                        studyMode: $scope.coursesOfStudy.studyMode._links.self.href,
                        degree: $scope.coursesOfStudy.degree._links.self.href,
                        numberOfSemesters: $scope.coursesOfStudy.numberOfSemesters._links.self.href,
                        profile: $scope.coursesOfStudy.profile._links.self.href,
                        polon: $scope.coursesOfStudy.polon,
                        specilaization: $scope.coursesOfStudy.specilaization
                    })
                        .success(function (data) {
                            $location.path("/konfiguracja/kierunki")
                        })
                        .error(function (data) {
                            toastr.error('Nieoczekiwany bład zapisu!!', 'Błąd',
                                {
                                    "autoDismiss": false,
                                    "positionClass": "toast-top-full-width",
                                    "type": "error",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "2000",
                                    "allowHtml": false,
                                    "closeButton": false,
                                    "tapToDismiss": true,
                                    "progressBar": false,
                                    "newestOnTop": true,
                                    "maxOpened": 0,
                                    "preventDuplicates": false,
                                    "preventOpenDuplicates": false
                                })
                        })
                } else {
                    $http.post("/coursesOfStudy", {
                        id : objectId,
                        nameCourses: $scope.coursesOfStudy.nameCourses,
                        studiesDegree: $scope.coursesOfStudy.studiesDegree._links.self.href,
                        studyMode: $scope.coursesOfStudy.studyMode._links.self.href,
                        degree: $scope.coursesOfStudy.degree._links.self.href,
                        numberOfSemesters: $scope.coursesOfStudy.numberOfSemesters._links.self.href,
                        profile: $scope.coursesOfStudy.profile._links.self.href,
                        polon: $scope.coursesOfStudy.polon,
                        specilaization: $scope.coursesOfStudy.specilaization
                    })
                        .success(function (data) {
                            $stateParams.id = null,
                            $location.path("/konfiguracja/kierunki")
                        })
                        .error(function (data) {
                            toastr.error('Nieoczekiwany bład zapisu!!', 'Błąd',
                                {
                                    "autoDismiss": false,
                                    "positionClass": "toast-top-full-width",
                                    "type": "error",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "2000",
                                    "allowHtml": false,
                                    "closeButton": false,
                                    "tapToDismiss": true,
                                    "progressBar": false,
                                    "newestOnTop": true,
                                    "maxOpened": 0,
                                    "preventDuplicates": false,
                                    "preventOpenDuplicates": false
                                })
                        })
                }
            }
        }
    }
})();