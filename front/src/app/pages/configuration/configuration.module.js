/**
 * Created by dawid-ss on 07.03.2017.
 */
(function(){
    'use strict';

    angular.module('BlurAdmin.pages.configuration',[])
        .config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {
      $stateProvider
          .state('configuration',{
              url:  '/konfiguracja',
              template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
              abstract: true,
              // controller: 'ConfigurationPagesCtrl',
              title: 'Konfiguracja',
              sidebarMeta: {
                  icon: 'ion-settings',
                  order:150,
              },
              role:'ADMINISTRATOR'
          })
          .state('configuration.coursesOfStudy',{
              url:'/kierunki',
              templateUrl: 'app/pages/configuration/coursesOfStudy/tables.html',
              controller: 'CoursOfStudyCtrl',
              title:'Kierunki studiów',
              sidebarMeta:{
                  order:0,
              },
              role:'ADMINISTRATOR'
      })
          .state('configuration.addCourses',{
              url: '/dodajKierunek',
              params :{
                  id : null
              },
              templateUrl: 'app/pages/configuration/coursesOfStudy/addCours.html',
              title:'Dodaj kierunek',
              controller: 'AddCoursesOfStudy',


              // sidebarMeta:{
              //     order:50,
              // },
          })

      $urlRouterProvider.when('/konfiguracja/','/konfiguracja/kierunki','/konfiguracja/dodajKierunek'
        );
    }
})();