/**
 * Created by dawid-ss on 07.03.2017.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.configuration')
        .controller('CoursOfStudyCtrl',CoursOfStudyCtrl);
    /** @ngInject */
    function  CoursOfStudyCtrl($scope,$filter,$http,$state, $location,editableOptions, editableThemes) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.smartTablePageSize = 10;
        $scope.coursesOfStudy = [];
        $scope.numberOfsemestrList=[]
        $http.get('/coursesOfStudy')
            .success(function (data) {
                console.log(data._embedded);
                $scope.coursesOfStudy = data._embedded.coursesOfStudies;
            })
            .error(function(data){

            });

        $scope.showCours = function (cours) {
            console.log(cours)
            if(cours.numberOfSemesters && $scope.numberOfsemestrList.length) {

                var selected = $filter('filter')($scope.numberOfsemestrList, {id: cours.nuberOfSemesters.numberSemesters});
                console.log(selected+' obiekt selected');
                return selected.length ? selected[0].numberSemesters : 'Brak';
            } else return 'Brak'
        };
        $scope.showStatus = function(cours) {
            var selected = [];
            // if(user.status) {
            //     selected = $filter('filter')($scope.statuses, {value: user.status});
            // }
            // return selected.length ? selected[0].text : 'Not set';
        };


        $scope.removeUser = function(index) {
            $scope.coursesOfStudy.splice(index, 1);
        };
        $scope.addConsole = function () {
            alert("sortuje");
        }
        $scope.addCoursesOfStudy = function() {
            $state.go('configuration.addCourses');
        };
        $scope.editCourse = function (coursesOfStudy) {
            $state.go('configuration.addCourses',{id:coursesOfStudy.id});
        }
        editableOptions.theme = 'bs3';
        editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary editable-table-button btn-xs">Zapisz</button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon">Anuluj</button>';
    }
})();
