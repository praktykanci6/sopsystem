(function () {
    'use strict';

    angular.module('BlurAdmin.pages.newAcademicYearWizard')
            .controller('NewAcademicYearWizardCtrl', NewAcademicYearWizardCtrl);

    /** @ngInject */
    function NewAcademicYearWizardCtrl($scope, $http, $filter) {
        var vm = this;

        
        vm.tabs = [];

        vm.tabNum = 1;
        vm.progress = 0;
        //
        // vm.addTab = function (tab) {
        //     tab.setPrev(vm.tabs[vm.tabs.length - 1]);
        //     vm.tabs.push(tab);
        //     vm.selectTab(0);
        // };

        $scope.academicYear = {};
        $scope.academicYearStudyField = {};

        $scope.studyFields = [];
        $scope.selectedStudyFields = [];

        $scope.getStudyFields = function () {
            $http.get('/coursesOfStudy').success(function (data) {
                $scope.studyFields = data._embedded.coursesOfStudies;
            })
        };
        $scope.getStudyFields();
        // $scope.$watch(angular.bind(vm, function () {
        //     return vm.tabNum;
        // }), calcProgress);
        // console.log(vm.tabs.length)
        // vm.selectTab = function (tabNum) {
        //     console.log('submit tab',tabNum)
        //     vm.tabs[vm.tabNum].submit();
        //     if (vm.tabs[tabNum].isAvailiable()) {
        //         vm.tabNum = tabNum;
        //         vm.tabs.forEach(function (t, tIndex) {
        //             tIndex == vm.tabNum ? t.select(true) : t.select(false);
        //         });
        //     }
        // };
        //
        // vm.isFirstTab = function () {
        //     return vm.tabNum == 0;
        // };
        //
        // vm.isLastTab = function () {
        //     return vm.tabNum == vm.tabs.length - 1;
        // };
        //
        //     $scope.nextTab= function () {
        //     consloe.log('działa na innym kontrolerze');
        //     if(vm.isLastTab()){
        //         $http({
        //             url: '/save/academicYear',
        //             method: 'POST',
        //             data: vm.academicYear
        //         }).success(function(response){
        //
        //     })
        //     } else {
        //     vm.selectTab(vm.tabNum + 1)}
        // };
        //
        // vm.previousTab = function () {
        //     vm.selectTab(vm.tabNum - 1)
        // };
        //
        // function calcProgress() {
        //     vm.progress = ((vm.tabNum + 1) / vm.tabs.length) * 100;
        // }
        $scope.saveAcademicYear = function () {
            $http.post('/addAcademicYear',{
                data : angular.toJson(data)
            })
                .success(function (data) {
                   $state.go('academicYear.list')

                })
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.selectedItem = function (item) {
            if($scope.selectedStudyFields.indexOf(item.id) === -1){
                $scope.selectedStudyFields.push(item.id);
            } else {
                $scope.selectedStudyFields.splice($scope.selectedStudyFields.indexOf(item.id),1);
            }
        };

        var data = {
            academicYear: $scope.academicYear,
            coursesOfStudy : $scope.selectedStudyFields
        };

        $scope.selectedCoursesOfStudy = function(){
            return $filter('filter')($scope.studyFields,{checked : true});
        }

    }
})();

