/**
 * Created by tifet on 2017-06-08.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.academicYear')
        .controller('StudentDetailsCtrl', StudentDetailsCtrl);

    /** @ngInject */
    function StudentDetailsCtrl($scope, $rootScope, $http) {
        $scope.student=$rootScope.studentCourse;
        console.log('studentkierunku',$scope.student);
        $http.get('/studentCoursesDetail?id='+$scope.student.numberAlbum+'&yearbookId='+$rootScope.courses.id)
            .success(function (data) {
                console.log('student',data);
            })
    }

})();
