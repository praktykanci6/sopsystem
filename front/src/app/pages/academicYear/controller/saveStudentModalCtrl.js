/**
 * Created by tifet on 2017-04-24.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.academicYear')
        .controller('SaveStudentModalCtrl', SaveStudentModalCtrl);

    /** @ngInject */
    function SaveStudentModalCtrl($scope, $uibModalInstance) {
        $scope.link = true;
        $scope.ok = function () {
            $uibModalInstance.close($scope.link);
        };
    }

})();