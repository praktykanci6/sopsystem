/**
 * Created by tifet on 2017-04-24.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.academicYear')
        .controller('ImportStudentsModalCtrl', ImportStudentsModalCtrl);

    /** @ngInject */
    function ImportStudentsModalCtrl($rootScope,$scope,$http, $uibModalInstance) {
        $scope.file;
        $scope.fileName = 'Nie wybrano pliku';
        $scope.ok = function () {
            saveStudents()

        };

        $scope.fileNameChanged = function (ele) {
            var files = ele.files;
            var l = files.length;
            console.log('plik dane rozmiar ', files[0]);
            $scope.file = files[0];
            $scope.fileName =  ele.files[0].name;
            // var namesArr = [];
            //
            // for (var i = 0; i < l; i++) {
            //     namesArr.push(files[i].name);
            //     console.log(files[i].name);
            // }
            // $scope.getFile()
            $scope.$apply();
        };

        $scope.uploadPicture = function () {
            var fileInput = document.getElementById('uploadFile');
            fileInput.click();
        };

        var saveStudents = function(){
            var fd = new FormData();            
            fd.append('id',$rootScope.courses.id);
            fd.append('file',$scope.file);
            $http.post('/importStudentsXls',fd,{
                transformRequest : angular.identity,
                headers : {
                    'Content-Type' : undefined
                }
            })
                .success(function (data) {                    
                    $scope.students = data;
                    $uibModalInstance.close($scope.students);
                })
                .error(function(data){
                    alert('błąd zapisu');
                })
        }
    }

})();