/**
 * Created by tifet on 2017-05-02.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.academicYear')
        .controller('ChooseCoordinatorCtrl', ChooseCoordinatorCtrl);

    /** @ngInject */
    function ChooseCoordinatorCtrl($scope, $http, $filter, $rootScope, $state) {

        $scope.coordinatorsCourse=[];
        var getCoordinators = function () {
            $http.get('/coordinatorLis?id='+$rootScope.courses.id)
                .success(function (data) {
                    $scope.coordinatorsCourse = data;
                })
                .error(function (data) {
                    console.log(data.message);
                })
        };
        getCoordinators();
        $scope.selectedCoordinators = [];

        $scope.selectedItem = function (item) {
            if($scope.selectedCoordinators.indexOf(item.id) === -1){
                $scope.selectedCoordinators.push(item.id);
            } else {
                $scope.selectedCoordinators.splice($scope.selectedCoordinators.indexOf(item.id),1);
            }
        };
        var data = {
            academicYear: $scope.academicYear,
            coursesOfStudy : $scope.selectedStudyFields
        };

        $scope.selectedCoursesOfStudy = function(){
            return $filter('filter')($scope.coordinatorsCourse,{checked : true});
        };

        $scope.addCoordinators = function () {
            $http.post('/addCoordinators',$scope.selectedCoordinators)
                .success(function(data){
                    $state.go('academicYear.yearbooks');
                })
                .error(function (data) {
                    alert(data.message);
                });
        };

        $scope.cancel = function () {
            $state.go('academicYear.yearbooks');
        };

        $scope.addNewCoordinator = function () {
            $state.go('academicYear.addNewCoordinator');
        };
    }
})();

