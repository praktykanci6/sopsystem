/**
 * Created by tifet on 2017-04-08.
 */
(function () {
    'use strict'
    angular.module('BlurAdmin.pages.academicYear')
        .controller('AcademicYearCtrl',AcademicYearCtrl)
    function AcademicYearCtrl($scope,$state,$http,$rootScope) {
        $scope.smartTablePageSize = 50;
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $http.get('/academicYear')
            .success(function (data) {
                $scope.rowCollection =[];
                $scope.rowCollection = data._embedded.academicYears;
            });

        var year={
            id:null,
            name:'',
            start:'',
            end:''
        };
        $scope.displayedCollection=[].concat($scope.rowCollection);
        $scope.displayedCollection.push(year);
        $scope.addNewYear = function () {
            $state.go('academicYear.newYear');
        };

        $scope.searchAcademicYear = function (searcText) {
            alert('brak implementacji');
        };
        $scope.yearDetails = function (row) {
            $rootScope.yearId = row.id;
            $state.go('academicYear.yearbooks',{id: row.id});
        };
        console.log($scope.rowCollection,'row colection');
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }
})();
