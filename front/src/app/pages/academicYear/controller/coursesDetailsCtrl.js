/**
 * Created by dawid-ss on 2017-04-20.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.academicYear')
        .controller('CoursesDetailsCtrl',CoursesDetailsCtrl);
    function CoursesDetailsCtrl($scope,$http,$uibModal,$state,$stateParams,$timeout,fileReader,$filter,$rootScope) {
        $scope.isEditable = false;
        $scope.isDisabled = true;
        $scope.smartTablePageSize = 50;
        $scope.courses = $rootScope.courses;
        $scope.practiceCourses={};
        $scope.coordinator ={};
        $http.get("/yearbookDeatils?id="+$scope.courses.id)
            .success(function (data) {
                $scope.practiceCourses =data;
                $scope.yearbook = {
                    id : $scope.courses.id,
                    academicYearId : null,
                    coursesOfStudy : null,
                    nameCourses : $scope.courses.courseOfStudy.nameCourses,
                    specilaization : $scope.courses.courseOfStudy.specilaization,
                    startInterviews : startInterviews,
                    endInterviews : endInterviews,
                    startChoice :  startChoice,
                    endChoice : endChoice,
                    startPractice :  startPractice,
                    endPractice : endPractice,
                    hour: $scope.practiceCourses.hour,
                    day:  $scope.practiceCourses.day

                };
            });
        $http.get('/coordinatorCourseList?id='+$rootScope.courses.id)
            .success(function (data) {
                $scope.coordinator = data;
            });
        if($scope.courses.startInterviews === null){
           var  startInterviews =  null;
        } else{
            var  startInterviews =  new Date($scope.courses.startInterviews)
        }
        if($scope.courses.endInterviews === null){
            var endInterviews = null;
        }   else{
            var endInterviews = new Date($scope.courses.endInterviews);
        }
        if($scope.courses.startChoice === null){
            var startChoice = null;
        }   else {
            var startChoice =  new Date($scope.courses.startChoice);
        }
        if($scope.courses.endChoice === null){
            var endChoice = null;
        }   else{
            var endChoice = new Date($scope.courses.endChoice);
        }
        if($scope.courses.startPractice === null){
            var startPractice = null;
        }   else{
            var startPractice =  new Date($scope.courses.startPractice);
        }
        if($scope.courses.endPractice === null){
            var endPractice = null;
        }   else{
            var endPractice = new Date($scope.courses.endPractice)
        }
        $scope.yearbook = {
            id : $scope.courses.id,
            academicYearId : null,
            coursesOfStudy : null,
            nameCourses : $scope.courses.courseOfStudy.nameCourses,
            specilaization : $scope.courses.courseOfStudy.specilaization,
            startInterviews : startInterviews,
            endInterviews : endInterviews,
            startChoice :  startChoice,
            endChoice : endChoice,
            startPractice :  startPractice,
            endPractice : endPractice,
            hour: $scope.practiceCourses.hour,
            day:  $scope.practiceCourses.day

        };
        console.log('kierunek',$scope.courses);
        $scope.edit = function () {
            if($scope.isEditable){
                $scope.isEditable = false;
                $scope.isDisabled = true;
            } else {
                $scope.isEditable = true;
                $scope.isDisabled = false;
            }
        }

        //tabs studenci
        $scope.myFile ={};
        $scope.myFiles ={};
        //flaga okreslajaca czy chcemy importowac studentów
        var s = false;
        $scope.students = {};
        $scope.saveDetails = function () {

            var yerbook = {
                id : $scope.yearbook.id,
                academicYearId : null,
                coursesOfStudy : null,
                startInterviews : $scope.yearbook.startInterviews,
                endInterviews :$scope.yearbook.endInterviews,
                startChoice : $scope.yearbook.startChoice,
                endChoice : $scope.yearbook.endChoice,
                startPractice : $scope.yearbook.startPractice,
                endPractice : $scope.yearbook.endPractice,
                hour: $scope.practiceCourses.hour,
                day:  $scope.practiceCourses.day
            };
            $http.post('/yearbookUpdate',yerbook)
                .success(function (data) {
                    console.log('zapisano yearbook',data);
                    $scope.isEditable = false;
                    $scope.isDisabled = true;

                })
                .error(function (data) {
                    console.log(data.message);

                });

            $scope.isEditable = false;
            $scope.isDisabled = false;
        }

        var id = $stateParams.id;
        // $scope.fileNameChanged = function (ele) {
        //     var files = ele.files;
        //     var l = files.length;
        //     // console.log('plik dane rozmiar ', files[0]);
        //     $scope.myFile = files[0]
        //     // var namesArr = [];
        //     //
        //     // for (var i = 0; i < l; i++) {
        //     //     namesArr.push(files[i].name);
        //     //     console.log(files[i].name);
        //     // }
        //     // $scope.getFile()
        //     $scope.showModal(s);
        //         console.log('resretuje plik');
        //         ele.files = null;
        //
        // }

        $scope.getStudents = function(){
            $http.get('/studentsCourses?id='+$rootScope.courses.id)
                .success(function (data) {
                    $scope.rowCollection =[];
                    $scope.rowCollection = data;
                })
        };

        $scope.getStudents();
        var year={
            numberAlbum:null,
            person:null
        }
        $scope.displayedCollection=[].concat($scope.rowCollection);
        $scope.displayedCollection.push(year);
        $scope.uploadPicture = function () {
            var fileInput = document.getElementById('uploadFile');
            fileInput.click();
        };

        $scope.showModal = function (item) {
            $uibModal.open({
                animation: false,
                controller: 'ImportStudentsModalCtrl',
                templateUrl: 'app/pages/academicYear/pages/widget/importStudentModal.html'
            }).result.then(function (file) {
                 $scope.myFile = file;
                  $scope.getStudents();
            });
        };
        $scope.importStudents = function () {
            $scope.showModal($scope.myFile)
        };

        $scope.saveYearbook = function () {
            var yerbook = {
                id : $scope.courses.id,
                academicYearId : null,
                coursesOfStudy : null,
                startInterviews : $scope.courses.startInterviews,
                endInterviews :$scope.courses.endInterviews,
                startChoice : $scope.courses.startChoice,
                endChoice : $scope.courses.endChoice,
                startPractice : $scope.courses.startPractice,
                endPractice : $scope.courses.endPractice

            };
            $http.post('/saveYearbook',yerbook)
                .success(function (data) {
                    console.log('zapisano yearbook',data);
                    $scope.isEditable = false;
                    $scope.isDisabled = true;

                })
                .error(function (data) {
                    console.log(data.message);

                });
        };

        $scope.displayedCollection = [].concat($scope.rowCollection);

        $scope.getNewCoordinator = function () {
            $state.go('academicYear.addNewCoordinator');
        };
        $scope.studentCourseDetail = function (item) {
            $rootScope.studentCourse = item;
            $state.go('academicYear.studentDetails');
        }
    }
})();