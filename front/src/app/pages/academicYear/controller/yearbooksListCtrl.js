/**
 * Created by tifet on 2017-04-09.
 */
/**
 * Created by tifet on 2017-04-08.
 */
(function () {
    'use strict'
    angular.module('BlurAdmin.pages.academicYear')
        .controller('YearbooksListCtrl',YearbooksListCtrl)
    function YearbooksListCtrl($scope,$state,$http,$stateParams,pageIdService,$rootScope) {
        $scope.smartTablePageSize = 50;
        console.log('id rocznika',$rootScope.yearId);
        var acdemicYearId = $rootScope.yearId;
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $http.get('/yearbooks/search/findByAcademicYearId?id='+acdemicYearId)
            .success(function (data) {
                $scope.rowCollection =[];
                $scope.rowCollection = data._embedded.yearbooks;
                console.log(data);
            });


        $scope.displayedCollection=[].concat($scope.rowCollection);
        $scope.addNewYear = function () {
            $state.go('academicYear.newYear');
        }

        $scope.searchAcademicYear = function (searcText) {
            alert('brak implementacji');
        }
        $scope.coursesDetails = function (item) {
            console.log(item);
            $rootScope.courses = item;
            // pageIdService.addData(item.coursesOfStudy.id);
            $state.go('academicYear.coursesDetails');
        }
        console.log($scope.rowCollection,'row colection');
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }
})();
