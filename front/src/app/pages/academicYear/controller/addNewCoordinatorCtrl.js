/**
 * Created by tifet on 2017-05-02.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.academicYear')
        .controller('AddNewCoordinatorCtrl', AddNewCoordinatorCtrl);

    /** @ngInject */
    function AddNewCoordinatorCtrl($scope, $http, $filter, $rootScope, $state) {
        $scope.showTabs1 = true;
        $scope.showTabs2 = false;
        $scope.employers = [];
        $scope.selectedEmployers =[];
        $scope.user={};
        var getEmployers = function () {
            $http.get('/employers?id='+$rootScope.courses.id)
                .success(function (data) {
                    $scope.employers = data;
                    console.log('pobrano liste praktykodawców',data);
                })
        };
        getEmployers();
        $scope.addCoordinator = function () {
            var data = {
                user : $scope.user,
                yearbookId : $rootScope.courses.id,
                employerIds: $scope.selectedEmployers
            };
            $http.post('/addCoordinatorCourse', data )
                .success(function(data){
                    $state.go('academicYear.yearbooks');
                })
                .error(function (data) {
                    alert(data.message);
                });
        };

        $scope.cancel = function () {
            $state.go('academicYear.yearbooks');
        };

        $scope.showTab1 = function () {
            $scope.showTabs2 = false;
            $scope.showTabs1 = true;

        };
        $scope.showTab2 = function () {

              $scope.showTabs2 = true;
              $scope.showTabs1 = false;
        };
        $scope.cancel = function () {
            $state.go('academicYear.coursesDetails');
        };
        $scope.selectedItem = function (item) {
            if($scope.selectedEmployers.indexOf(item.id) === -1){
                $scope.selectedEmployers.push(item.id);
            } else {
                $scope.selectedEmployers.splice($scope.selectedEmployers.indexOf(item.id),1);
            }
        };
        var data = {
            academicYear: $scope.academicYear,
            coursesOfStudy : $scope.selectedStudyFields
        };

        $scope.selectedCoursesOfStudy = function(){
            return $filter('filter')($scope.employers,{checked : true});
        };

    }
})();

