/**
 * Created by tifet on 2017-04-08.
 */
/**
 * Created by tifet on 2017-04-01.
 */
(function(){
    'use strict';

    angular.module('BlurAdmin.pages.academicYear',[])
        .config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('academicYear',{
                url:'/lata',
                template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
                abstract: true,
                // controller: 'ConfigurationPagesCtrl',
                title: 'Roczniki',
                sidebarMeta: {
                    icon: 'ion-calendar',
                    order:50,
                },
                role:'ADMINISTRATOR'
            })
            .state('academicYear.list',{
                url:  '/rocznikiStudiow',
                templateUrl: 'app/pages/academicYear/academicYearList.html',
                controller: 'AcademicYearCtrl',
                title:'Lata akademickie',
                sidebarMeta: {
                    order:0,
                },
                role:'ADMINISTRATOR'
            })
            .state('academicYear.newYear',{
                url:'/nowyRokAkademicki',
                templateUrl:'app/pages/academicYear/newAcademicYearWizard.html',
                controller:'NewAcademicYearWizardCtrl',
                controllerAs: 'vm',
                title:'Dodaj nowy rok',
                sidebarMeta:{
                    order : 50
                },
                role:'ADMINISTRATOR'
            })
            .state('academicYear.yearbooks',{
                url:'/kierunkiRoku',
                params : {
                    id : null
                },
                templateUrl:'app/pages/academicYear/yearbooksList.html',
                controller:'YearbooksListCtrl',
                title:'Szcególy rocznika',
                // sidebarMeta:{
                //     order : 50
                // }
            })
            .state('academicYear.coursesDetails',{
                url:'/szczegulyKierunku',
                params : {
                    id : null
                },
                templateUrl:'app/pages/academicYear/coursesDetails.html',
                controller:'CoursesDetailsCtrl',
                title:'Szcególy kierunku',

            })
            .state('academicYear.choosCoordinator',{
                url:'/wybierzKoordynatora',
                templateUrl: 'app/pages/academicYear/pages/chooseCoordinator.html',
                controller : 'ChooseCoordinatorCtrl',
                title : 'Wybierz koordynatora'
            })
            .state('academicYear.addNewCoordinator',{
            url:'/dodajKoordynatora',
            templateUrl: 'app/pages/academicYear/pages/addNewCoordinator.html',
            controller : 'AddNewCoordinatorCtrl',
            title : 'Dodaj koordynatora'
        })
            .state('academicYear.studentDetails',{
                url:'/studentSzczegoly',
                templateUrl: 'app/pages/academicYear/pages/studentDetails.html',
                controller : 'StudentDetailsCtrl',
                title : 'Szczegóły studenta kierunku'
            });
        $urlRouterProvider.when('/lata/','lata/rocznikiStudiow','/lata/nowyRokAkademicki',
            '/lata/kierunkiRoku','/lata/szczegulyKierunku'
        );
    }
})();
