/**
 * Created by tifet on 2017-04-01.
 */
(function(){
    'use strict';

    angular.module('BlurAdmin.pages.employer',[])
        .config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('employer',{
                url:  '/praktykodawcy',
                template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',

                abstract: true,
                title: 'Praktykodawcy',
                sidebarMeta: {
                    icon: 'ion-briefcase',
                    order:100,
                },
                role:'ADMINISTRATOR'
            })
            .state('employer.employerTabs',{
                url:'/praktykodawcaSzczegoly',
                templateUrl: 'app/pages/employer/pages/employer.html',
                controller: 'EmployerCtrl',
                params :{
                    id : null
                },
                 title:'Szczegóły',
                // sidebarMeta:{
                //     order:0,
                // },
            })
            .state('employer.addEmployer',
                {
                    url:'/dodajPraktykodawce',
                    templateUrl:'app/pages/employer/pages/addEmployer.html',
                    title:'Dodaj praktykodawcę',
                    controller:'AddEmployerCtr',
                    sidebarMeta:{
                        order:50,
                    },
                    role:'ADMINISTRATOR'

                })
            .state('employer.employerList',
                {
                    url:'/praktykodawcy',
                    templateUrl:'app/pages/employer/pages/employerList.html',
                    title:'Praktykodawcy',
                    controller:'EmployerListCtrl',
                    sidebarMeta:{
                        order:0,
                    },
                    role:'ADMINISTRATOR'

                })
            .state('employer.addCoordinator',
                {
                    url:'/dodajKoordynatora',
                    templateUrl:'app/pages/employer/pages/addCoordinator.html',
                    title:'Dodaj kordynatora',
                    controller:'AddCoordinatorCtrl',
                    // params:{
                    //     id : pageIdService.getData()
                    // }
                })
            .state('employer.addPractice', {
                url: '/dodajPraktyke',
                templateUrl: 'app/pages/employer/pages/addPractice.html',
                controller: 'AddPracticeCtrl',
                title: 'Dodaj Praktyki',
                params: {
                    employerId : null,
                },
                role:'PRAKTYKODAWCA'
            })
            .state('employer.interviewList',{
            url:'/rozmowyKwalifikacyjne',
            templateUrl: 'app/pages/employer/pages/interviewList.html',
            controller: 'InterviewListCtrl',
            title: 'Zgłoszenia na rozmowy',
            role:'PRAKTYKODAWCA'
        })
            .state('employer.editPractice',{
                url:'/edytujPraktyke',
                templateUrl: 'app/pages/employer/pages/addPractice.html',
                controller: 'EditPracticeCtrl',
                title: 'Edytuj praktyke',
                role:'PRAKTYKODAWCA'
            });
        $stateProvider
            .state('employers', {
                url: '/startPage',
                templateUrl: 'app/pages/employer/pages/dashboard.html',
                controller: 'DashboardCtrl',
                title: 'Strona Główna',
                sidebarMeta: {
                    icon: 'ion-android-home',
                    order: 0,
                },
                role:'PRAKTYKODAWCA'
            });
    }
})();
