/**
 * Created by tifet on 2017-03-21.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.employer')
            .controller('AddEmployerCtr', AddEmployerCtr)
    function AddEmployerCtr($scope, $http, $state, toastr, toastrConfig) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.tabs1 = true;
        $scope.tabs2 = false;
        $scope.error = '';
        $scope.coordinator = {};
        $scope.studyFields = [];
        $scope.selectedStudyFields = [];
        var re = '';
        var re2 = '';
        var req = '';
        var errorCheck = false;
        var error = '';
        var errorList = ['Pole Nazwa skrócona jest wymagane <br />',
            'Pole Nazwa jest wymagane <br />',
            'Pole Ulica jest wymagane <br />',
            'Pole Numer budynku jest wymagane <br />',
            'Pole Miejscowość jest wymagane <br />',
            'Pole Kod pocztowy jest wymagane <br />',
            'Pole Kraj jest wymagane <br />',
            'Pole Kod pocztowy: niepoprawny format <br />',
        ];
        var errorList2 = ['Pole Login jest wymagane <br />',
            'Pole Imię jest wymagane <br />',
            'Pole Nazwisko jest wymagane <br />',
            'Pole Stanowisko jest wymagane <br />',
            'Pole Hasło jest wymagane <br />',
            'Pole Powtórz hasło jest wymagane <br />',
            'Pole Email jest wymagane <br />',
            'Pole Email: niepoprawny format <br />',
            'Hasła się nie zgadzają <br />'

        ];
        var postCodePattern = /^[0-9]{2}-[0-9]{3}$/;
        var emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //model address
        $scope.employer = {
            name: '',
            fullName: '',
            description: '',
            addresses: []
        };
        var getListCourses = function () {
            $http.get('/yearbook/coursesList')
                    .success(function (data) {
                        $scope.studyFields = data;
                    })
                    .error(function (data) {
                        alert(data.message);
                    })
        };
        getListCourses();
        var address = {
            departament: false,
            street: '',
            numberBuilding: '',
            city: '',
            postCode: '',
            country: '',
            phone1: '',
            phone2: '',
            fax: ''
        };
        $scope.employer.addresses.push(address);
        //dodaje address odziału
        $scope.addDepartament = function () {
            var address1 = {
                departament: true,
                street: '',
                numberBuilding: '',
                city: '',
                postCode: '',
                country: '',
                phone1: '',
                phone2: '',
                fax: ''
            };
            $scope.employer.addresses.push(address1);
        };
        $scope.saveAddress = function () {
            if (validate()) {
                toastr.error(re, 'Błąd',
                        {
                            "autoDismiss": false,
                            "positionClass": "toast-top-full-width",
                            "type": "error",
                            "timeOut": "10000",
                            "extendedTimeOut": "2000",
                            "allowHtml": true,
                            "closeButton": true,
                            "tapToDismiss": false,
                            "progressBar": false,
                            "newestOnTop": true,
                            "maxOpened": 0,
                            "preventDuplicates": false,
                            "preventOpenDuplicates": false
                        })
            } else {
                $scope.tabs1 = false;
                $scope.tabs2 = true;
            }

        };
        $scope.cancel = function () {
            $state.go('employer.employerList');
        };
        $scope.back = function () {
            $scope.tabs1 = true;
            $scope.tabs2 = false;
        };
        $scope.selectedItem = function (item) {
            if ($scope.selectedStudyFields.indexOf(item.id) === -1) {
                $scope.selectedStudyFields.push(item.id);
            } else {
                $scope.selectedStudyFields.splice($scope.selectedStudyFields.indexOf(item.id), 1);
            }
        };
        $scope.selectedCoursesOfStudy = function () {
            return $filter('filter')($scope.studyFields, {checked: true});
        };
        $scope.saveWizard = function () {
            if (validateWizard()) {
                toastr.error(re2, 'Błąd',
                        {
                            "autoDismiss": false,
                            "positionClass": "toast-top-full-width",
                            "type": "error",
                            "timeOut": "10000",
                            "extendedTimeOut": "2000",
                            "allowHtml": true,
                            "closeButton": true,
                            "tapToDismiss": false,
                            "progressBar": false,
                            "newestOnTop": true,
                            "maxOpened": 0,
                            "preventDuplicates": false,
                            "preventOpenDuplicates": false
                        })
            } else {
                var data = {
                    employerData: $scope.employer,
                    coordinator: $scope.coordinator,
                    courses: $scope.selectedStudyFields
                };
                $http.post('/addEmployer', {
                    data: angular.toJson(data)
                })
                        .success(function (data) {
                            $state.go('employer.employerList');
                        })
                        .error(function (data) {
                            alert(data.message);
                        })
            }

        }

        var validate = function () {
            re2 = '';
            error = '';
            errorCheck = false;
            if ($scope.employer.name === undefined || $scope.employer.name === '') {
                error += errorList[0];
                errorCheck = true;
            }
            if ($scope.employer.fullName === undefined || $scope.employer.fullName === '') {
                error += errorList[1];
                errorCheck = true;
            }            
            for (var i = 0; i < $scope.employer.addresses.length; i++) {                
                if ($scope.employer.addresses[i].street === undefined || $scope.employer.addresses[i].street === '') {
                    error += errorList[2];
                    errorCheck = true;
                }
                if ($scope.employer.addresses[i].numberBuilding === undefined || $scope.employer.addresses[i].numberBuilding === '') {
                    error += errorList[3];
                    errorCheck = true;
                }
                if ($scope.employer.addresses[i].city === undefined || $scope.employer.addresses[i].city === '') {
                    error += errorList[4];
                    errorCheck = true;
                }
                if ($scope.employer.addresses[i].postCode === undefined || $scope.employer.addresses[i].postCode === '') {
                    error += errorList[5];
                    errorCheck = true;
                } else if (!postCodePattern.test($scope.employer.addresses[i].postCode)) {
                    error += errorList[7];
                    errorCheck = true;
                }
                if ($scope.employer.addresses[i].country === undefined || $scope.employer.addresses[i].country === '') {
                    error += errorList[6];
                    errorCheck = true;
                }
            }
            re = error + req;
            return errorCheck;

        }

        var validateWizard = function () {
            re2 = '';
            error = '';
            errorCheck = false;
            
            if ($scope.coordinator.login === undefined || $scope.coordinator.login === '') {
                error += errorList2[0];
                errorCheck = true;
            }
            if ($scope.coordinator.firstName === undefined || $scope.coordinator.firstName === '') {
                error += errorList2[1];
                errorCheck = true;
            }
            if ($scope.coordinator.lastName === undefined || $scope.coordinator.lastName === '') {
                error += errorList2[2];
                errorCheck = true;
            }
            if ($scope.coordinator.position === undefined || $scope.coordinator.position === '') {
                error += errorList2[3];
                errorCheck = true;
            }
            if ($scope.coordinator.password === undefined || $scope.coordinator.password === '') {
                error += errorList2[4];
                errorCheck = true;
            }
            if ($scope.coordinator.confirmPassword === undefined || $scope.coordinator.confirmPassword === '') {
                error += errorList2[5];
                errorCheck = true;
            } else if (!($scope.coordinator.password === $scope.coordinator.confirmPassword)) {
                error += errorList2[8];
                errorCheck = true;
            }
            if ($scope.coordinator.email === undefined || $scope.coordinator.email === '') {
                error += errorList2[6];
                errorCheck = true;
            } else if (!emailPattern.test($scope.coordinator.email)) {
                error += errorList2[7];
                errorCheck = true;
            }
            re2 = error + req;
            return errorCheck;
        }
    }


})();
