/**
 * Created by tifet on 2017-05-04.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.employer')
        .controller('DashboardCtrl',DashboardCtrl)
    /** @ngInject */
    function DashboardCtrl($scope,$http,$state,$filter,employerService,$rootScope) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.data = [];

        $scope.addPractice = function (item) {
            $rootScope.yearbookId = item.yearbook.id;
             $state.go('employer.addPractice')

        };
        $http.get('getDashboardData')
            .success(function (data) {
                $scope.data = data;
                $rootScope.employerId = $scope.data[0].employerId;
                console.log("employer data",data);
            })
            .error(function (data) {
                console.log(data.message);
            });
        $scope.getInterview = function (item) {
            $rootScope.employerId = item.employerId;
            $rootScope.yearbookId = item.yearbookId;
            $state.go('employer.interviewList')
            // $http.get('/practiceInterview?id=' + item.employerId + '&yearbookId=' + item.yearbookId)
            //     .success(function (data) {
            //         console.log('return data', data);
            //
            //     })
        }

    }
})();