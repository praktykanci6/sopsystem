/**
 * Created by tifet on 2017-04-01.
 * Szcególy praktykodawcy
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.employer')
        .controller('EmployerCtrl',EmployerCtrl)
    function EmployerCtrl($scope,$http,$state,$stateParams,$timeout,fileReader,$filter,$rootScope){
        $scope.assistant=[];
        var id = $rootScope.employer.id;
        $http.defaults.headers.post["Content-Type"] = "application/json";
        //tabs dane praktykodawcy
        $http.get('/employer/'+id).success(function (data) {
            $scope.employer = data;
        });
        $http.get('/employerAddresses/search/getByEmployerId?id='+id)
            .success(function(data1) {
                $scope.addresses = data1._embedded.employAddresses;
            });
        var tab = 1;

        //pobieranie opiekunów praktych praktykodawców
        $http.get('/assistantCourseByEmployer?id='+id)
            .success(function (data) {
                $scope.assistant = data;
            });
        var getEmployerPractice = function () {
            $http.get('/practicesEmployer?id=' + $rootScope.employer.id)
                .success(function (data) {
                    $scope.practice = data;
                })
                .error(function (data) {
                    console.log(data.message);
                });
        };
        getEmployerPractice();

        $scope.setTab = function (tabId) {
            tab = tabId;
        };
        $scope.isSet = function (tabId) {
            return tab === tabId;
        };
        $scope.form={name:''};
        $scope.$watch('form.name', function(newVal, oldVal){
            console.log('changed :', newVal);

        });

        //inicjalizacja map google
        function initialize() {
            var mapCanvas = document.getElementById('google-maps');
            var mapOptions = {
                center: new google.maps.LatLng( 50.014688, 20.92079),
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(mapCanvas, mapOptions);
            var CentralPark = new google.maps.LatLng(50.014688, 20.92079);
            new google.maps.Marker({
                position: CentralPark,
                title:'Tarnów',
                map: map
            });
        }
        $timeout(function(){
            initialize();
        }, 100);

        //tabs koordynatorzy
        $scope.coordinator = undefined;
        // console.dir($scope.file);
        $scope.addCoordinator = function () {
            $state.go('employer.addCoordinator');
        };

        $scope.editPractice = function (item) {
            $$rootScope.practice = item;
            $state.go('employer.editPractice');
        }
    }
})();