/**
 * Created by dawid-ss on 2017-03-26.
 *
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.employer')
        .controller('EmployerListCtrl',EmployerListCtrl)
    /** @ngInject */
    function EmployerListCtrl($rootScope,$scope,$http,$state,$filter,employerService) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.smartTablePageSize = 100;
        $scope.searchText="";
        $http.get('/employer?size=10000')
        .success(function (data){
        $scope.rowCollection =[];
        $scope.rowCollection = data._embedded.employers;
                });
        var employ = {
            name:'alibaba',
            fullName:'ali',
            description:'opis'
        };

        $scope.displayedCollection=[].concat($scope.rowCollection);
        $scope.displayedCollection.push(employ);
        $scope.searchEmployer = function(searchText){
            $http.get("/employer/search/fullTsxtSearch?fragments="+searchText)
                .success(function (data) {
                    $scope.rowCollection = data._embedded.employers;
                    console.log(data);
             })
        };
        $scope.displayedCollection=[].concat($scope.rowCollection);

        $scope.employerDetail = function (item) {
            $rootScope.employer=item;
            console.log
            $state.go('employer.employerTabs',{id:item.id})
        }
    }
})();
