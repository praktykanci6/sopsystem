/**
 * Created by tifet on 2017-05-04.
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.employer')
        .controller('AddPracticeCtrl',AddPracticeCtrl)
    /** @ngInject */
    function AddPracticeCtrl($scope,$http,$state,$rootScope,$location) {
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.addresses=[];
        $scope.practice={};
        $scope.saveCoordinator = function () {
            alert('submit form');
        };
        $http.get('/employerAddress?id='+$rootScope.employerId)
            .success(function(data1) {
                $scope.addresses = data1;
                console.log('employerAddress : ',$scope.addresses)
            })
            .error(function (data) {
                console.log(data.message);
            });
        $scope.cancel12 = function () {
            console.log('wolam pat startPage');
            $location.path('/startPage');
        };
        $scope.addPractice = function () {
            console.log('yearbook',$rootScope.yearbookId);
            console.log('dane z widoku',$scope.practice);
            var practice={
                addressId:$scope.practice.address.id,
                yearbookId: $rootScope.yearbookId,
                star :$scope.practice.star,
                end:$scope.practice.end,
                interview:$scope.practice.interview,
                counter:$scope.practice.counter
            };
            console.log('praktyka dane :',practice);
            $http.post('/addPractice',practice)
                .success(function (data) {

                })
                .error(function (data) {
                    alert(data.message);
                })
        }
    }
})();