/**
 * Created by tifet on 2017-06-04.
 */
/**
 * Created by tifet on 2017-04-01.
 * Szcególy praktykodawcy
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.employer')
        .controller('InterviewListCtrl',InterviewListCtrl)
    function InterviewListCtrl($scope,$http,toastr,$rootScope){
        var employerId = $rootScope.employerId;
        var yearbookId = $rootScope.yearbookId;
        var copyRow;
        $scope.statuses=[
            {id:1,name : 'Zaliczona',property:'PASSED'},
            {id:2,name : 'Nie zaliczona',property:'FAILED'}];
        $scope.selectedStatus='';
        $scope.items=[];

        $http.get('/practiceInterview?id=' + employerId + '&yearbookId=' + yearbookId)
            .success(function (data) {
                console.log('return data', data);
                $scope.items = data;
                angular.forEach($scope.items, function(value, key){
                    if(value.dataInterview!= undefined) {
                        console.log('date',value.dataInterview);
                        value.dataInterview = new Date(value.dataInterview);
                        console.log("ustawianie daty")
                    }
                        value.isEditable = false;

                });
            });


        $scope.edit = function (item,index) {
            copyRow = angular.copy($scope.items[index]);
            item.isEditable = true;
        };
        $scope.cancel = function (item,index) {
            $scope.items[index] = copyRow;
            item.isEditable = false;
        };
        $scope.save = function (item,index) {
           $http.post("/interviewSave",item)
               .success(function (data) {
                   toastr.success(data.notification , 'Info',
                       {
                           "autoDismiss": false,
                           "positionClass": "toast-top-full-width",
                           "type": "success",
                           "timeOut": "10000",
                           "extendedTimeOut": "2000",
                           "allowHtml": false,
                           "closeButton": true,
                           "tapToDismiss": false,
                           "progressBar": false,
                           "newestOnTop": true,
                           "maxOpened": 0,
                           "preventDuplicates": false,
                           "preventOpenDuplicates": false
                       });

               })
               .error(function (data) {
                   toastr.error('Wystąpił błąd podczas zapisu!' , 'Błąd',
                       {
                           "autoDismiss": false,
                           "positionClass": "toast-top-full-width",
                           "type": "success",
                           "timeOut": "10000",
                           "extendedTimeOut": "2000",
                           "allowHtml": false,
                           "closeButton": true,
                           "tapToDismiss": false,
                           "progressBar": false,
                           "newestOnTop": true,
                           "maxOpened": 0,
                           "preventDuplicates": false,
                           "preventOpenDuplicates": false
                       });
               });
            item.isEditable = false;
        }
    }
})();