/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages', [
        'ui.router',
        'BlurAdmin.pages.academicYear',
        'BlurAdmin.pages.dashboard',
        'BlurAdmin.pages.ui',
        // 'BlurAdmin.pages.components',
        'BlurAdmin.pages.form',
        // 'BlurAdmin.pages.tables',
        'BlurAdmin.pages.configuration',
        'BlurAdmin.pages.newAcademicYearWizard',
        // 'BlurAdmin.pages.charts',
        // 'BlurAdmin.pages.maps',
        'BlurAdmin.pages.profile',
        'BlurAdmin.pages.employer',
        'BlurAdmin.pages.reports',
        // 'BlurAdmin.pages.student',
        'BlurAdmin.pages.studentpractice'

    ])
        .config(routeConfig);
    /** @ngInject */
    function routeConfig($urlRouterProvider, baSidebarServiceProvider,urlGetServiceProvider) {


        // baSidebarServiceProvider.addStaticItem({
        //     title: 'Menu Level 1',
        //     icon: 'ion-ios-more',
        //     subMenu: [{
        //         title: 'Menu Level 1.1',
        //         disabled: true
        //     }, {
        //         title: 'Menu Level 1.2',
        //         subMenu: [{
        //             title: 'Menu Level 1.2.1',
        //             disabled: true
        //         }]
        //     }]
        // });
    }

})();
