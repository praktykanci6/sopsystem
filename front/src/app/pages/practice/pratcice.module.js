/**
 * Created by tifet on 2017-05-04.
 */


(function (){
    'use strict';

    angular.module('BlurAdmin.pages.practice', ['ui.select','ngSanitize'])
        .config(routeConfig);

    function routeConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state('practice.adPractice',{
                url:  '/dodajPraktyki',
                templateUrl : 'app/pages/practice/pages/addPractice.html',
                controller:'AddPracticeCtrl',
                title: 'Praktyki',
                // sidebarMeta: {
                //     icon: 'ion-briefcase',
                //     order:100,
                // },
                role:'PRAKTYKODAWCA'
            })
        $urlRouterProvider.when('/dodajPraktyki'
        );

    }
})();
